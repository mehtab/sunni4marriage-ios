//
//  ArrayList.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 09/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

//getCharacterList
//getIncomeList
//getEducationList
//getLanguageList
//getBodyType
//getHieghts
//getNationalityArray

import UIKit

extension UIViewController {
    
    func getCharacterList() -> [String]
    {
        let array = [
            "Easy going",
            "Energetic",
            "Quarrelsome",
            "Predictable",
            "Controlling",
            "Compassionate",
            "Trustworthy",
            "To the point",
            "Funny",
            "Sensitive",
            "Adventurous",
            "Generous",
            "Content",
            "Patient",
            "Optimistic",
            "Spiritual",
            "Compromising",
            "Creative",
            "Romantic",
            "Good listener",
            "Spontaneous",
            "Organused",
            "Intelligent",
            "Random",
            "Assertive"
        ]
        
        return array
    }
    func getIncomeList() -> [String]
    {
        let array = [
            "Unemployed",
            "Less than £20k",
            "£20k-£30k",
            "£30,001-£40k",
            "£40,000-£50k",
            "£50,000-£60k",
            "More Than £60k",
            "Do not Wish to Disclose"
        ]
        
        return array
    }
    func getEducationList() -> [String]
    {
        let array = [
            "Completed High School",
            "GCSES",
            "GNVQ/NVQ",
            "A-Levels",
            "Post-graduate",
            "Bachelor degree",
            "Master degree",
            "Doctorate degree"
        ]
        
        return array
    }
    func getLanguageList() -> [String] {
        let array = [
            "English",
            "Urdu",
            "Arabic",
            "Farsi",
            "Hindi",
            "Spanish",
            "Pashto",
            "Chinese",
            "Portuguese",
            "Gujarati",
            "Indonesian",
            "Malay",
            "Tamil",
            "Italian",
            "Punjabi",
            "German",
            "French",
            "Other"
        ]
        
        return array
    }
    func getBodyType() -> [String]
    {
        let array = [
            "Slim",
            "Average",
            "Medium",
            "Athletic",
            "Slender",
            "Large"
        ]
        return array
    }
    
    func getHieghts() -> [String]
    {
        let array = [
            "4 foot 5 inches",
            "4 foot 6 inches",
            "4 foot 7 inches",
            "4 foot 8 inches",
            "4 foot 9 inches",
            "4 foot 10 inches",
            "4 foot 11 inches",
            "5 foot",
            "5 foot 1 inches",
            "5 foot 2 inches",
            "5 foot 3 inches",
            "5 foot 4 inches",
            "5 foot 5 inches",
            "5 foot 6 inches",
            "5 foot 7 inches",
            "5 foot 8 inches",
            "5 foot 9 inches",
            "5 foot 10 inches",
            "5 foot 11 inches",
            "6 foot",
            "6 foot 1 inches",
            "6 foot 2 inches",
            "6 foot 3 inches",
            "6 foot 4 inches",
            "6 foot 5 inches",
            "6 foot 6 inches",
            "6 foot 7 inches",
            "6 foot 8 inches",
            "6 foot 9 inches",
            "6 foot 10 inches",
            "6 foot 11 inches",
            "7 foot"
        ]
        
        return array
    }
    func getNationalitySelectedArray() -> [String]
    {
        let array = [
            "British",
            "Pakistani",
            "Indian",
            "American",
            "Other"
        ]
        
        //let sortedNames = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        
        return array
    }
    func getNationalityArray() -> [String]
    {
        let array = [
            "Afghan",
            "Åland Island",
            "Albanian",
            "Algerian",
            "American Samoan",
            "Andorran",
            "Angolan",
            "Anguillan",
            "Antarctic",
            "Antiguan or Barbudan",
            "Argentine",
            "Armenian",
            "Aruban",
            "Australian",
            "Austrian",
            "Azerbaijani, Azeri",
            "Bahamian",
            "Bahraini",
            "Bangladeshi",
            "Barbadian",
            "Belarusian",
            "Belgian",
            "Belizean",
            "Beninese, Beninois",
            "Bermudian, Bermudan",
            "Bhutanese",
            "Bolivian",
            "Bonaire",
            "Bosnian or Herzegovinian",
            "Motswana, Botswanan",
            "Bouvet Island",
            "Brazilian",
            "BIOT",
            "Bruneian",
            "Bulgarian",
            "Burkinabé",
            "Burundian",
            "Cabo Verdean",
            "Cambodian",
            "Cameroonian",
            "Canadian",
            "Caymanian",
            "Central African",
            "Chadian",
            "Chilean",
            "Chinese",
            "Christmas Island",
            "Cocos Island",
            "Colombian",
            "Comoran, Comorian",
            "Congolese",
            "Congolese",
            "Cook Island",
            "Costa Rican",
            "Ivorian",
            "Croatian",
            "Cuban",
            "Curaçaoan",
            "Cypriot",
            "Czech",
            "Danish",
            "Djiboutian",
            "Dominican",
            "Dominican",
            "Ecuadorian",
            "Egyptian",
            "Salvadoran",
            "Equatorial Guinean, Equatoguinean",
            "Eritrean",
            "Estonian",
            "Ethiopian",
            "Falkland Island",
            "Faroese",
            "Fijian",
            "Finnish",
            "French",
            "French Guianese",
            "French Polynesian",
            "French Southern Territories",
            "Gabonese",
            "Gambian",
            "Georgian",
            "German",
            "Ghanaian",
            "Gibraltar",
            "Greek, Hellenic",
            "Greenlandic",
            "Grenadian",
            "Guadeloupe",
            "Guamanian, Guambat",
            "Guatemalan",
            "Channel Island",
            "Guinean",
            "Bissau-Guinean",
            "Guyanese",
            "Haitian",
            "Heard Island or McDonald Islands",
            "Vatican",
            "Honduran",
            "Hong Kong, Hong Kongese",
            "Hungarian, Magyar",
            "Icelandic",
            "Indian",
            "Indonesian",
            "Iranian, Persian",
            "Iraqi",
            "Irish",
            "Manx",
            "Israeli",
            "Italian",
            "Jamaican",
            "Japanese",
            "Channel Island",
            "Jordanian",
            "Kazakhstani, Kazakh",
            "Kenyan",
            "I-Kiribati",
            "North Korean",
            "South Korean",
            "Kuwaiti",
            "Kyrgyzstani, Kyrgyz, Kirgiz, Kirghiz",
            "Lao, Laotian",
            "Latvian",
            "Lebanese",
            "Basotho",
            "Liberian",
            "Libyan",
            "Liechtenstein",
            "Lithuanian",
            "Luxembourg, Luxembourgish",
            "Macanese, Chinese",
            "Macedonian",
            "Malagasy",
            "Malawian",
            "Malaysian",
            "Maldivian",
            "Malian, Malinese",
            "Maltese",
            "Marshallese",
            "Martiniquais, Martinican",
            "Mauritanian",
            "Mauritian",
            "Mahoran",
            "Mexican",
            "Micronesian",
            "Moldovan",
            "Monégasque, Monacan",
            "Mongolian",
            "Montenegrin",
            "Montserratian",
            "Moroccan",
            "Mozambican",
            "Burmese",
            "Namibian",
            "Nauruan",
            "Nepali, Nepalese",
            "Dutch, Netherlandic",
            "New Caledonian",
            "New Zealand, NZ",
            "Nicaraguan",
            "Nigerien",
            "Nigerian",
            "Niuean",
            "Norfolk Island",
            "Northern Marianan",
            "Norwegian",
            "Omani",
            "Pakistani",
            "Palauan",
            "Palestinian",
            "Panamanian",
            "Papua New Guinean, Papuan",
            "Paraguayan",
            "Peruvian",
            "Philippine, Filipino",
            "Pitcairn Island",
            "Polish",
            "Portuguese",
            "Puerto Rican",
            "Qatari",
            "Réunionese, Réunionnais",
            "Romanian",
            "Russian",
            "Rwandan",
            "Barthélemois",
            "Saint Helenian",
            "Kittitian or Nevisian",
            "Saint Lucian",
            "Saint-Martinoise",
            "Saint-Pierrais or Miquelonnais",
            "Saint Vincentian, Vincentian",
            "Samoan",
            "Sammarinese",
            "São Toméan",
            "Saudi, Saudi Arabian",
            "Senegalese",
            "Serbian",
            "Seychellois",
            "Sierra Leonean",
            "Singaporean",
            "Sint Maarten",
            "Slovak",
            "Slovenian, Slovene",
            "Solomon Island",
            "Somali, Somalian",
            "South African",
            "South Georgia or South Sandwich Islands",
            "South Sudanese",
            "Spanish",
            "Sri Lankan",
            "Sudanese",
            "Surinamese",
            "Svalbard",
            "Swazi",
            "Swedish",
            "Swiss",
            "Syrian",
            "Chinese, Taiwanese",
            "Tajikistani",
            "Tanzanian",
            "Thai",
            "Timorese",
            "Togolese",
            "Tokelauan",
            "Tongan",
            "Trinidadian or Tobagonian",
            "Tunisian",
            "Turkish",
            "Turkmen",
            "Turks and Caicos Island",
            "Tuvaluan",
            "Ugandan",
            "Ukrainian",
            "Emirati, Emirian, Emiri",
            "British",
            "American",
            "American",
            "Uruguayan",
            "Uzbekistani, Uzbek",
            "Ni-Vanuatu, Vanuatuan",
            "Venezuelan",
            "Vietnamese",
            "British Virgin Island",
            "U.S. Virgin Island",
            "Wallis and Futuna, Wallisian or Futunan",
            "Sahrawi, Sahrawian, Sahraouian",
            "Yemeni",
            "Zambian",
            "Zimbabwean"
        ]
        
        let sortedNames = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }

        return sortedNames
    }
    func getEthnicities() -> [String]
    {
        let array = [
            "White British",
            "Irish British",
            "Other White Background",
            "White and Black Caribbean",
            "White and Black African",
            "White and Asian",
            "Indian",
            "Pakistani",
            "Bangladeshi",
            "Chinese",
            "Other Asain background",
            "Black Caribbean",
            "Black African",
            "Arab",
            "Other"
        ]
        
        return array
    }
    func getLocation() -> [String]
    {
        let array = [
            "Same Town",
            "Same Region",
            "Same Country"
        ]
        
        return array
    }
    func getMaritialStatus() -> [String]
    {
        let array = [
            "Never Married (Single)",
            "Divorced",
            "Widowed"
        ]
        
        return array
    }
    func getChildren() -> [String]
    {
        let array = [
            "Has No Children",
            "Has Children"
        ]
        
        return array
    }
    func getIslamic() -> [String]
    {
        let array = [
            "Born into Islam",
            "Reverted to Islam"
        ]
        
        return array
    }
    
    func getCountryNameList() -> [String] {
        let country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"]
        
        return country_list
    }
}
