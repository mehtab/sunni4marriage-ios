//
//  ActivePlanVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ActivePlanVC: UIViewController {

    @IBOutlet weak var lblNodata: UILabel!
    @IBOutlet weak var tblActivePlan: UITableView!
    
    var planArray = [NSObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPlanStatusViewAPI()
    }
    @IBAction func btnGetNow(_ sender: Any) {
    }
}
extension ActivePlanVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return planArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllPlanCell") as! AllPlanCell
        
        if let amount = planArray[indexPath.row].value(forKey:"amount") as? String
        {
            cell.lblPrice.text = "£\(amount)"
        }
        if let plan_days =  planArray[indexPath.row].value(forKey:"plan_days") as? String
        {
            cell.lblPriceTyper.text = "\(plan_days) Days"
        }
        if let plan_type =  planArray[indexPath.row].value(forKey:"plan_type") as? String
        {
            cell.lblPlanType.text = plan_type
        }
        if let plan_start_date =  planArray[indexPath.row].value(forKey:"plan_start_date") as? String
        {
            cell.lblStartDate.text = changeDateFormate_date(date: plan_start_date)
        }
        if let plan_end_date =  planArray[indexPath.row].value(forKey:"plan_end_date") as? String
        {
            cell.lblEndDate.text = changeDateFormate_date(date: plan_end_date)
            
            cell.lblDaysRemaining.text = getDaysBetweenTwoDates(endDateString: plan_end_date, dateformate: "yyyy-MM-dd HH:mm:ss")
        }
       return cell
    }
    
    
    func getDaysBetweenTwoDates(endDateString: String, dateformate:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateformate//"yyyy-MM-dd HH:mm:ss"
        let endDate:Date = dateFormatter.date(from: endDateString)!
        
        let calendar = Calendar.current
        
        let date1 = endDate.endOfDay
        let date2 = Date().startOfDay
        let componentsday = calendar.dateComponents([.day], from: date2, to: date1)
                
        var tempString = ""
        
        if componentsday.day == 1 {
            tempString = "\(componentsday.day!) day"
        } else {
            tempString = "\(componentsday.day!) days"
        }
        
        return tempString
    }
    
    
}
extension ActivePlanVC
{
    func getPlanStatusViewAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameters = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        let url = MAIN_API_URL+URLs.getPlanStatusView.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameters)
        print("\n\n")
        
        Alamofire.request(url, method: .post,parameters: parameters, encoding:  URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()

                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary

                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let items = mainResponce["data"] as! NSArray
                        
                        for item in items {
                            self.planArray.append(item as! NSObject)
                        }
                        self.tblActivePlan.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.lblNodata.isHidden = false
                        self.tblActivePlan.reloadData()
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                    self.lblNodata.isHidden = false
                    self.tblActivePlan.reloadData()
                }
        }
    }
}
