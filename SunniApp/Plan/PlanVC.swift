//
//  PlanVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Parchment

class PlanVC: UIViewController {

    @IBOutlet weak var viewcontainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "ActivePlanVC")
        firstViewController.title = "ACTIVATED PLAN"
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "AllPlanVC")
        secondViewController.title = "ALL PLAN"
        
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController
            ])
        
        pagingViewController.menuBackgroundColor = UIColor.white
        pagingViewController.textColor = UIColor.gray
        pagingViewController.indicatorColor = UIColor(rgb: 0x008515)
        pagingViewController.selectedTextColor = UIColor(rgb: 0x008515)
        pagingViewController.selectedBackgroundColor = UIColor.white
        pagingViewController.menuItemSize = .sizeToFit(minWidth: 170, height: 50)
        
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(pagingViewController)
        self.addSubview(subView: pagingViewController.view, toView: viewcontainer)
        pagingViewController.didMove(toParent: self)
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
