//
//  IAPManager.swift
//  FakeGame
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import Foundation
import StoreKit
import SVProgressHUD

// MARK: - SKPaymentTransactionObserver
extension AllPlanVC: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        transactions.forEach { (transaction) in
            SVProgressHUD.dismiss()
            
            switch transaction.transactionState {
            case .purchased:
                if transaction.payment.productIdentifier == IN_APP_PURCHASE.inAppPurchaseProductId
                {
                    self.receiptValidation()
                }
                
                self.successInAppPurchase(isRestore: false, transactionID: transaction.transactionIdentifier!)
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case .restored:
                self.successInAppPurchase(isRestore: true, transactionID: transaction.transactionIdentifier!)
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case .failed:
                if let error = transaction.error as? SKError {
                    if error.code != .paymentCancelled {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                            self.showAlertView(message: "Unable to fetch available In-App Purchase products at the moment.")
                        }
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                            self.showAlertView(message: "In-App Purchase process was cancelled.")
                        }
                    }
                    print("IAP Error:", error.localizedDescription)
                }
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case .deferred, .purchasing: break
            @unknown default: break
            }
        }
    }
    
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        SVProgressHUD.dismiss()
        print("IAP: purchases to restore!")
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        if let error = error as? SKError {
            SVProgressHUD.dismiss()
            if error.code != .paymentCancelled {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                    self.showAlertView(message: "Unable to fetch available In-App Purchase products at the moment.")
                }
                print("IAP Restore Error:", error.localizedDescription)
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                    self.showAlertView(message: "In-App Purchase process was cancelled.")
                }
            }
        }
    }
}

// MARK: - SKProductsRequestDelegate
extension AllPlanVC: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        SVProgressHUD.dismiss()
        
        let products = response.products
        
        if products.count > 0 {
            self.products = products
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                self.getPlanAPI()
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                self.showAlertView(message: "No products found.")
            }
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        SVProgressHUD.dismiss()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
            self.showAlertView(message: "Unable to fetch available In-App Purchase products at the moment.")
        }
    }
    
    func requestDidFinish(_ request: SKRequest) {
        SVProgressHUD.dismiss()
        // Implement this method OPTIONALLY and add any custom logic
        // you want to apply when a product request is finished.
    }
}

extension AllPlanVC {
    func inAppPurchaseSetup() {
        SVProgressHUD.show()
        
        guard let productIDs = getProductIDs() else {
            SVProgressHUD.dismiss()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                self.showAlertView(message: "No products found.")
            }
            return
        }
        
        let request = SKProductsRequest(productIdentifiers: Set(productIDs))
        request.delegate = self
        request.start()
    }
    
    func getProductIDs() -> [String]? {
        return [IN_APP_PURCHASE.inAppPurchaseProductId]
        
        /*guard let url = Bundle.main.url(forResource: "IAP_ProductIDs", withExtension: "plist") else { return nil }
        do {
            let data = try Data(contentsOf: url)
            let productIDs = try PropertyListSerialization.propertyList(from: data, options: .mutableContainersAndLeaves, format: nil) as? [String] ?? []
            return productIDs
        } catch {
            print(error.localizedDescription)
            return nil
        }*/
    }
    
    func getProduct(containing keyword: String) -> SKProduct? {
        return products.filter { $0.productIdentifier.contains(keyword) }.first
    }
    
    func showPaymentAlert(for product: SKProduct) {
        guard let price = self.getPriceFormatted(for: product) else { return }
        
        let alertController = UIAlertController(title: product.localizedTitle,
                                                message: product.localizedDescription,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Buy now for \(price)", style: .default, handler: { (_) in
            
            if !self.purchase(product: product) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
                    self.showAlertView(message: "In-App Purchases are not allowed in this device.")
                }
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getPriceFormatted(for product: SKProduct) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = product.priceLocale
        return formatter.string(from: product.price)
    }
    
    func startObserving() {
        SKPaymentQueue.default().add(self)
    }
    
    
    func stopObserving() {
        SKPaymentQueue.default().remove(self)
    }
    
    func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    func purchase(product: SKProduct) -> Bool {
        if !self.canMakePayments() {
            SVProgressHUD.dismiss()
            return false
        } else {
            SVProgressHUD.show()
            
            let payment = SKPayment(product: product)
            startObserving()
            SKPaymentQueue.default().add(payment)
        }
        
        return true
    }
    
    func restorePurchases() {
        SVProgressHUD.show()
        
        startObserving()
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

extension AllPlanVC {
    func receiptValidation()
    {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path)
        {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                
                let receiptString = receiptData.base64EncodedString(options: [])
                let dict = ["receipt-data" : receiptString, "password" : IN_APP_PURCHASE.inAppPurchaseSecretKey] as [String : Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    
//                    let sandbox_verifyReceipt = "https://sandbox.itunes.apple.com/verifyReceipt"
                    let live_verifyReceipt = "https://buy.itunes.apple.com/verifyReceipt"
                    
                    if let sandboxURL = Foundation.URL(string: live_verifyReceipt)
                    {
                        var request = URLRequest(url: sandboxURL)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        let session = URLSession(configuration: URLSessionConfiguration.default)
                        let task = session.dataTask(with: request)
                        { data, response, error in
                            if let receivedData = data,
                                let httpResponse = response as? HTTPURLResponse,
                                error == nil,
                                httpResponse.statusCode == 200
                            {
                                do {
                                    if  (try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>) != nil
                                    {
                                        let mainResponce = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                                        
                                        print("\n\n\n\n hurreeeeeee sucess")
                                        print(mainResponce)
                                        if mainResponce["status"] as! Int == 0
                                        {
                                            //"Transaction sucess"
                                        } else {
                                            //"Transaction Fail"
                                        }
                                    }
                                    else
                                    {
                                        print("Failed to cast serialized JSON to Dictionary<String, AnyObject>")
                                    }
                                }
                                catch
                                {
                                    print("Couldn't serialize JSON with error: " + error.localizedDescription)
                                }
                            }
                        }
                        task.resume()
                    }
                    else
                    {
                        print("Couldn't convert string into URL. Check for special characters.")
                    }
                }
                catch
                {
                    print("Couldn't create JSON with error: " + error.localizedDescription)
                }
            }
            catch
            {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
            }
        }
    }
}
