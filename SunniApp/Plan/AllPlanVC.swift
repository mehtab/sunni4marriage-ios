//
//  AllPlanVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import StoreKit

class AllPlanVC: UIViewController {

    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var tblPlan: UITableView!
    @IBOutlet weak var viewRestore: LetsView!
    @IBOutlet weak var viewRestoreHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewHeaderHeightsConstaint: NSLayoutConstraint!
    var textValue:String = ""
    var orignalPrice:Double!
    var discount:Int = 0
    
    var planArray = NSMutableArray()
    
    var products = [SKProduct]()
    var isOpenAllPlan = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPlanAPI()
        if isOpenAllPlan {
            viewHeader.isHidden = false
            viewHeaderHeightsConstaint.constant = 60
        } else {
            viewHeader.isHidden = true
            viewHeaderHeightsConstaint.constant = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.inAppPurchaseSetup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        self.stopObserving()
    }
    @IBAction func restorePurchases(_ sender: Any) {
        self.restorePurchases()
    }
    
    func hideRestoreView() {
        viewRestore.isHidden = true
        viewRestoreHieghtConstraint.constant = 0
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnGetNow(_ sender: UIButton)
    {
        
        // let my login here
        
    //   let discountamount =
        
        
       // Integer newAmount = orignalPrice - (orignalPrice * (discount / 100));
        
      
        
       
    }
    
    func nextController()
    {
        var gotoPaymentScreen = false
        
        if UserDefaults.standard.value(forKey: "payment_status") != nil
        {
            if UserDefaults.standard.value(forKey: "payment_status") as! String == "1"
            {
                //"Account-Paid"
                gotoPaymentScreen = false
            }
            else
            {
                //"Account-Free"
                gotoPaymentScreen = true
            }
        }
        
        if gotoPaymentScreen == false {
            showTosterAlert(message: "Your plan is already upgraded")
        } else {
            guard let product = self.getProduct(containing: IN_APP_PURCHASE.inAppPurchaseProductId) else {
                self.showAlertView(message: "No products found.")
                return
            }
            showPaymentAlert(for: product)
            
            /*let mainDic = planArray[sender.tag] as! NSDictionary
            
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            nextview.amount = "\(mainDic["plan_sell_amount"] as! String)"
            self.navigationController?.pushViewController(nextview, animated: true)*/
        }
    }
    
    func successInAppPurchase(isRestore: Bool, transactionID: String) {
        SVProgressHUD.dismiss()
        
        var message = ""
        if isRestore {
            message = "In app purchase restore successfully"
        } else {
            message = "In app purchase successfully"
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
            let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                if !isRestore {
                    self.iosPaymentInfoAPI(payer_id: transactionID)
                }
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension AllPlanVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return planArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllPlanCell") as! AllPlanCell
        
        let mainDic = planArray[indexPath.row] as! NSDictionary
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "£\(mainDic["plan_amount"] as! String)")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        cell.lblOldPrice.attributedText = attributeString
        cell.lblNewPrice.text = "£69"
        orignalPrice = Double(mainDic["plan_sell_amount"] as! String)
        cell.lblTotalDays.text = "\(mainDic["plan_days"] as! String) Days"
        cell.lblDescription.attributedText = "\(mainDic["plan_description"] as! String)".htmlAttributedString()
        cell.lblDescription.font = UIFont.init(name: "Montserrat-Regular", size: 15)
        cell.lblDescription.textColor = UIColor.lightGray
        cell.lblPlanName.text = "\(mainDic["plan_name"] as! String)".uppercased()
        cell.PromoCodetext.text = ""
        cell.PromoCodetext.tag = indexPath.row
        cell.PromoCodetext.addTarget(self, action: #selector(handleChangetext), for: .valueChanged)
        cell.btnGetNow.tag = indexPath.row
       
        cell.btnGetNow.addTarget(self, action: #selector(HangleGetNew), for: .touchUpInside)
        cell.PromoCodetext.text = ""
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    @objc func handleChangetext(_ sender:UITextField)
    {
        textValue = sender.text!
    }
}
extension AllPlanVC
{
    func getPlanAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getPlan.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.planArray = data.mutableCopy() as! NSMutableArray
                        self.tblPlan.reloadData()
                        
                        if self.planArray.count == 0
                        {
                            self.lblNoData.isHidden = false
                        }
                        else
                        {
                            self.lblNoData.isHidden = true
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                        self.lblNoData.isHidden = false
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                    self.lblNoData.isHidden = false
                }
        }
    }
    
    @objc func HangleGetNew(_ sender: UIButton)
    {
        let index = IndexPath(row: sender.tag, section: 0)
       
        
        let cell = tblPlan.cellForRow(at: index) as! AllPlanCell
        if  cell.PromoCodetext.text!.isEmpty{
        
            var gotoPaymentScreen = false
            
            if UserDefaults.standard.value(forKey: "payment_status") != nil
            {
                if UserDefaults.standard.value(forKey: "payment_status") as! String == "1"
                {
                    //"Account-Paid"
                    gotoPaymentScreen = false
                }
                else
                {
                    //"Account-Free"
                    gotoPaymentScreen = true
                }
            }
            
            if gotoPaymentScreen == false {
                showTosterAlert(message: "Your plan is already upgraded")
            } else {
                let mainDic = planArray[sender.tag] as! NSDictionary
                
                let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                nextview.amount = "69.00"
                self.navigationController?.pushViewController(nextview, animated: true)
//                guard let product = self.getProduct(containing: IN_APP_PURCHASE.inAppPurchaseProductId) else {
//                    self.showAlertView(message: "No products found.")
//                    return
//                }
//                showPaymentAlert(for: product)
                
                /*let mainDic = planArray[sender.tag] as! NSDictionary
                
                let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                nextview.amount = "\(mainDic["plan_sell_amount"] as! String)"
                self.navigationController?.pushViewController(nextview, animated: true)*/
            }
           
        }else{
            let name = cell.PromoCodetext.text!.lowercased()
            SVProgressHUD.show(withStatus: "LOADING")
            let url = "\(MAIN_API_URL)Promo/GetProfomInfo?Promo="+name
            Alamofire.request(url , method: .get, encoding: JSONEncoding.default,headers:[:]).responseJSON { [self] response in
                switch response.result {
                    case .success(let value):
                    if let json = value as? [String:Any]
                    {
                        SVProgressHUD.dismiss()
                        let message = json["message"] as? String
                        if message!.lowercased() == "expired"
                        {
                            showAlertViewTextcolrChange(message: "Promotion code is expired")
                        }else if message!.lowercased() == "success"
                        {
                            showTosterAlert(message: "your Promo successfully updated")
                            let discount = json["discount"] as? String
                            let mainDic = planArray[sender.tag] as! NSDictionary
                            
                            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            
                            let orignalPrice = "69.00"
                            
                            let discountParsing = Double(discount!)
                            let orignalPriceParsing = Double(orignalPrice)
                            let number = 100 - discountParsing!
                            let finalNumber = (number * orignalPriceParsing!) / 100
                            
                           
//                            let newAmount = orignalPriceParsing ?? 0 - discountParsing! ?? 0 * (discountParsing! / 100)
                            
//                            let newAmount = Int(orignalPrice)! - (Int(discount!)! * Int(Double((Int(discount!)! / 100))))
                            
                            nextview.amount = String(Int(finalNumber.rounded(toPlaces: 0)))
                            
                            
                            
                            self.navigationController?.pushViewController(nextview, animated: true)
                        }else
                        {
                            if message!.lowercased() == "not found"
                            {
                                showAlertViewTextcolrChange(message: "Promotion code is not Valid")
                            }
                        }
                      
                        
                        
                        
                        
                      
                       
                    }
                    break;
                case .failure(let error):
                    break
                    
                }
            }
            
//
//            if cell.PromoCodetext.text == "ALI123"{
//
//                showTosterAlert(message: "VERIFIED")
//            }else if cell.PromoCodetext.text != "ALI123"
//            {
//                showTosterAlert(message: "Expired")
//            }
                
        }
        
    }
    
    func getProfileAPI(username:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":username
        ]
        
        let url = MAIN_API_URL+URLs.getUserData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        if let userId = dicarr["userId"] as? String
                        {
                            UserDefaults.standard.set(userId, forKey: "user_id")
                        }
                        if let name = dicarr["name"] as? String
                        {
                            UserDefaults.standard.set(name, forKey: "name")
                        }
                        if let first_name = dicarr["first_name"] as? String
                        {
                            UserDefaults.standard.set(first_name, forKey: "first_name")
                        }
                        if let last_name = dicarr["last_name"] as? String
                        {
                            UserDefaults.standard.set(last_name, forKey: "last_name")
                        }
                        if let unique_user_id = dicarr["unique_user_id"] as? String
                        {
                            UserDefaults.standard.set(unique_user_id, forKey: "unique_user_id")
                        }
                        
                        let payment_status = self.getNumberString(object: dicarr["payment_status"] as? NSObject)
                        UserDefaults.standard.set(payment_status, forKey: "payment_status")
                        
                        if let profilepic = dicarr["profilepic"] as? String
                        {
                            UserDefaults.standard.set(profilepic, forKey: "profilepic")
                        }
                        
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                        self.navigationController?.pushViewController(nextView, animated: true)
                    }
                    else if mainResponce["statusCode"] as! Int == 401
                    {
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                    else if mainResponce["statusCode"] as! Int == 404
                    {
                        UserDefaults.standard.removeObject(forKey: "email")
                        
                        let view = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let calender = NSCalendar.current
        dateFormatter.calendar = calender
        dateFormatter.timeZone = TimeZone.current

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let dateString = dt {
            return dateFormatter.string(from: dateString)
        } else {
            return ""
        }
    }
    
    func iosPaymentInfoAPI(payer_id: String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let parameter = [
            "payment_date":localToUTC(date: dateFormatter.string(from: Date())),
            "payer_id":payer_id,
            "mc_currency":IN_APP_PURCHASE.mc_currency,
            "mc_gross":IN_APP_PURCHASE.mc_gross,
            "custom":UserDefaults.standard.value(forKey: "user_id") as! String,
            "item_name":"upgrade",
            "item_number":"1"
        ]
        
        let url = MAIN_API_URL+URLs.iosPaymentInfo.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.getProfileAPI(username: UserDefaults.standard.value(forKey: "email") as! String)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    let alertController = UIAlertController(title: "Alert", message: "Server is slow down Please try Again", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                    {
                        UIAlertAction in
                        self.iosPaymentInfoAPI(payer_id: payer_id)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
        }
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
