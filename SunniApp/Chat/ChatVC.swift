//
//  ChatVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SVProgressHUD

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: LetsImageView!
}
class ChatVC: UIViewController {

    @IBOutlet weak var tblChat: UITableView!
    
    var searchDataArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getFriendListAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func getFriendListAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "flag":"friend_list"
        ]
        
        let url = MAIN_API_URL+URLs.FriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.searchDataArray = data.mutableCopy() as! NSMutableArray
                        self.tblChat.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension ChatVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return searchDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as! ChatCell
        
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary

        if let name = mainDic["name"] as? String
        {
            let trimmedString = name.trimmingCharacters(in: .whitespaces)
            cell.lblName.text = trimmedString
        }
        else
        {
            cell.lblName.text = ""
        }
        
        
        var avtarImage = UIImage.init(named: placeHolderImages_male)
        if let gender = mainDic["gender"] as? String
        {
            if gender == "FEMALE"
            {
                avtarImage = UIImage.init(named: placeHolderImages_female)
            }
            else
            {
                avtarImage = UIImage.init(named: placeHolderImages_male)
            }
        }
        
        cell.imgProfile.isHidden = false
        if let profilepic = mainDic["profilepic"] as? String
        {
            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(profilepic)"
            cell.imgProfile.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
            cell.imgProfile.isHidden = false
        }
        else
        {
            cell.imgProfile.isHidden = true
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ConversationVC") as! ConversationVC
        nextview.recieverid = mainDic["userId"] as! String
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    
}
