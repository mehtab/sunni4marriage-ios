//
//  ConversationVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 13/09/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SVProgressHUD

class messageCell: UITableViewCell
{
    @IBOutlet weak var lblMessageWidthConst: NSLayoutConstraint!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}
class ConversationVC: UIViewController {

    @IBOutlet weak var sentMessageViewBottomConst: NSLayoutConstraint!
    @IBOutlet weak var scrollviewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var contentViewHieghtConst: NSLayoutConstraint!
    @IBOutlet weak var txtMessage: LetsTextView!
    @IBOutlet weak var tblMessage: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    
    var messageArr = NSMutableArray()
    var recieverid = ""
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        txtMessage.delegate = self
        tblMessage.isHidden = true
        
        getChatMessageAPI(isDidload: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        IQKeyboardManager.shared.enable = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        IQKeyboardManager.shared.enable = true
        timer.invalidate()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSend(_ sender: Any)
    {
        if txtMessage.text == ""
        {
            showTosterAlert(message: "Please write message")
        }
        else
        {
            addChatMessageAPI()
        }
    }
    func heightForView(text:String, width:CGFloat) -> CGFloat
    {
        let font = UIFont.init(name: "Montserrat-Regular", size: 17)
        
        let label:UILabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    func widthForView(text:String, height:CGFloat) -> CGFloat
    {
        let font = UIFont.init(name: "Montserrat-Regular", size: 17)
        
        let label:UILabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.width
    }
}
extension ConversationVC:UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        print("textViewDidBeginEditing")
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
        print("textViewDidEndEditing")
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            print(keyboardHeight)
            
            sentMessageViewBottomConst.constant = keyboardHeight
        }
    }
    @objc func keyboardWillHide(_ notification: Notification)
    {
        sentMessageViewBottomConst.constant = 0
    }
}
extension ConversationVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return messageArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = messageCell()
        
        let mainDic = messageArr[indexPath.row] as! NSDictionary
        let lbldate = mainDic["sentat"] as! String
        let msgString = mainDic["message"] as! String
    
        let sentidString = mainDic["sent_id"] as! String
        let loginUseridString = UserDefaults.standard.value(forKey: "user_id") as! String
        
        let maxWidth = self.view.frame.size.width-60
        let tempHeight = heightForView(text: msgString, width: maxWidth)
        let tempWidth = widthForView(text: msgString, height: tempHeight)
        
        if sentidString == loginUseridString
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "messageCell_my") as! messageCell
            
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "messageCell_fnd") as! messageCell
        }
        cell.lblDate.text = lbldate
        cell.lblMessage.text = msgString
        cell.lblMessageWidthConst.constant = tempWidth
        if maxWidth > tempWidth
        {
            cell.lblMessageWidthConst.constant = tempWidth
        }
        else
        {
            cell.lblMessageWidthConst.constant = maxWidth
        }

        contentViewHieghtConst.constant = tableView.contentSize.height + 40
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension ConversationVC
{
        @objc func eventWith(timer: Timer!)
        {
            getChatMessageAPI(isDidload: false)
        }
    func getChatMessageAPI(isDidload:Bool)
    {
        if isDidload == true
        {
            SVProgressHUD.show(withStatus: "LOADING")
        }
//        http://diestechnology.com.au/projects/sunnis4marriage/app_controller/Chat_message/getChatMessage?sent_id="+num1+"&reciver_id="+num2
//        num1=Session id(Receiver ID)
//        num2=(Sender ID)
        
        let senderid = UserDefaults.standard.value(forKey: "user_id") as! String
        
        let url = MAIN_API_URL + "Chat_message/getChatMessage?sent_id=\(recieverid)&reciver_id=\(senderid)"
        
        print("\n\n\n\n\n")
        print(url)
        print("\n\n")
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["error"] as! Int == 0
                    {
                        let data = mainResponce["message"] as! NSArray
                        self.messageArr = data.mutableCopy() as! NSMutableArray
                        self.tblMessage.reloadData()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                            let bottomOffset = CGPoint(x: 0, y: self.scrollview.contentSize.height - self.scrollview.bounds.size.height)
                            self.scrollview.setContentOffset(bottomOffset, animated: true)
                            
                            self.tblMessage.isHidden = false
                        }
                        
                        
                        if isDidload == true
                        {
                            self.timer = Timer.scheduledTimer(timeInterval: 5.0,
                                                              target: self,
                                                              selector: #selector(self.eventWith),
                                                              userInfo: nil,
                                                              repeats: true)
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func addChatMessageAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "sent_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "messages":txtMessage.text!,
            "reciver_id":recieverid
        ]
        
        
        let url = MAIN_API_URL+URLs.addChatMessage.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["error"] as! Int == 0
                    {
                        self.txtMessage.resignFirstResponder()
                        
                        let mainDic = [
                            "message": self.txtMessage.text!,
                            "sent_id": UserDefaults.standard.value(forKey: "user_id") as! String,
                            "reciver_id": self.recieverid
                        ]
                        self.messageArr.add(mainDic)
                        self.txtMessage.text = ""
                        
                        self.tblMessage.reloadData()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                            let bottomOffset = CGPoint(x: 0, y: self.scrollview.contentSize.height - self.scrollview.bounds.size.height)
                            self.scrollview.setContentOffset(bottomOffset, animated: true)
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
