//
//  NearbyProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import CoreLocation
import RangeSeekSlider

class NearbyProfileVC: UIViewController,CLLocationManagerDelegate
{
    @IBOutlet weak var sliderMiles: UISlider!
    @IBOutlet weak var rangeslider: RangeSeekSlider!
    @IBOutlet weak var lblMilesMin: LetsLabel!
    @IBOutlet weak var lblMilesMax: LetsLabel!

    @IBOutlet weak var sliderRadius: RangeSeekSlider!
    @IBOutlet weak var lblAgeMin: LetsLabel!
    @IBOutlet weak var lblAgeMax: LetsLabel!
    @IBOutlet weak var sliderAge: UISlider!
    
    var age_min = "18"
    var age_max = "80"
    
    var miles_min = "0"
    var miles_max = "0"//100
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        lblMilesMin.text = "0 Miles"
        sliderMiles.value = 0
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        latitude = "\(locValue.latitude)"
        longitude = "\(locValue.longitude)"
        
        locationManager.stopUpdatingLocation()
    }
    @IBAction func mileSliderNew(_ sender: UISlider) {
        lblMilesMin.text = "\(Int(sender.value)) Miles"
        
        miles_min = "0"
        miles_max = "\(Int(sender.value))"
    }
    @IBAction func sliderAge(_ sender: UISlider)
    {
//        lblAge.text = "\(Int(sender.value)) Age"
//        age = "\(Int(sender.value))"
    }
    @IBAction func sliderAction(_ sender: RangeSeekSlider)
    {
        lblAgeMin.text = "Min \(Int(sender.selectedMinValue)) Yrs"
        lblAgeMax.text = "Max \(Int(sender.selectedMaxValue)) Yrs"
        
        age_min = "\(Int(sender.selectedMinValue))"
        age_max = "\(Int(sender.selectedMaxValue))"
    }
    @IBAction func changeslider(_ sender: RangeSeekSlider) {
        lblAgeMin.text = "Min \(Int(sender.selectedMinValue)) Yrs"
        lblAgeMax.text = "Max \(Int(sender.selectedMaxValue)) Yrs"
        
        age_min = "\(Int(sender.selectedMinValue))"
        age_max = "\(Int(sender.selectedMaxValue))"
    }
    @IBAction func sliderRadius(_ sender: RangeSeekSlider)
    {
        lblMilesMin.text = "\(Int(sender.selectedMinValue)) Miles"
        lblMilesMax.text = "\(Int(sender.selectedMaxValue)) Miles"
        
        miles_min = "\(Int(sender.selectedMinValue))"
        miles_max = "\(Int(sender.selectedMaxValue))"
    }
    @IBAction func sliderDraginsideRadius(_ sender: RangeSeekSlider)
    {
        lblMilesMin.text = "\(Int(sender.selectedMinValue)) Miles"
        lblMilesMax.text = "\(Int(sender.selectedMaxValue)) Miles"
        
        miles_min = "\(Int(sender.selectedMinValue))"
        miles_max = "\(Int(sender.selectedMaxValue))"
    }
    @IBAction func btnSearchByLocation(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "NearByProfileUserListVC") as! NearByProfileUserListVC
        nextview.age_min = age_min
        nextview.age_max = age_max
        nextview.miles = "\(miles_max)"
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    
}

