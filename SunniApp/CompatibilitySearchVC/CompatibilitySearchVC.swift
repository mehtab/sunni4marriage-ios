//
//  CompatibilitySearchVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Parchment

class CompatibilitySearchVC: UIViewController {

    @IBOutlet weak var viewcontainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "SearchVC")
        firstViewController.title = "SEARCH"
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "SearchForIdVC")
        secondViewController.title = "SEARCH FOR ID"
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "SuggestionVC")
        thirdViewController.title = "SUGGESTION"
        
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController,
            thirdViewController
            ])
        
        pagingViewController.menuBackgroundColor = UIColor(rgb: 0x008515)
        pagingViewController.textColor = UIColor.white
        pagingViewController.borderColor = UIColor.white
        pagingViewController.indicatorColor = UIColor.white
        pagingViewController.selectedTextColor = UIColor.white
        pagingViewController.selectedBackgroundColor = UIColor(rgb: 0x008515)
        pagingViewController.menuItemSize = .sizeToFit(minWidth: 150, height: 50)
        
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(pagingViewController)
        self.addSubview(subView: pagingViewController.view, toView: viewcontainer)
        pagingViewController.didMove(toParent: self)
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
