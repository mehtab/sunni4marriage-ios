//
//  SearchForIdVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit

class SearchForIdVC: UIViewController {

    @IBOutlet weak var txtSearchByid: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnSearch(_ sender: Any)
    {
        if txtSearchByid.text == ""
        {
            showTosterAlert(message: "Please enter search id")
        }
        else
        {
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SearchDataVC") as! SearchDataVC
            nextview.uniq_id_SearchVC = txtSearchByid.text!
            nextview.isFrom = "SearchForIdVC"
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    
}
