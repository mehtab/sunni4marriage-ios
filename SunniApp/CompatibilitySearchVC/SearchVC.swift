//
//  SearchVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import RangeSeekSlider

class SearchVC: UIViewController {

    @IBOutlet weak var tblEthnicity: UITableView!
    @IBOutlet weak var viewTablePopup: UIView!
    @IBOutlet weak var rangeslider: RangeSeekSlider!
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var viewPickerPopup: UIView!
    @IBOutlet weak var txtIsamicBg: UITextField!
    @IBOutlet weak var txtChildren: UITextField!
    @IBOutlet weak var txtMaritialStatus: UITextField!
    @IBOutlet weak var txtNationalities: UITextField!
    @IBOutlet weak var txtEducation: UITextField!
    @IBOutlet weak var txtLanguage: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtEthnicities: UITextField!
    @IBOutlet weak var lblMaxYear: UILabel!
    @IBOutlet weak var lblMinYear: UILabel!
    
    var pickerArray = [String]()
    var selectedIndex = 0
    
    var eth_loc_lan_edu_nat_mar_chi_isl = ""
    var ethnicitiesArray = [String]()
    var locationArray = [String]()
    var languageArray = [String]()
    var educationArray = [String]()
    var nationalityArray = [String]()
    var maritalArray = [String]()
    var childrenArray = [String]()
    var islamArray = [String]()
    
    var selectedEthnicitiesArray = NSMutableArray()
    var selectedLanguagesArray = NSMutableArray()
    var tableDataArray = [String]()
    var eth_lan = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ethnicitiesArray = getEthnicities()
        locationArray = getLocation()
        languageArray = getLanguageList()
        educationArray = getEducationList()
        nationalityArray = getNationalitySelectedArray()
        maritalArray = getMaritialStatus()
        childrenArray = getChildren()
        islamArray = getIslamic()
    }
    @IBAction func sliderAction(_ sender: RangeSeekSlider)
    {
        lblMinYear.text = "Min \(Int(sender.selectedMinValue)) Yrs"
        lblMaxYear.text = "Max \(Int(sender.selectedMaxValue)) Yrs"
    }
    @IBAction func changeslider(_ sender: RangeSeekSlider) {
        lblMinYear.text = "Min \(Int(sender.selectedMinValue)) Yrs"
        lblMaxYear.text = "Max \(Int(sender.selectedMaxValue)) Yrs"
    }
    @IBAction func btnSearch(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SearchDataVC") as! SearchDataVC
        nextview.ethnicity_SearchVC = txtEthnicities.text!
        nextview.age_min_SearchVC = "\(Int(rangeslider.selectedMinValue))"
        nextview.age_max_SearchVC = "\(Int(rangeslider.selectedMaxValue))"
        nextview.nationality_SearchVC = txtNationalities.text!
        nextview.maritalstatus_SearchVC = txtMaritialStatus.text!.replacingOccurrences(of: " (Single)", with: "")
        nextview.islamic_SearchVC = txtIsamicBg.text!
        nextview.education_SearchVC = txtEducation.text!
        nextview.town_city_SearchVC = txtLocation.text!
        nextview.isFrom = "SearchVC"
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnClear(_ sender: Any)
    {
        rangeslider.maxValue = 80
        rangeslider.minValue = 18
        
        lblMinYear.text = "Min \(Int(rangeslider.selectedMinValue)) Yrs"
        lblMaxYear.text = "Max \(Int(rangeslider.selectedMaxValue)) Yrs"
        
        txtEthnicities.text = ""
        txtLocation.text = ""
        txtLanguage.text = ""
        txtEducation.text = ""
        txtNationalities.text = ""
        txtMaritialStatus.text = ""
        txtChildren.text = ""
        txtIsamicBg.text = ""
    }
    @IBAction func btnEthnicities(_ sender: Any)
    {
        //pickerArray = ethnicitiesArray
//        selectedIndex = 0
//        eth_loc_lan_edu_nat_mar_chi_isl = "eth"
//        pickerview.reloadAllComponents()
//        pickerview.selectRow(0, inComponent: 0, animated: false)
        eth_lan = "eth"
        tableDataArray = ethnicitiesArray
        tblEthnicity.reloadData()
        viewTablePopup.isHidden = false
    }
    @IBAction func btnLocation(_ sender: Any)
    {
        pickerArray = locationArray
        selectedIndex = 0
        eth_loc_lan_edu_nat_mar_chi_isl = "loc"
        pickerview.reloadAllComponents()
        pickerview.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnLanguage(_ sender: Any)
    {
//        pickerArray = languageArray
//        selectedIndex = 0
//        eth_loc_lan_edu_nat_mar_chi_isl = "lan"
//        pickerview.reloadAllComponents()
//        pickerview.selectRow(0, inComponent: 0, animated: false)
//
//        viewPickerPopup.isHidden = false
        
        eth_lan = "lan"
        tableDataArray = languageArray
        tblEthnicity.reloadData()
        viewTablePopup.isHidden = false
    }
    @IBAction func btnEducation(_ sender: Any)
    {
        pickerArray = educationArray
        selectedIndex = 0
        eth_loc_lan_edu_nat_mar_chi_isl = "edu"
        pickerview.reloadAllComponents()
        pickerview.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnNationalities(_ sender: Any)
    {
        pickerArray = nationalityArray
        selectedIndex = 0
        eth_loc_lan_edu_nat_mar_chi_isl = "nat"
        pickerview.reloadAllComponents()
        pickerview.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnMariatialStatus(_ sender: Any)
    {
        pickerArray = maritalArray
        selectedIndex = 0
        eth_loc_lan_edu_nat_mar_chi_isl = "mar"
        pickerview.reloadAllComponents()
        pickerview.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnChildrenbg(_ sender: Any)
    {
        pickerArray = childrenArray
        selectedIndex = 0
        eth_loc_lan_edu_nat_mar_chi_isl = "chi"
        pickerview.reloadAllComponents()
        pickerview.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnIslamicbg(_ sender: Any)
    {
        pickerArray = islamArray
        selectedIndex = 0
        eth_loc_lan_edu_nat_mar_chi_isl = "isl"
        pickerview.reloadAllComponents()
        pickerview.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnCancelPopup(_ sender: Any)
    {
        viewPickerPopup.isHidden = true
    }
    @IBAction func btnPickerClear(_ sender: Any) {
        if eth_loc_lan_edu_nat_mar_chi_isl == "eth"
        {
            txtEthnicities.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "loc"
        {
            txtLocation.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "lan"
        {
            txtLanguage.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "edu"
        {
            txtEducation.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "nat"
        {
            txtNationalities.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "mar"
        {
            txtMaritialStatus.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "chi"
        {
            txtChildren.text = ""
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "isl"
        {
            txtIsamicBg.text = ""
        }
        viewPickerPopup.isHidden = true
    }
    @IBAction func btnDonePopup(_ sender: Any)
    {
        if eth_loc_lan_edu_nat_mar_chi_isl == "eth"
        {
            txtEthnicities.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "loc"
        {
            txtLocation.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "lan"
        {
            txtLanguage.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "edu"
        {
            txtEducation.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "nat"
        {
            txtNationalities.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "mar"
        {
            txtMaritialStatus.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "chi"
        {
            txtChildren.text = pickerArray[selectedIndex]
        }
        else if eth_loc_lan_edu_nat_mar_chi_isl == "isl"
        {
            txtIsamicBg.text = pickerArray[selectedIndex]
        }
        viewPickerPopup.isHidden = true
    }
    @IBAction func btnDoneTable(_ sender: Any)
    {
        if eth_lan == "eth"
        {
            var tempString = ""
            
            for(index,_) in selectedEthnicitiesArray.enumerated()
            {
                tempString.append(selectedEthnicitiesArray[index] as! String)
                tempString.append(",")
            }
            if tempString != ""
            {
                tempString.removeLast()
            }
            txtEthnicities.text = tempString
        }
        else
        {
            var tempString = ""
            
            for(index,_) in selectedLanguagesArray.enumerated()
            {
                tempString.append(selectedLanguagesArray[index] as! String)
                tempString.append(",")
            }
            if tempString != ""
            {
                tempString.removeLast()
            }
            txtLanguage.text = tempString
        }
        
        viewTablePopup.isHidden = true
    }
    @IBAction func btnClearTable(_ sender: Any)
    {
        if eth_lan == "eth"
        {
            self.selectedEthnicitiesArray.removeAllObjects()
            txtEthnicities.text = ""
        }
        else
        {
            self.selectedLanguagesArray.removeAllObjects()
            txtLanguage.text = ""
        }
        
        viewTablePopup.isHidden = true
    }
    @IBAction func btnCancelTable(_ sender: Any)
    {
        viewTablePopup.isHidden = true
    }
    
}
extension SearchVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedIndex = row
    }
}
extension SearchVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkboxCell") as! checkboxCell
        
        cell.lblName.text = tableDataArray[indexPath.row]
        
        cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
        if eth_lan == "eth"
        {
            if selectedEthnicitiesArray.contains(tableDataArray[indexPath.row])
            {
                cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
            }
            else
            {
                cell.imgCheckbox.image = UIImage.init(named: "untickbox.png")
            }
        }
        else
        {
            if selectedLanguagesArray.contains(tableDataArray[indexPath.row])
            {
                cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
            }
            else
            {
                cell.imgCheckbox.image = UIImage.init(named: "untickbox.png")
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if eth_lan == "eth"
        {
            if selectedEthnicitiesArray.contains(tableDataArray[indexPath.row])
            {
                selectedEthnicitiesArray.remove(tableDataArray[indexPath.row])
            }
            else
            {
                selectedEthnicitiesArray.add(tableDataArray[indexPath.row])
            }
        }
        else
        {
            if selectedLanguagesArray.contains(tableDataArray[indexPath.row])
            {
                selectedLanguagesArray.remove(tableDataArray[indexPath.row])
            }
            else
            {
                selectedLanguagesArray.add(tableDataArray[indexPath.row])
            }
        }
        
        tblEthnicity.reloadData()
    }
}
