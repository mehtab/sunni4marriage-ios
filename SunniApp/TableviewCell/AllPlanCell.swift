//
//  AllPlanCell.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 21/07/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit

class AllPlanCell: UITableViewCell {

    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblTotalDays: UILabel!
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblDaysRemaining: UILabel!
    @IBOutlet weak var lblPlanType: UILabel!
    @IBOutlet weak var btnGetNow: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPriceTyper: UILabel!
    @IBOutlet weak var PromoCodetext: UITextField!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setActivePlanData(mainDic:NSDictionary)
    {
        if let amount = mainDic["amount"] as? String
        {
            lblPrice.text = "£\(amount)"
        }
        if let plan_days = mainDic["plan_days"] as? String
        {
            lblPriceTyper.text = "\(plan_days) Days"
        }
        if let plan_type = mainDic["plan_type"] as? String
        {
            lblPlanType.text = plan_type
        }
        if let plan_start_date = mainDic["plan_start_date"] as? String
        {
            lblStartDate.text = changeDateFormate_date(date: plan_start_date)
        }
        if let plan_end_date = mainDic["plan_end_date"] as? String
        {
            lblEndDate.text = changeDateFormate_date(date: plan_end_date)
            
            lblDaysRemaining.text = getDaysBetweenTwoDates(endDateString: plan_end_date, dateformate: "yyyy-MM-dd HH:mm:ss")
        }
    }
    
    func changeDateFormate_date(date:String) -> String
    {
        //2019-06-28 12:56:21
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let formattedDate = format.date(from: date)
        {
            format.dateFormat = "dd-MM-yyyy"
            return format.string(from: formattedDate)
        }
        return ""
    }
    func getRemainingDays(date:String) -> String
    {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let formattedDate = format.date(from: date)
        {
            let calendar = Calendar.current
            let ageComponents = calendar.dateComponents([.day], from: formattedDate, to: Date())
            if let age = ageComponents.year {
                return "\(age)"
            } else {
                return ""
            }
        }
        return ""
    }
    func getDaysBetweenTwoDates(endDateString: String, dateformate:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateformate//"yyyy-MM-dd HH:mm:ss"
        let endDate:Date = dateFormatter.date(from: endDateString)!
        
        let calendar = Calendar.current
        
        let date1 = endDate.endOfDay
        let date2 = Date().startOfDay
        let componentsday = calendar.dateComponents([.day], from: date2, to: date1)
                
        var tempString = ""
        
        if componentsday.day == 1 {
            tempString = "\(componentsday.day!) day"
        } else {
            tempString = "\(componentsday.day!) days"
        }
        
        return tempString
    }
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
}
