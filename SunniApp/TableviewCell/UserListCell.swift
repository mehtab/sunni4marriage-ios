//
//  TableViewCell.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 21/07/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit

class UserListCell: UITableViewCell {

    @IBOutlet weak var viewDarkLayerOnImage: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewQus: LetsView!
    @IBOutlet weak var btnRejectReq: UIButton!
    @IBOutlet weak var btnAcceptReq: UIButton!
    @IBOutlet weak var btnViewQus: UIButton!
    @IBOutlet weak var btnBlockUNblock: UIButton!
    @IBOutlet weak var imgBlckUnblock: UIImageView!
    @IBOutlet weak var btnInvitationSet: UIButton!
    @IBOutlet weak var lblInvitationSent: UILabel!
    @IBOutlet weak var imgLikeUnlike: UIImageView!
    @IBOutlet weak var btnLikeUnlike: UIButton!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewPrimium: LetsView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUserListData(mainDic:NSDictionary)
    {
        var avtarImage = UIImage.init(named: placeHolderImages_male)
        if let gender = mainDic["gender"] as? String
        {
            if gender == "FEMALE"
            {
                avtarImage = UIImage.init(named: placeHolderImages_female)
            }
            else
            {
                avtarImage = UIImage.init(named: placeHolderImages_male)
            }
        }
        
        if let name = mainDic["name"] as? String
        {
            let trimmedString = name.trimmingCharacters(in: .whitespaces)
            lblName.text = trimmedString
        }
        else
        {
            lblName.text = ""
        }
        
        var town_city = ""
        if let address = mainDic["town_city"] as? String
        {
            town_city = address
           //lblAddress.text = address
        }
        else
        {
            town_city = ""
            //lblAddress.text = ""
        }
        
        var country = ""
        if let coun = mainDic["country"] as? String
        {
            country = coun
            //lblCountry.text = country
        }
        else
        {
            country = ""
            //lblCountry.text = ""
        }
        lblAddress.text = "\(town_city),\(country)"
        lblCountry.text = ""//not usable
        
        if let dob = mainDic["dob"] as? String
        {
            lblYear.text = date_to_year(date: dob)
        }
        else
        {
            lblYear.text = "0 Year"
        }
        
        //imgProfile.isHidden = false
        viewDarkLayerOnImage.isHidden = false
        if let profilepic = mainDic["profilepic"] as? String
        {
            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(profilepic)"
            imgProfile.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
            //imgProfile.isHidden = false
            viewDarkLayerOnImage.isHidden = false
        }
        else
        {
            //imgProfile.isHidden = true
            imgProfile.image = avtarImage
            viewDarkLayerOnImage.isHidden = true
        }
    }
    func setUserListData_pending(mainDic:NSDictionary)
    {
        if let name = mainDic["name"] as? String
        {
            let trimmedString = name.trimmingCharacters(in: .whitespaces)
            lblName.text = trimmedString
        }
        else
        {
            lblName.text = ""
        }
        
        if let dob = mainDic["dob"] as? String
        {
            lblYear.text = date_to_year(date: dob)
        }
        else
        {
            lblYear.text = "0 Year"
        }
        
        if let occupation = mainDic["occupation"] as? String
        {
            lblAddress.text = occupation
        }
        else
        {
            lblAddress.text = ""
        }
        
        var city = ""
        if let town_city = mainDic["town_city"] as? String
        {
            city = town_city
        }
        var country = ""
        if let country1 = mainDic["country"] as? String
        {
            country = country1
        }
        
        self.lblCountry.text = city + "," + country
        
        if let message = mainDic["message"] as? String
        {
            lblMessage.text = message
        }
        else
        {
            lblAddress.text = ""
        }
    }
    func date_to_year(date:String) -> String
    {
        //11/11/2000
        let format = DateFormatter()
        format.dateFormat = "dd-MM-yyyy"
        if let formattedDate = format.date(from: date)
        {
            let calendar = Calendar.current
            //let ageComponents = calendar.components(.CalendarUnitYear,
            //                                                    fromDate: formattedDate,
            //                                                    toDate: Date(),
            //                                                    options: nil)
            let ageComponents = calendar.dateComponents([.year], from: formattedDate, to: Date())
            let age = ageComponents.year
            
            //            let calendar = Calendar.current
            //            let year = calendar.component(.year, from: formattedDate)
            return "\(age!) Year"
        }
        
        //calendar.component(.month, from: date)
        //calendar.component(.day, from: date)
        
        return "0 Year"
    }
}
