//
//  JBTextView.swift
//
//
//  Created by Lokesh Dudhat on 25/12/15.
//  Copyright © 2015 letsnurture. All rights reserved.
//
import UIKit
public class LetsTextView : UITextView {
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.refreshPlaceholder),
            name: UITextField.textDidChangeNotification,
            object: nil)
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        NotificationCenter.default.addObserver(self, selector: #selector(LetsTextView.refreshPlaceholder), name: UITextView.textDidChangeNotification, object: self)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(LetsTextView.refreshPlaceholder), name: UITextView.textDidChangeNotification, object: self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private var placeholderLabel: UILabel?
//    @IBInspectable  public var placeholderColor : UIColor? {
//        get {
//            return placeholderColor == nil ? self.textColor : placeholderColor!
//        }
//        set {
//            if placeholderLabel != nil {
//                
//            }
//        }
//    }
    /** @abstract To set textView's placeholder text. Default is ni.    */
    @IBInspectable  public var placeholder : String? {
        
        get {
            return placeholderLabel?.text
        }
        
        set {
            
            if placeholderLabel == nil {
                var frm = self.bounds.insetBy(dx: 5, dy: 6)
                frm.size.height = 20
                placeholderLabel = UILabel(frame:frm)
                
                if let unwrappedPlaceholderLabel = placeholderLabel {
                    
                    unwrappedPlaceholderLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    unwrappedPlaceholderLabel.lineBreakMode = .byWordWrapping
                    unwrappedPlaceholderLabel.numberOfLines = 0
                    unwrappedPlaceholderLabel.font = self.font
                    unwrappedPlaceholderLabel.backgroundColor = UIColor.clear
                    unwrappedPlaceholderLabel.textColor = UIColor.gray//UIColor(red: 255/255, green: 190/255, blue: 70/255, alpha: 1.0)
                    unwrappedPlaceholderLabel.alpha = 0
                    addSubview(unwrappedPlaceholderLabel)
                }
            }
            
            placeholderLabel?.text = newValue
            refreshPlaceholder()
        }
    }
    
    @objc func refreshPlaceholder() {
        
        if text.count != 0 {
            placeholderLabel?.alpha = 0
        } else {
            placeholderLabel?.alpha = 1
        }
    }
    
    override public var text: String! {
        
        didSet {
            
            refreshPlaceholder()
            
        }
    }
    
    override public var font : UIFont? {
        
        didSet {
            
            if let unwrappedFont = font {
                placeholderLabel?.font = unwrappedFont
            } else {
                placeholderLabel?.font = UIFont.systemFont(ofSize: 12)
            }
        }
    }
    
    override public var delegate : UITextViewDelegate? {
        
        get {
            refreshPlaceholder()
            return super.delegate
        }
        
        set {
            
        }
    }
}
