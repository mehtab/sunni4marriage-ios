//
//  ChangeIconColor.swift
//  PetsForeverApp
//
//  Created by Nitesh Makwana on 19/11/18.
//  Copyright © 2018 Nitesh Makwana. All rights reserved.
//

import UIKit
@IBDesignable
class ChangeIconColor : UIImageView
{
    @IBInspectable var iconColor: UIColor = UIColor.clear {
        didSet {
            let origImage = self.image
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            self.image = tintedImage
            self.tintColor = iconColor
        }
    }
}
class GradiantIconColor : UIImageView
{
    @IBInspectable var iconColor: UIColor = UIColor.clear {
        didSet {
            let origImage = self.image
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            self.image = tintedImage
            self.tintColor = iconColor
        }
    }
}
