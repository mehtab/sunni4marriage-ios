//
//  GradiantColorView.swift
//  PetsForeverApp
//
//  Created by Nitesh Makwana on 18/11/18.
//  Copyright © 2018 Nitesh Makwana. All rights reserved.
//

import UIKit


@IBDesignable class GradientView: UIView {
    @IBInspectable var firstColor: UIColor = UIColor.white

    @IBInspectable var secondColor: UIColor = UIColor.gray.withAlphaComponent(0.5)

    @IBInspectable var vertical: Bool = false

    lazy var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        layer.startPoint = CGPoint.zero
        return layer
    }()

    //MARK: -

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        applyGradient()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        applyGradient()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        applyGradient()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
    }

    //MARK: -

    func applyGradient() {
        updateGradientDirection()
        layer.sublayers = [gradientLayer]
    }

    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }

    func updateGradientDirection() {
        gradientLayer.endPoint = vertical ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 1)
    }
}

//@IBDesignable
//class GradientView: UIView {
//
//    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
//    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
//    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
//    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
//    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
//    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
//
//    override public class var layerClass: AnyClass { return CAGradientLayer.self }
//
//    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
//
//    func updatePoints() {
//        if horizontalMode {
//            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
//            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
//        } else {
//            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
//            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
//        }
//    }
//    func updateLocations() {
//        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
//    }
//    func updateColors() {
//        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
//    }
//
//    override public func layoutSubviews() {
//        super.layoutSubviews()
//        updatePoints()
//        updateLocations()
//        updateColors()
//    }
//}
