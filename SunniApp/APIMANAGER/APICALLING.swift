//
//  APICALLING.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 21/07/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Alamofire
import Toaster

protocol UserListDelegate : class
{
    func addShortListResponce(index:Int)
    func removeShortlistResponce(index:Int)
    
    func addBlockListResponce(index:Int)
    func removeBlocklistAPIResponce(index:Int)
    
    func showAlertView_delegate(lostconnection_alert:String, msg:String)
    
    func refreshAPIResponce()
}



class UserAPIModel: NSObject
{
    var delegate:UserListDelegate? = nil
    
    
    func addShortListAPI(sl_profile_id:String, index:Int)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "sl_profile_id":sl_profile_id,
            "sl_user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.addShortList.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.delegate?.addShortListResponce(index: index)
                    }
                    else
                    {
                        print("data not found")
                        self.delegate?.showAlertView_delegate(lostconnection_alert: "alert", msg: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.delegate?.showAlertView_delegate(lostconnection_alert: "lostconnection", msg:"")
                }
        }
    }
    func RemoveShortlistAPI(sl_profile_id:String, index:Int)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "profile_id":sl_profile_id,
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.RemoveShortlist.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.delegate?.removeShortlistResponce(index: index)
                    }
                    else
                    {
                        print("data not found")
                        self.delegate?.showAlertView_delegate(lostconnection_alert: "alert", msg: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.delegate?.showAlertView_delegate(lostconnection_alert: "lostconnection", msg:"")
                }
        }
    }
    func addBlockListAPI(sl_profile_id:String, index:Int)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "bl_profile_id":sl_profile_id,
            "bl_user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.addBlockList.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.delegate?.addBlockListResponce(index: index)
                    }
                    else
                    {
                        print("data not found")
                        self.delegate?.showAlertView_delegate(lostconnection_alert: "alert", msg: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.delegate?.showAlertView_delegate(lostconnection_alert: "lostconnection", msg:"")
                }
        }
    }
    func RemoveBlocklistAPI(sl_profile_id:String, index:Int)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "profile_id":sl_profile_id,
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.RemoveBlocklist.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.delegate?.removeBlocklistAPIResponce(index: index)
                        
                    }
                    else
                    {
                        print("data not found")
                        self.delegate?.showAlertView_delegate(lostconnection_alert: "alert", msg: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.delegate?.showAlertView_delegate(lostconnection_alert: "lostconnection", msg:"")
                }
        }
    }
    func cancelRequestAPI(connect_request_user_id:String, connect_request_id:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "connect_request_user_id":connect_request_user_id,
            "status":"1",
            "flag":"friend_list",
            "message":"",
            "connect_request_id":connect_request_id
        ]
        
        let url = MAIN_API_URL+URLs.UpdateFriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.delegate!.refreshAPIResponce()
                    }
                    else
                    {
                        print("data not found")
                        self.delegate?.showAlertView_delegate(lostconnection_alert: "alert", msg: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.delegate?.showAlertView_delegate(lostconnection_alert: "lostconnection", msg:"")
                }
        }
    }
    func showTosterAlert(message:String)
    {
        let toast = Toast(text: message, duration: Delay.long)
        toast.show()
    }
}
