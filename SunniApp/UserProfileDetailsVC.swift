//
//  UserProfileDetailsVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Alamofire

class UserProfileDetailsVC: UIViewController {

    @IBOutlet weak var viewGallery: LetsView!
    @IBOutlet weak var lblSearchingFor: UILabel!
    @IBOutlet weak var lblQ3: UILabel!
    @IBOutlet weak var lblQ2: UILabel!
    @IBOutlet weak var lblQ1: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var viewFirstnameTopConst: NSLayoutConstraint!
    @IBOutlet weak var viewFirstnameHeightConst: NSLayoutConstraint!
    @IBOutlet weak var viewFirstname: UIView!
    @IBOutlet weak var lblEmailRepresentative: UILabel!
    @IBOutlet weak var lblNameOfRepresantive: UILabel!
    @IBOutlet weak var lblChildren: UILabel!
    @IBOutlet weak var lblMarritialStatus: UILabel!
    @IBOutlet weak var viewChildrenTopConst: NSLayoutConstraint!
    @IBOutlet weak var viewChildrenHeightConst: NSLayoutConstraint!
    @IBOutlet weak var viewChildren: UIView!
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var viewBottomTopConst: NSLayoutConstraint!
    @IBOutlet weak var lblBottomMessage: UILabel!
    @IBOutlet weak var viewBottomMessage: LetsView!
    @IBOutlet weak var viewContactDetailHieghtConst: NSLayoutConstraint!//295
    @IBOutlet weak var viewContactDetail: LetsView!
    @IBOutlet weak var lblCDMobile: UILabel!
    @IBOutlet weak var lblCDEmail: UILabel!
    @IBOutlet weak var lblIB2: UILabel!
    @IBOutlet weak var lblAIncome: UILabel!
    @IBOutlet weak var lblAOccupation: UILabel!
    @IBOutlet weak var lblALanguage: UILabel!
    @IBOutlet weak var lblABoby: UILabel!
    @IBOutlet weak var lblAHieght: UILabel!
    @IBOutlet weak var lblPDRegion: UILabel!
    @IBOutlet weak var lblPDBorncity: UILabel!
    @IBOutlet weak var lblPDNationality: UILabel!
    @IBOutlet weak var lblPDAddress: UILabel!
    @IBOutlet weak var lblPDYear: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgBlockUnblock: UIImageView!
    @IBOutlet weak var lblInvitationSent: UILabel!
    @IBOutlet weak var imgLikeUnlike: UIImageView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    
    var user_id = ""
    var flag = ""
    
    var mainDic = NSDictionary()
    
    private var userAPIModel: UserAPIModel = UserAPIModel()
    var tempImageArr = [String]()

    var view_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(mainDic)
        
        userAPIModel.delegate = self
        
        self.view_id = getNumberString(object: mainDic["view_id"] as? NSObject)
        self.getViewProfileAPI()
        
        if let first_name = mainDic["first_name"] as? String
        {
            if first_name == "" {
                lblFirstName.text = "N/A"
            } else {
                lblFirstName.text = first_name
            }
        }
        else
        {
            lblFirstName.text = "N/A"
        }
        
        if let firstname = mainDic["firstname"] as? String
        {
            if firstname == "" {
                lblUsername.text = "N/A"
            } else {
                lblUsername.text = firstname
            }
        } else {
            lblUsername.text = "N/A"
        }
        
        if let unique_user_id = mainDic["unique_user_id"] as? String
        {
            if unique_user_id == "" {
                lblID.text = "N/A"
            } else {
                lblID.text = unique_user_id
            }
        } else {
            lblID.text = "N/A"
        }
        
        if let dob = mainDic["dob"] as? String
        {
            if dob == "" {
                lblPDYear.text = "0 Year"
            } else {
                lblPDYear.text = date_to_year(date: dob)
            }
        }
        else
        {
            lblPDYear.text = "0 Year"
        }
        
        if let town_city = mainDic["town_city"] as? String
        {
            if town_city == "" {
                lblPDAddress.text = "N/A"
            } else {
                lblPDAddress.text = town_city
            }
        } else {
            lblPDAddress.text = "N/A"
        }
        
        if let nationality = mainDic["nationality"] as? String
        {
            if nationality == "" {
                lblPDNationality.text = "N/A"
            } else {
                lblPDNationality.text = nationality
            }
        } else {
            lblPDNationality.text = "N/A"
        }
        
        if let ethnicity = mainDic["ethnicity"] as? String
        {
            if ethnicity == "" {
                lblPDBorncity.text = "N/A"
            } else {
                lblPDBorncity.text = ethnicity
            }
        } else {
            lblPDBorncity.text = "N/A"
        }
        
        if let islamic = mainDic["islamic"] as? String
        {
            if islamic == "" {
                lblPDRegion.text = "N/A"
            } else {
                lblPDRegion.text = islamic
            }
        } else {
            lblPDRegion.text = "N/A"
        }
        
        
        /////////////////////
        /////////////////////
        /////////////////////
        
        if let height = mainDic["tall"] as? String
        {
            if height == "" {
                lblAHieght.text = "N/A"
            } else {
                lblAHieght.text = height
            }
        }
        else
        {
            lblAHieght.text = "N/A"
        }
        
        if let bodytype = mainDic["bodytype"] as? String
        {
            if bodytype == "" {
                lblABoby.text = "N/A"
            } else {
                lblABoby.text = bodytype
            }
        } else {
            lblABoby.text = "N/A"
        }
        
        if let languagesspeak = mainDic["languagesspeak"] as? String
        {
            if languagesspeak == "" {
                lblALanguage.text = "N/A"
            } else {
                lblALanguage.text = languagesspeak
            }
        } else {
            lblALanguage.text = "N/A"
        }
        
        if let occupation = mainDic["occupation"] as? String
        {
            if occupation == "" {
                lblAOccupation.text = "N/A"
            } else {
                lblAOccupation.text = occupation
            }
        } else {
            lblAOccupation.text = "N/A"
        }
        
        if let annualincome = mainDic["annualincome"] as? String
        {
            if annualincome == "" {
                lblAIncome.text = "N/A"
            } else {
                lblAIncome.text = annualincome
            }
        } else {
            lblAIncome.text = "N/A"
        }
        
        if let maritalstatus = mainDic["maritalstatus"] as? String
        {
            if maritalstatus == "" {
                lblMarritialStatus.text = "N/A"
            } else {
                lblMarritialStatus.text = maritalstatus
            }
        } else {
            lblMarritialStatus.text = "N/A"
        }
        
        if lblMarritialStatus.text! == "Divorced" || lblMarritialStatus.text! == "Widowed"
        {
            viewChildren.isHidden = false
            viewChildrenHeightConst.constant = 55
            viewBottomTopConst.constant = 15
            
            if let children = mainDic["children"] as? String
            {
                if children == ""
                {
                    lblChildren.text = "No Data Available For children"
                }
                else
                {
                    lblChildren.text = children
                }
            }
            else
            {
                lblChildren.text = "No Data Available For children"
            }
        }
        else
        {
            viewChildren.isHidden = true
            viewChildrenHeightConst.constant = 0
            viewBottomTopConst.constant = 0
        }
        /////////////////////
        /////////////////////
        /////////////////////
        
        
        if let aboutyourself = mainDic["yourself"] as? String
        {
            if aboutyourself != ""
            {
                lblIB2.text = aboutyourself
            } else {
                lblIB2.text = "N/A"
            }
        } else {
            lblIB2.text = "N/A"
        }
        if let searchingspouse = mainDic["searchingspouse"] as? String
        {
            if searchingspouse != ""
            {
                lblSearchingFor.text = searchingspouse
            } else {
                lblSearchingFor.text = "N/A"
            }
        } else {
            lblSearchingFor.text = "N/A"
        }
        
        self.lblQ1.text = "No questions available"
        self.lblQ2.text = ""
        self.lblQ3.text = ""
        if let question = mainDic["question"] as? String
        {
            if question != ""
            {
                let arrQ = question.components(separatedBy: ",")
                if arrQ.count == 0
                {
                    self.lblQ1.text = "No questions available"
                }
                if arrQ.count == 1
                {
                    self.lblQ1.text = "Question 1 : " + arrQ[0]
                }
                if arrQ.count == 2
                {
                    self.lblQ1.text = "Question 1 : " + arrQ[0]
                    self.lblQ2.text = "Question 2 : " + arrQ[1]
                }
                if arrQ.count == 3
                {
                    self.lblQ1.text = "Question 1 : " + arrQ[0]
                    self.lblQ2.text = "Question 2 : " + arrQ[1]
                    self.lblQ3.text = "Question 3 : " + arrQ[2]
                }
            }
        }
        
        /////////////////////
        /////////////////////
        /////////////////////
        
        
        if let email = mainDic["email"] as? String
        {
            lblCDEmail.text = email
        }
        if let contactnumber = mainDic["contactnumber"] as? String
        {
            lblCDMobile.text = contactnumber
        }
        if let leagalname = mainDic["leagalname"] as? String
        {
            lblNameOfRepresantive.text = leagalname
        }
        if let email_rep = mainDic["email_rep"] as? String
        {
            lblEmailRepresentative.text = email_rep
        }
        
        var avtarImage = UIImage.init(named: placeHolderImages_male)
        if let gender = mainDic["gender"] as? String
        {
            if gender == "FEMALE"
            {
                avtarImage = UIImage.init(named: placeHolderImages_female)
            }
            else
            {
                avtarImage = UIImage.init(named: placeHolderImages_male)
            }
        }
        
        if let profilepic = mainDic["profilepic"] as? String
        {
            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(profilepic)"
            imgProfilePic.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
        }
        else
        {
            imgProfilePic.image = avtarImage
        }
        
        tempImageArr = [String]()
        if let image11 = mainDic["image11"] as? String
        {
            if image11 != ""
            {
                tempImageArr.append(image11)
            }
        }
        if let image12 = mainDic["image12"] as? String
        {
            if image12 != ""
            {
                tempImageArr.append(image12)
            }
        }
        if let image13 = mainDic["image13"] as? String
        {
            if image13 != ""
            {
                tempImageArr.append(image13)
            }
        }
        if let image14 = mainDic["image14"] as? String
        {
            if image14 != ""
            {
                tempImageArr.append(image14)
            }
        }
        
        if tempImageArr.count == 0
        {
            viewGallery.isHidden = true
        }
        else
        {
            viewGallery.isHidden = false
        }
        setImages()
    }
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnGallery(_ sender: Any)
    {
        if tempImageArr.count != 0
        {
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            nextview.imagesArr = tempImageArr
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnLikeUnlike(_ sender: UIButton)
    {
        if let userId = mainDic["userId"] as? String
        {
            if let shortlist_status = mainDic["shortlist_status"] as? Int
            {
                if shortlist_status == 0
                {
                    if let block_status = mainDic["block_status"] as? Int
                    {
                        if block_status == 1
                        {
                            showTosterAlert(message: "Please remove from block first")
                            return
                        }
                    }
                    userAPIModel.addShortListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveShortlistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnInvitationSent(_ sender: Any)
    {
        let user_id = mainDic["userId"] as! String
        let connect_request_id = getNumberString(object: mainDic["connect_request_id"] as? NSObject)
        
        if let question_status = mainDic["question_status"] as? String
        {
            if let request_status = mainDic["request_status"] as? String
            {
                if request_status == "0"
                {
                    //cell.lblInvitationSent.text = "CANCEL REQUEST"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "1"
                {
                    //cell.lblInvitationSent.text = "REMOVE"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "2"
                {
                    //cell.lblInvitationSent.text = "PENDING REQUEST"
                }
                else if request_status == "3"
                {
                    //                    if question_status == "1"
                    //                    {
                    //                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
                    //                        nextview.delegate = self
                    //                        nextview.user_id = user_id
                    //                        nextview.modalPresentationStyle = .overCurrentContext
                    //                        self.present(nextview, animated: true, completion: nil)
                    //                    }
                    //                    else
                    //                    {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                    //                    }
                }
                else
                {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                }
            }
        }
        else
        {
            if let request_status = mainDic["request_status"] as? String
            {
                if request_status == "0"
                {
                    //cell.lblInvitationSent.text = "CANCEL REQUEST"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "1"
                {
                    //cell.lblInvitationSent.text = "REMOVE"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "2"
                {
                    //cell.lblInvitationSent.text = "PENDING REQUEST"
                }
                else if request_status == "3"
                {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                }
                else
                {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                }
            }
        }
    }
    @IBAction func btnBlockUnblck(_ sender: UIButton)
    {
        if let userId = mainDic["userId"] as? String
        {
            if let block_status = mainDic["block_status"] as? Int
            {
                if block_status == 0
                {
                    userAPIModel.addBlockListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveBlocklistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    
    func date_to_year(date:String) -> String
    {
        //11/11/2000
        let format = DateFormatter()
        format.dateFormat = "dd-MM-yyyy"
        if let formattedDate = format.date(from: date)
        {
            let calendar = Calendar.current
            //let ageComponents = calendar.components(.CalendarUnitYear,
            //                                                    fromDate: formattedDate,
            //                                                    toDate: Date(),
            //                                                    options: nil)
            let ageComponents = calendar.dateComponents([.year], from: formattedDate, to: Date())
            let age = ageComponents.year
            
            //            let calendar = Calendar.current
            //            let year = calendar.component(.year, from: formattedDate)
            return "\(age!) Year"
        }
        
        //calendar.component(.month, from: date)
        //calendar.component(.day, from: date)
        
        return "0 Year"
    }
    func setImages()
    {
        if let shortlist_status = mainDic["shortlist_status"] as? Int
        {
            if shortlist_status == 0
            {
                imgLikeUnlike.image = UIImage.init(named: "unlike.png")
            }
            else
            {
                imgLikeUnlike.image = UIImage.init(named: "like.png")
            }
        }
        else
        {
            imgLikeUnlike.image = UIImage.init(named: "unlike.png")
        }
        
        if let block_status = mainDic["block_status"] as? Int
        {
            if block_status == 0
            {
                imgBlockUnblock.image = UIImage.init(named: "unblock.png")
            }
            else
            {
                imgBlockUnblock.image = UIImage.init(named: "block.png")
            }
        }
        else
        {
            imgBlockUnblock.image = UIImage.init(named: "unblock.png")
        }
        
        
        if let request_status = mainDic["request_status"] as? String
        {
            if request_status == "0"
            {
                lblInvitationSent.text = "CANCEL REQUEST"
                
                viewContactDetail.isHidden = true
                viewContactDetailHieghtConst.constant = 0
                viewBottomTopConst.constant = 0
                
                viewFirstname.isHidden = true
                viewFirstnameHeightConst.constant = 0
                viewFirstnameTopConst.constant = 0
            }
            else if request_status == "1"
            {
                lblInvitationSent.text = "REMOVE"
                
                viewBottomMessage.isHidden = true
                lblBottomMessage.text = ""
                viewContactDetail.isHidden = false
                viewContactDetailHieghtConst.constant = 225
                viewBottomTopConst.constant = 0
                
                viewFirstname.isHidden = false
                viewFirstnameHeightConst.constant = 55
                viewFirstnameTopConst.constant = 15
            }
            else if request_status == "2"
            {
                lblInvitationSent.text = "PENDING REQUEST"
                
                viewContactDetail.isHidden = true
                viewContactDetailHieghtConst.constant = 0
                viewBottomTopConst.constant = 0
                
                viewFirstname.isHidden = true
                viewFirstnameHeightConst.constant = 0
                viewFirstnameTopConst.constant = 0
            }
            else if request_status == "3"
            {
                lblInvitationSent.text = "SEND REQUEST"
                
                viewContactDetail.isHidden = true
                viewContactDetailHieghtConst.constant = 0
                viewBottomTopConst.constant = 0
                
                viewFirstname.isHidden = true
                viewFirstnameHeightConst.constant = 0
                viewFirstnameTopConst.constant = 0
            }
            else
            {
                lblInvitationSent.text = "SEND REQUEST"
                
                viewContactDetail.isHidden = true
                viewContactDetailHieghtConst.constant = 0
                viewBottomTopConst.constant = 0
                
                viewFirstname.isHidden = true
                viewFirstnameHeightConst.constant = 0
                viewFirstnameTopConst.constant = 0
            }
        }
        else
        {
            lblInvitationSent.text = "SEND REQUEST"
            
            viewContactDetail.isHidden = true
            viewContactDetailHieghtConst.constant = 0
            viewBottomTopConst.constant = 0
            
            viewFirstname.isHidden = true
            viewFirstnameHeightConst.constant = 0
            viewFirstnameTopConst.constant = 0
        }
    }
}
extension UserProfileDetailsVC : UserListDelegate
{
    func refreshAPIResponce() {
        
    }
    func showAlertView_delegate(lostconnection_alert: String, msg: String) {
        if lostconnection_alert == "lostconnection"
        {
            self.lostInternetConnectionAlert()
        }
        else
        {
            self.showAlertView(message: msg)
        }
    }
    
    func addShortListResponce(index: Int)
    {
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "shortlist_status")
        
        mainDic = tempMainDic
        
        setImages()
    }
    func removeShortlistResponce(index: Int)
    {
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "shortlist_status")
        
        mainDic = tempMainDic
        
        setImages()
    }
    func addBlockListResponce(index: Int)
    {
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "block_status")
        
        mainDic = tempMainDic
        
        setImages()
    }
    func removeBlocklistAPIResponce(index: Int)
    {
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "block_status")
        
        mainDic = tempMainDic
        
        setImages()
    }
}
extension UserProfileDetailsVC
{
    func getProfileAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":user_id,
            "flag":flag //friend_list, search_friend, blocktlist_data, shortlist_data, pending_request, sent_request
        ]
        
        
        let url = MAIN_API_URL+URLs.FriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    func getViewProfileAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "view_id": view_id
        ]
        
        
        let url = MAIN_API_URL+URLs.viewProfile.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
