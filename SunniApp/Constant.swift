
/*
request_status = ""     -> not connect  -> button name=CONNECT
request_status = "0"    -> connected    -> button name=INVITATION SENT
request_status = "1"    -> accepted     -> button name=REMOVE (only accepted request screen)
 
 */







import UIKit
import Toaster
import AVFoundation


//http://diestechnology.com.au/projects/sunnis4marriage/app_controller/paypal/
//"http://diestechnology.com.au/projects/sunnis4marriage/app_controller/paypal/cancel"
//"http://diestechnology.com.au/projects/sunnis4marriage/app_controller/paypal/success"

//let MAIN_API_URL = "http://diestechnology.com.au/projects/sunnis4marriage/app_controller/"
//let MAIN_API_IMAGE_URL = "http://diestechnology.com.au/projects/sunnis4marriage/"
//let MAIN_API_IMAGE_URL_no_upload = "http://diestechnology.com.au/projects/sunnis4marriage/"

let MAIN_API_URL = "https://sunnis4marriage.com/Appadmin/app_controller/"
let MAIN_API_IMAGE_URL = "https://sunnis4marriage.com/Appadmin/"
let MAIN_API_IMAGE_URL_no_upload = "https://sunnis4marriage.com/Appadmin/"

//let PAYPAL_WEBVIEW_URL = "https://sunnis4marriage.com/Appadmin/app_controller/paypal/"
let PAYPAL_WEBVIEW_URL = "https://sunnis4marriage.com/Appadmin/app_controller/paypal"
let PAYPAL_CANCEL_URL = "https://sunnis4marriage.com/Appadmin/app_controller/paypal/cancel"
let PAYPAL_SUCCESS_URL = "https://sunnis4marriage.com/Appadmin/app_controller/paypal/success"

var placeHolderImages = "logo.png"
var placeHolderImages_male = "ic_male_avtar.png"
var placeHolderImages_female = "ic_female_avatar.png"

var deviceTokenID = "This is iPhoneSimulator"

let appDelegate = UIApplication.shared.delegate as! AppDelegate

var isFrom_UserData = ""

var latitude = ""
var longitude = ""

struct IN_APP_PURCHASE {
    static let mc_currency = "GBP"
    static let mc_gross = "69.00"
    static let inAppPurchaseSecretKey = "49dfd10249ad4fa585ec6eb243b016ea"
    static let inAppPurchaseProductId = "membership_plan_2"
}

enum URLs: String
{
    case register = "UserData/registernew"
    case login = "UserData/login"
    case getUserData = "UserData/getUserData"
    
    case profileupdatestep1 = "UserData/profileupdatestep1"
    case profileupdatestep2 = "UserData/profileupdatestep2"
    case profileupdatestep3 = "UserData/profileupdatestep3"
    case profileupdatestep4 = "UserData/profileupdatestep4"
    case profileupdatestep5 = "UserData/profileupdatestep5"
    case profileupdatestep6 = "UserData/profileupdatestep6"
    
    case getUserDataForEdit = "UserData/getUserDataForEdit"
    case getUserDataForStep2 = "UserData/getUserDataForStep2"
    case getUserDataForStepthree = "UserData/getUserDataForStepthree"
    case getUserDataForStepfour = "UserData/getUserDataForStepfour"
    case getUserDataForStepfive = "UserData/getUserDataForStepfive"
    case getUserDataForStepsix = "UserData/getUserDataForStepsix"
    
    case CheckFilterData = "UserData/CheckFilterData"
    case getVisitedUsers = "UserData/getVisitedUsers"
    
    case addShortList = "connect/ShortList"
    case RemoveShortlist = "connect/RemoveShortlist"
    
    case addBlockList = "connect/BlockList"
    case RemoveBlocklist = "connect/RemoveBlocklist"
    
    case getQuestins = "UserData/getQuestionsByUserId"
    case addQuestion = "UserData/addQuestion"
    case submitAnswer = "UserData/submitAnswer"
    case getQueAnsByUserId = "UserData/getQueAnsByUserId"
    case deleteQuestion = "UserData/deleteQuestion"
    
    case FriendData = "connect/FriendData"
    
    case getPaymentHistory = "UserData/getPaymentHistory"
    
    case getDistance = "UserData/getDistance"
    case suggestionFilterDataByAge = "UserData/suggestionFilterDataByAge"
    case getnewmiles = "UserData/getnewmiles"
    
    case getPlanStatusView = "UserData/getPlanStatusView"
    case getPlan = "UserData/getPlan"
    
    case UpdateFriendData = "connect/UpdateFriendData"
    case UploadProfileImage = "UserData/Upload_ProfileImage"
    
    case changePassword = "UserData/changePassword"
    case user_delete_profile = "UserData/user_delete_profile"
    
    case setPaymentInfo = "UserData/setPaymentInfo"
    
    case addChatMessage = "Chat_message/addChatMessage"
    
    case getOTP = "UserData/getOTP"
    case CheckOTPMail = "UserData/CheckOTPMail"
    case updatePwd = "UserData/updatePwd"
    case getNotificationData = "UserData/getNotificationData"
    case RemoveImage = "UserData/RemoveImage"
    case getStates = "UserData/getStates"
    case iosPaymentInfo = "UserData/iosPaymentInfo"
    case viewProfile = "UserData/viewProfile"
    case CheckIdWiseFilterData = "UserData/CheckIdWiseFilterData"
    
}
extension UIView {
    func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}
extension UIViewController
{
    func getNumberString(object:NSObject?) -> String
    {
        var message = ""
        if let obj = object as? String {
            message = obj
        } else if let obj = object as? NSNumber {
            message = obj.stringValue
        }
        return message
    }
    func getBoolValue(object:NSObject?) -> Bool {
        var message = false
        if let obj = object as? String {
            let obj111 = obj.lowercased()
            if obj111 == "false" || obj111 == "0"{
                message = false
            } else {
                message = true
            }
        } else if let obj = object as? NSNumber {
            if obj.intValue == 0 {
                message = false
            } else {
                message = true
            }
        } else if let obj = object as? Bool {
            if obj == false {
                message = false
            } else {
                message = true
            }
        } else {
            message = false
        }
        return message
    }
    func showTosterAlert(message:String)
    {
        let toast = Toast(text: message, duration: Delay.long)
        toast.show()
    }
    
    func showTosterAlertText(message:String)
    {
        let toast = Toast(text: message, duration: Delay.long)
        toast.textColor = UIColor.red
        toast.backgroundColor = .clear
        toast.show()
    }
    
    
    func showAlertViewTextcolrChange(message:String)
    {
        let alertController = UIAlertController(title: "Alert", message: nil, preferredStyle: .alert)
        alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.foregroundColor : UIColor.red]), forKey: "attributedTitle")
       

        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        alertController.addAction(okAction)
       // alertController.view.tintColor = UIColor.red

        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertView(message:String)
    {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func lostInternetConnectionAlert()
    {
        let alertController = UIAlertController(title: "Alert", message: "Server is slow down Please try Again", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func addSubview(subView:UIView, toView parentView:UIView)
    {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
    }
    
    
    func changeDateFormate_date(date:String) -> String
    {
        //2019-06-28 12:56:21
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        format.calendar = .current
        if let formattedDate = format.date(from: date)
        {
            format.dateFormat = "yyyy-MM-dd"
            return format.string(from: formattedDate)
        }
        return ""
    }
    func changeDateFormate_time(date:String) -> String
    {
        //2019-06-28 12:56:21
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        format.calendar = .current
        if let formattedDate = format.date(from: date)
        {
            format.dateFormat = "HH:mm:ss"
            return format.string(from: formattedDate)
        }
        return ""
    }
    
 }
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(data: data, options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
            ], documentAttributes: nil) else { return nil }
        return html
    }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
