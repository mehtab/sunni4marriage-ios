//
//  CharacterProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 30/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import DLRadioButton

class checkboxCell: UITableViewCell
{
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgCheckbox: UIImageView!
}
class CharacterProfileVC: UIViewController {

    
    @IBOutlet weak var tblCheckbox: UITableView!
    @IBOutlet weak var viewTablePopup: UIView!
    @IBOutlet weak var imgCheckbox5: UIImageView!
    @IBOutlet weak var imgCheckbox4: UIImageView!
    @IBOutlet weak var imgCheckbox3: UIImageView!
    @IBOutlet weak var imgCheckbox2: UIImageView!
    @IBOutlet weak var imgCheckbox1: UIImageView!
    @IBOutlet weak var txtTraitSpouse: UITextField!
    @IBOutlet weak var txtDescribeYourself: UITextField!
    
    var strCheckbox1 = "0"
    var strCheckbox2 = "0"
    var strCheckbox3 = "0"
    var strCheckbox4 = "0"
    var strCheckbox5 = "0"
    
    var isQuestion1 = ""
    var isQuestion2 = ""
    var isQuestion3 = ""
    var isQuestion4 = ""
    var isQuestion5 = ""
    var isQuestion6 = ""
    var isQuestion7 = ""
    var isQuestion8 = ""
    var isQuestion9 = ""
    var isQuestion10 = ""
    var isQuestion11 = ""
    
    var pickerArr = [String]()
    var selectedArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerArr = getCharacterList()
        tblCheckbox.reloadData()
        
        getUserDataForStepthreeAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDescribeYouself(_ sender: Any)
    {
        viewTablePopup.isHidden = false
    }
    @IBAction func btnCheckbox1(_ sender: Any)
    {
        if strCheckbox1 == "0"
        {
            strCheckbox1 = "1"
            imgCheckbox1.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox1 = "0"
            imgCheckbox1.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox2(_ sender: Any)
    {
        if strCheckbox2 == "0"
        {
            strCheckbox2 = "1"
            imgCheckbox2.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox2 = "0"
            imgCheckbox2.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox3(_ sender: Any)
    {
        if strCheckbox3 == "0"
        {
            strCheckbox3 = "1"
            imgCheckbox3.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox3 = "0"
            imgCheckbox3.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox4(_ sender: Any)
    {
        if strCheckbox4 == "0"
        {
            strCheckbox4 = "1"
            imgCheckbox4.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox4 = "0"
            imgCheckbox4.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox5(_ sender: Any)
    {
        if strCheckbox5 == "0"
        {
            strCheckbox5 = "1"
            imgCheckbox5.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox5 = "0"
            imgCheckbox5.image = UIImage.init(named: "untickbox.png")
        }
    }
    
    
    @IBAction func btnQuestion1(_ sender: DLRadioButton)
    {
        isQuestion1 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion2(_ sender: DLRadioButton)
    {
        isQuestion2 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion3(_ sender: DLRadioButton)
    {
        isQuestion3 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion4(_ sender: DLRadioButton)
    {
        isQuestion4 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion5(_ sender: DLRadioButton)
    {
        isQuestion5 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion6(_ sender: DLRadioButton)
    {
        isQuestion6 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion7(_ sender: DLRadioButton)
    {
        isQuestion7 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion8(_ sender: DLRadioButton)
    {
        isQuestion8 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion9(_ sender: DLRadioButton)
    {
        isQuestion9 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion10(_ sender: DLRadioButton)
    {
        isQuestion10 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion11(_ sender: DLRadioButton)
    {
        isQuestion11 = sender.titleLabel!.text!
    }
    @IBAction func btnNext(_ sender: Any)
    {
        profileupdatestep3API()
    }
    @IBAction func btnDoneTablePopup(_ sender: Any)
    {
        var tempString = ""
        
        for(index,_) in selectedArr.enumerated()
        {
            tempString.append(selectedArr[index] as! String)
            tempString.append(",")
        }
        if tempString != ""
        {
            tempString.removeLast()
        }
        txtDescribeYourself.text = tempString
        viewTablePopup.isHidden = true
    }
    @IBAction func btnCancelTablePopup(_ sender: Any)
    {
        viewTablePopup.isHidden = true
    }
    
    func profileupdatestep3API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "howbestwould":txtDescribeYourself.text!,
            //"traitswouldyouwant":txtTraitSpouse.text!,
            "questionisimportant1":strCheckbox1,
            "youexpectyourspousetodothings":isQuestion1,
            "Yourspousewouldsay2":isQuestion2,
            "questionisimportant2":strCheckbox2,
            "howimportantisittoyou":isQuestion3,
            "Yourspousewouldsay3":isQuestion4,
            "questionisimportant3":strCheckbox3,
            "whichdescribes":isQuestion5,
            "Yourspousewouldsay4":isQuestion6,
            "questionisimportant4":strCheckbox4,
            "describesyoubest5":isQuestion7,
            "Yourspousewouldsay5":isQuestion8,
            "questionisimportant5":strCheckbox5,
            "areyou6":isQuestion9,
            "areyou7":isQuestion10,
            "Yourspousewouldsay7":isQuestion11,
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        
        let url = MAIN_API_URL+URLs.profileupdatestep3.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ReligionProfileVC") as! ReligionProfileVC
                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getUserDataForStepthreeAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserDataForStepthree.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        self.txtDescribeYourself.text = dicarr["howbestwould"] as? String
                        //self.txtTraitSpouse.text = dicarr["traitswouldyouwant"] as? String
                        
                        
                        /////////////////
                        self.isQuestion1 = dicarr["youexpectyourspousetodothings"] as? String ?? ""
                        if dicarr["youexpectyourspousetodothings"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youexpectyourspousetodothings"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youexpectyourspousetodothings"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(103) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion2 = dicarr["Yourspousewouldsay1"] as? String ?? ""
                        if dicarr["Yourspousewouldsay1"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay1"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay1"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(203) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion3 = dicarr["howimportantisittoyou"] as? String ?? ""
                        if dicarr["howimportantisittoyou"] as? String == "Very"
                        {
                            if let button = self.view.viewWithTag(301) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["howimportantisittoyou"] as? String == "Somewhat"
                        {
                            if let button = self.view.viewWithTag(302) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["howimportantisittoyou"] as? String == "Not at all"
                        {
                            if let button = self.view.viewWithTag(303) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion4 = dicarr["Yourspousewouldsay2"] as? String ?? ""
                        if dicarr["Yourspousewouldsay2"] as? String == "Very"
                        {
                            if let button = self.view.viewWithTag(401) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay2"] as? String == "Somewhat"
                        {
                            if let button = self.view.viewWithTag(402) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay2"] as? String == "Not at all"
                        {
                            if let button = self.view.viewWithTag(403) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion5 = dicarr["whichdescribes"] as? String ?? ""
                        if dicarr["whichdescribes"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(501) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["whichdescribes"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(502) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion6 = dicarr["Yourspousewouldsay3"] as? String ?? ""
                        if dicarr["Yourspousewouldsay3"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(601) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay3"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(602) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion7 = dicarr["describesyoubest5"] as? String ?? ""
                        if dicarr["describesyoubest5"] as? String == "Ambitious"
                        {
                            if let button = self.view.viewWithTag(701) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["describesyoubest5"] as? String == "Easy Going"
                        {
                            if let button = self.view.viewWithTag(702) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion8 = dicarr["Yourspousewouldsay4"] as? String ?? ""
                        if dicarr["Yourspousewouldsay4"] as? String == "Ambitious"
                        {
                            if let button = self.view.viewWithTag(801) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay4"] as? String == "Easy Going"
                        {
                            if let button = self.view.viewWithTag(802) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion9 = dicarr["areyou6"] as? String ?? ""
                        if dicarr["areyou6"] as? String == "Shy and nervous with new people"
                        {
                            if let button = self.view.viewWithTag(901) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["areyou6"] as? String == "open and unreserved with new people"
                        {
                            if let button = self.view.viewWithTag(902) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion10 = dicarr["areyou7"] as? String ?? ""
                        if dicarr["areyou7"] as? String == "Conservative"
                        {
                            if let button = self.view.viewWithTag(1001) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["areyou7"] as? String == "Progressive"
                        {
                            if let button = self.view.viewWithTag(1002) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion11 = dicarr["Yourspousewouldsay5"] as? String ?? ""
                        if dicarr["Yourspousewouldsay5"] as? String == "Conservative"
                        {
                            if let button = self.view.viewWithTag(1101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["Yourspousewouldsay5"] as? String == "Progressive"
                        {
                            if let button = self.view.viewWithTag(1102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.strCheckbox1 = dicarr["questionisimportant1"] as? String ?? ""
                        if dicarr["questionisimportant1"] as? String == "0"
                        {
                            self.imgCheckbox1.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant1"] as? String == "1"
                        {
                            self.imgCheckbox1.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox2 = dicarr["questionisimportant2"] as? String ?? ""
                        if dicarr["questionisimportant2"] as? String == "0"
                        {
                            self.imgCheckbox2.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant2"] as? String == "1"
                        {
                            self.imgCheckbox2.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox3 = dicarr["questionisimportant3"] as? String ?? ""
                        if dicarr["questionisimportant3"] as? String == "0"
                        {
                            self.imgCheckbox3.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant3"] as? String == "1"
                        {
                            self.imgCheckbox3.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox4 = dicarr["questionisimportant4"] as? String ?? ""
                        if dicarr["questionisimportant4"] as? String == "0"
                        {
                            self.imgCheckbox4.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant4"] as? String == "1"
                        {
                            self.imgCheckbox4.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox5 = dicarr["questionisimportant5"] as? String ?? ""
                        if dicarr["questionisimportant5"] as? String == "0"
                        {
                            self.imgCheckbox5.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant5"] as? String == "1"
                        {
                            self.imgCheckbox5.image = UIImage.init(named: "tickbox.png")
                        }
                        
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension CharacterProfileVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return pickerArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkboxCell") as! checkboxCell
        
        cell.lblName.text = pickerArr[indexPath.row]
        
        cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
        if selectedArr.contains(pickerArr[indexPath.row])
        {
            cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            cell.imgCheckbox.image = UIImage.init(named: "untickbox.png")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if selectedArr.contains(pickerArr[indexPath.row])
        {
            selectedArr.remove(pickerArr[indexPath.row])
        }
        else
        {
            selectedArr.add(pickerArr[indexPath.row])
        }
        tblCheckbox.reloadData()
    }
}
