//
//  PersonalProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire
import SVProgressHUD

class PersonalProfileVC: UIViewController {
    @IBOutlet weak var lblState: UITextField!
    @IBOutlet weak var txtNickname: UITextField!

    @IBOutlet weak var pickerviewRadio: UIPickerView!
    @IBOutlet weak var viewPickerPopup: UIView!
    
    @IBOutlet weak var txtContactnoLegal: UITextField!
    @IBOutlet weak var txtEmailLegal: UITextField!
    @IBOutlet weak var txtReletionshipLegal: UITextField!
    @IBOutlet weak var txtNameLegal: UITextField!
    @IBOutlet weak var txtEthnicity: UITextField!
    @IBOutlet weak var txtNationality: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    
    var isHaritage = ""
    
    var countryArr = [String]()
    var nationalityArr = [String]()
    var pickerArray = [String]()
    var stateArray = [String]()
    var ethnicityArr = [String]()
    
    var selectedIndex = 0
    var cou_nat_state_eth = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countryArr = getCountryNameList()
        nationalityArr = getNationalityArray()
        ethnicityArr = getEthnicities()
        
        self.pickerviewRadio.reloadAllComponents()
        
        getUserDataForEditAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCountry(_ sender: Any)
    {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPostalCode.resignFirstResponder()
        txtNationality.resignFirstResponder()
        txtNameLegal.resignFirstResponder()
        txtReletionshipLegal.resignFirstResponder()
        txtEmailLegal.resignFirstResponder()
        txtContactnoLegal.resignFirstResponder()
        
        
        pickerArray = countryArr
        selectedIndex = 0
        cou_nat_state_eth = "cou"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnSelectState(_ sender: Any) {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPostalCode.resignFirstResponder()
        txtNationality.resignFirstResponder()
        txtNameLegal.resignFirstResponder()
        txtReletionshipLegal.resignFirstResponder()
        txtEmailLegal.resignFirstResponder()
        txtContactnoLegal.resignFirstResponder()
        
        
        pickerArray = stateArray
        selectedIndex = 0
        cou_nat_state_eth = "state"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnSelectNationality(_ sender: Any)
    {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPostalCode.resignFirstResponder()
        txtNationality.resignFirstResponder()
        txtNameLegal.resignFirstResponder()
        txtReletionshipLegal.resignFirstResponder()
        txtEmailLegal.resignFirstResponder()
        txtContactnoLegal.resignFirstResponder()
        
        
        pickerArray = nationalityArr
        selectedIndex = 0
        cou_nat_state_eth = "nat"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnSelectEthnicity(_ sender: Any)
    {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPostalCode.resignFirstResponder()
        txtNationality.resignFirstResponder()
        txtNameLegal.resignFirstResponder()
        txtReletionshipLegal.resignFirstResponder()
        txtEmailLegal.resignFirstResponder()
        txtContactnoLegal.resignFirstResponder()
        
        
        pickerArray = ethnicityArr
        selectedIndex = 0
        cou_nat_state_eth = "eth"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnSubmit(_ sender: Any)
    {
        if txtFirstName.text == ""
        {
            showTosterAlert(message: "Please enter nick name")
        }
        else if txtFirstName.text == ""
        {
            showTosterAlert(message: "Please enter first name")
        }
        else if txtLastName.text == ""
        {
            showTosterAlert(message: "Please enter last name")
        }
        else if txtAddress.text == ""
        {
            showTosterAlert(message: "Please enter address")
        }
        else if txtCountry.text == ""
        {
            showTosterAlert(message: "Please enter country")
        }
        else if txtCity.text == ""
        {
            showTosterAlert(message: "Please enter city")
        }
        else if txtPostalCode.text == ""
        {
            showTosterAlert(message: "Please enter postal code")
        }
        else if txtNationality.text == ""
        {
            showTosterAlert(message: "Please select nationality")
        }
//        else if txtEthnicity.text == ""
//        {
//            showTosterAlert(message: "Please enter ethnivity")
//        }
        else if isHaritage == ""
        {
            showTosterAlert(message: "Please choose islamic haritage")
        }
//        else if txtNameLegal.text == ""
//        {
//            showTosterAlert(message: "Please enter The Name of Your Legal Representative")
//        }
//        else if txtReletionshipLegal.text == ""
//        {
//            showTosterAlert(message: "Please enter Relationship To Legal Representative")
//        }
//        else if txtEmailLegal.text == ""
//        {
//            showTosterAlert(message: "Please enter Your Legal Representative's Email")
//        }
//        else if txtContactnoLegal.text == ""
//        {
//            showTosterAlert(message: "Please enter Your Legal Representative's Contact No")
//        }
        else
        {
            txtFirstName.resignFirstResponder()
            txtLastName.resignFirstResponder()
            txtAddress.resignFirstResponder()
            txtCountry.resignFirstResponder()
            txtCity.resignFirstResponder()
            txtPostalCode.resignFirstResponder()
            txtNationality.resignFirstResponder()
            txtNameLegal.resignFirstResponder()
            txtReletionshipLegal.resignFirstResponder()
            txtEmailLegal.resignFirstResponder()
            txtContactnoLegal.resignFirstResponder()
            
            profileupdatestep1API()
        }
    }
    @IBAction func rbGender(_ radioButton: DLRadioButton)
    {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPostalCode.resignFirstResponder()
        txtNationality.resignFirstResponder()
        txtNameLegal.resignFirstResponder()
        txtReletionshipLegal.resignFirstResponder()
        txtEmailLegal.resignFirstResponder()
        txtContactnoLegal.resignFirstResponder()
        
        
        isHaritage = radioButton.titleLabel!.text!
        
//        if (radioButton.isMultipleSelectionEnabled)
//        {
//            for button in radioButton.selectedButtons()
//            {
//                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
//            }
//        }
//        else
//        {
//            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
//        }
    }
    @IBAction func btnDonePickerPopup(_ sender: Any)
    {
        if cou_nat_state_eth == "cou"
        {
            txtCountry.text = countryArr[selectedIndex]
        }
        else if cou_nat_state_eth == "nat"
        {
            txtNationality.text = nationalityArr[selectedIndex]
        }
        else if cou_nat_state_eth == "eth"
        {
            txtEthnicity.text = ethnicityArr[selectedIndex]
        }
        else
        {
            lblState.text = stateArray[selectedIndex]
        }
        
        viewPickerPopup.isHidden = true
    }
    @IBAction func btnCancelPickerPopup(_ sender: Any)
    {
        viewPickerPopup.isHidden = true
    }
    func profileupdatestep1API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "first_name":txtFirstName.text!,
            "firstname":txtNickname.text!,
            "lastname":txtLastName.text!,
            "address":txtAddress.text!,
            "town_city":txtCity.text!,
            "country":txtCountry.text!,
            "islamic":isHaritage,
            "postcode":txtPostalCode.text!,
            "nationality":txtNationality.text!,
            "ethnicity":txtEthnicity.text!,
            "leagalname":txtNameLegal.text!,
            "relationship":txtReletionshipLegal.text!,
            "emailaddress":txtEmailLegal.text!,
            "contactnumber":txtContactnoLegal.text!,
            "state":lblState.text!,
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        
        let url = MAIN_API_URL+URLs.profileupdatestep1.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        if let name = self.txtNickname.text
                        {
                            UserDefaults.standard.set(name, forKey: "name")
                        }
                        if let first_name = self.txtFirstName.text
                        {
                            UserDefaults.standard.set(first_name, forKey: "first_name")
                        }
                        if let last_name = self.txtLastName.text
                        {
                            UserDefaults.standard.set(last_name, forKey: "last_name")
                        }
                        
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        self.navigationController?.popViewController(animated: true)
//                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "AboutYouProfileVC") as! AboutYouProfileVC
//                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getUserDataForEditAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserDataForEdit.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                self.getStatesAPI()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        self.txtFirstName.text = dicarr["first_name"] as? String ?? ""
                        self.txtNickname.text = dicarr["firstname"] as? String ?? ""
                        self.txtLastName.text = dicarr["lastname"] as? String ?? ""
                        self.txtAddress.text = dicarr["address"] as? String ?? ""
                        self.txtCountry.text = dicarr["country"] as? String ?? ""
                        self.txtCity.text = dicarr["town_city"] as? String ?? ""
                        self.txtPostalCode.text = dicarr["postcode"] as? String ?? ""
                        self.txtNationality.text = dicarr["nationality"] as? String ?? ""
                        self.txtEthnicity.text = dicarr["ethnicity"] as? String ?? ""
                        self.txtNameLegal.text = dicarr["leagalname"] as? String ?? ""
                        self.txtReletionshipLegal.text = dicarr["relationship"] as? String ?? ""
                        self.txtEmailLegal.text = dicarr["email"] as? String ?? ""
                        self.txtContactnoLegal.text = dicarr["contactnumber"] as? String ?? ""
                        self.lblState.text = dicarr["state"] as? String ?? ""
                        
                        self.isHaritage = dicarr["islamic"] as? String ?? ""
                        if dicarr["islamic"] as? String == "Born into Islam"
                        {
                            if let button1 = self.view.viewWithTag(101) as? DLRadioButton
                            {
                                button1.isSelected = true
                            }
                        }
                        else if dicarr["islamic"] as? String == "Reverted to Islam"
                        {
                            if let button1 = self.view.viewWithTag(102) as? DLRadioButton
                            {
                                button1.isSelected = true
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    func getStatesAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let url = MAIN_API_URL+URLs.getStates.rawValue
        print("\n\n\n\n\n")
        print(url)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let dataArr = mainResponce["data"] as! NSArray
                        self.stateArray = [String]()
                        for dataDic in dataArr {
                            if let mainDic = dataDic as? NSDictionary {
                                if let stateName = mainDic["state_name"] as? String {
                                    self.stateArray.append(stateName)
                                }
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension PersonalProfileVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedIndex = row
    }
}


