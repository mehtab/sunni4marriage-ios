//
//  EditProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPersonal(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PersonalProfileVC") as! PersonalProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnAboutYou(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "AboutYouProfileVC") as! AboutYouProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnCharacter(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "CharacterProfileVC") as! CharacterProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnReligion(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ReligionProfileVC") as! ReligionProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnFamily(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "FamilyProfileVC") as! FamilyProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnPreference(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesProfileVC") as! PreferencesProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnQuestions(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "QuestionsVC") as! QuestionsVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
}
