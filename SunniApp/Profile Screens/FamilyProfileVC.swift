//
//  FamilyProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 31/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire
import SVProgressHUD

class FamilyProfileVC: UIViewController {
    @IBOutlet weak var imgCheckbox9: UIImageView!
    @IBOutlet weak var imgCheckbox8: UIImageView!
    @IBOutlet weak var imgCheckbox7: UIImageView!
    @IBOutlet weak var imgCheckbox6: UIImageView!
    @IBOutlet weak var imgCheckbox5: UIImageView!
    @IBOutlet weak var imgCheckbox4: UIImageView!
    @IBOutlet weak var imgCheckbox3: UIImageView!
    @IBOutlet weak var imgCheckbox2: UIImageView!
    @IBOutlet weak var imgCheckbox1: UIImageView!
    
    var strCheckbox1 = "0"
    var strCheckbox2 = "0"
    var strCheckbox3 = "0"
    var strCheckbox4 = "0"
    var strCheckbox5 = "0"
    var strCheckbox6 = "0"
    var strCheckbox7 = "0"
    var strCheckbox8 = "0"
    var strCheckbox9 = "0"
    
    var isQuestion1 = ""
    var isQuestion2 = ""
    var isQuestion3 = ""
    var isQuestion4 = ""
    var isQuestion5 = ""
    var isQuestion6 = ""
    var isQuestion7 = ""
    var isQuestion8 = ""
    var isQuestion9 = ""
    var isQuestion10 = ""
    var isQuestion11 = ""
    var isQuestion12 = ""
    var isQuestion13 = ""
    var isQuestion14 = ""
    var isQuestion15 = ""
    var isQuestion16 = ""
    var isQuestion17 = ""
    var isQuestion18 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getUserDataForStepfiveAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnQuestion1(_ sender: DLRadioButton)
    {
        isQuestion1 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion2(_ sender: DLRadioButton)
    {
        isQuestion2 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion3(_ sender: DLRadioButton)
    {
        isQuestion3 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion4(_ sender: DLRadioButton)
    {
        isQuestion4 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion5(_ sender: DLRadioButton)
    {
        isQuestion5 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion6(_ sender: DLRadioButton)
    {
        isQuestion6 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion7(_ sender: DLRadioButton)
    {
        isQuestion7 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion8(_ sender: DLRadioButton)
    {
        isQuestion8 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion9(_ sender: DLRadioButton)
    {
        isQuestion9 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion10(_ sender: DLRadioButton)
    {
        isQuestion10 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion11(_ sender: DLRadioButton)
    {
        isQuestion11 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion12(_ sender: DLRadioButton)
    {
        isQuestion12 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion13(_ sender: DLRadioButton)
    {
        isQuestion13 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion14(_ sender: DLRadioButton)
    {
        isQuestion14 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion15(_ sender: DLRadioButton)
    {
        isQuestion15 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion16(_ sender: DLRadioButton)
    {
        isQuestion16 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion17(_ sender: DLRadioButton)
    {
        isQuestion17 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion18(_ sender: DLRadioButton)
    {
        isQuestion18 = sender.titleLabel!.text!
    }
    
    @IBAction func btnCheckbox1(_ sender: Any)
    {
        if strCheckbox1 == "0"
        {
            strCheckbox1 = "1"
            imgCheckbox1.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox1 = "0"
            imgCheckbox1.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox2(_ sender: Any)
    {
        if strCheckbox2 == "0"
        {
            strCheckbox2 = "1"
            imgCheckbox2.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox2 = "0"
            imgCheckbox2.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox3(_ sender: Any)
    {
        if strCheckbox3 == "0"
        {
            strCheckbox3 = "1"
            imgCheckbox3.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox3 = "0"
            imgCheckbox3.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox4(_ sender: Any)
    {
        if strCheckbox4 == "0"
        {
            strCheckbox4 = "1"
            imgCheckbox4.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox4 = "0"
            imgCheckbox4.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox5(_ sender: Any)
    {
        if strCheckbox5 == "0"
        {
            strCheckbox5 = "1"
            imgCheckbox5.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox5 = "0"
            imgCheckbox5.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox6(_ sender: Any)
    {
        if strCheckbox6 == "0"
        {
            strCheckbox6 = "1"
            imgCheckbox6.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox6 = "0"
            imgCheckbox6.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox7(_ sender: Any)
    {
        if strCheckbox7 == "0"
        {
            strCheckbox7 = "1"
            imgCheckbox7.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox7 = "0"
            imgCheckbox7.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox8(_ sender: Any)
    {
        if strCheckbox8 == "0"
        {
            strCheckbox8 = "1"
            imgCheckbox8.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox8 = "0"
            imgCheckbox8.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox9(_ sender: Any)
    {
        if strCheckbox9 == "0"
        {
            strCheckbox9 = "1"
            imgCheckbox9.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox9 = "0"
            imgCheckbox9.image = UIImage.init(named: "untickbox.png")
        }
    }
    
    @IBAction func btnNext(_ sender: Any)
    {
        profileupdatestep5API()
    }
    func profileupdatestep5API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "closerelationshipwithyourmother":isQuestion1,
            "spousewouldsay16":isQuestion2,
            "relationshipwithyoursiblings":isQuestion3,
            "spousewouldsay17":isQuestion4,
            "3yearsofmarriage":isQuestion5,
            "spousewouldsay18":isQuestion6,
            "childrentobeeducated":isQuestion7,
            "spousewouldsay19":isQuestion8,
            "roleofawifeis":isQuestion9,
            "spousewouldsay20":isQuestion10,
            "TVintheirhome":isQuestion11,
            "spousewouldsay21":isQuestion12,
            "roleofahusbandis":isQuestion13,
            "spousewouldsay22":isQuestion14,
            "samehouseasyourparents":isQuestion15,
            "spousewouldsay23":isQuestion16,
            "samehouseasyourinlaws":isQuestion17,
            "spousewouldsay24":isQuestion18,
            "importnatqestion16":strCheckbox1,
            "importnatqestion17":strCheckbox2,
            "importnatqestion18":strCheckbox3,
            "importnatqestion19":strCheckbox4,
            "importnatqestion20":strCheckbox5,
            "importnatqestion21":strCheckbox6,
            "importnatqestion22":strCheckbox7,
            "importnatqestion23":strCheckbox8,
            "importnatqestion24":strCheckbox9,
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.profileupdatestep5.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesProfileVC") as! PreferencesProfileVC
                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getUserDataForStepfiveAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserDataForStepfive.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        
                        /////////////////
                        self.isQuestion1 = dicarr["closerelationshipwithyourmother"] as? String ?? ""
                        if dicarr["closerelationshipwithyourmother"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["closerelationshipwithyourmother"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion2 = dicarr["spousewouldsay16"] as? String ?? ""
                        if dicarr["spousewouldsay16"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay16"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion3 = dicarr["relationshipwithyoursiblings"] as? String ?? ""
                        if dicarr["relationshipwithyoursiblings"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(301) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["relationshipwithyoursiblings"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(302) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion4 = dicarr["spousewouldsay17"] as? String ?? ""
                        if dicarr["spousewouldsay17"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(401) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay17"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(402) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion5 = dicarr["threeyearsofmarriage"] as? String ?? ""
                        if dicarr["threeyearsofmarriage"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(501) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["threeyearsofmarriage"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(502) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion6 = dicarr["spousewouldsay18"] as? String ?? ""
                        if dicarr["spousewouldsay18"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(601) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay18"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(602) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion7 = dicarr["childrentobeeducated"] as? String ?? ""
                        if dicarr["childrentobeeducated"] as? String == "Islamic"
                        {
                            if let button = self.view.viewWithTag(701) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["childrentobeeducated"] as? String == "Home School"
                        {
                            if let button = self.view.viewWithTag(702) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["childrentobeeducated"] as? String == "Public School"
                        {
                            if let button = self.view.viewWithTag(703) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion8 = dicarr["spousewouldsay19"] as? String ?? ""
                        if dicarr["spousewouldsay19"] as? String == "Islamic"
                        {
                            if let button = self.view.viewWithTag(801) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay19"] as? String == "Home School"
                        {
                            if let button = self.view.viewWithTag(802) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay19"] as? String == "Public School"
                        {
                            if let button = self.view.viewWithTag(803) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion9 = dicarr["roleofawifeis"] as? String ?? ""
                        if dicarr["roleofawifeis"] as? String == "Work and help provide for the family"
                        {
                            if let button = self.view.viewWithTag(901) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["roleofawifeis"] as? String == "Stay home to raise the kids"
                        {
                            if let button = self.view.viewWithTag(902) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion10 = dicarr["spousewouldsay20"] as? String ?? ""
                        if dicarr["spousewouldsay20"] as? String == "Work and help provide for the family"
                        {
                            if let button = self.view.viewWithTag(1001) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay20"] as? String == "Stay home to raise the kids"
                        {
                            if let button = self.view.viewWithTag(1002) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion11 = dicarr["TVintheirhome"] as? String ?? ""
                        if dicarr["TVintheirhome"] as? String == "No, TV is time-wasting and a distraction"
                        {
                            if let button = self.view.viewWithTag(1101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["TVintheirhome"] as? String == "Yes, a TV can be used for beneficial things"
                        {
                            if let button = self.view.viewWithTag(1102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion12 = dicarr["spousewouldsay21"] as? String ?? ""
                        if dicarr["spousewouldsay21"] as? String == "No, TV is time-wasting and a distraction"
                        {
                            if let button = self.view.viewWithTag(1201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay21"] as? String == "Yes, a TV can be used for beneficial things"
                        {
                            if let button = self.view.viewWithTag(1202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion13 = dicarr["roleofahusbandis"] as? String ?? ""
                        if dicarr["roleofahusbandis"] as? String == "Exclusively support the family and make decisions"
                        {
                            if let button = self.view.viewWithTag(1301) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["roleofahusbandis"] as? String == "Be involved with household chores and share duties"
                        {
                            if let button = self.view.viewWithTag(1302) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion14 = dicarr["spousewouldsay22"] as? String ?? ""
                        if dicarr["spousewouldsay22"] as? String == "Exclusively support the family and make decisions"
                        {
                            if let button = self.view.viewWithTag(1401) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay22"] as? String == "Be involved with household chores and share duties"
                        {
                            if let button = self.view.viewWithTag(1402) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion15 = dicarr["samehouseasyourparents"] as? String ?? ""
                        if dicarr["samehouseasyourparents"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1501) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["samehouseasyourparents"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1502) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["samehouseasyourparents"] as? String == "Open for Discussion"
                        {
                            if let button = self.view.viewWithTag(1503) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion16 = dicarr["spousewouldsay23"] as? String ?? ""
                        if dicarr["spousewouldsay23"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1601) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay23"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1602) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay23"] as? String == "Open for Discussion"
                        {
                            if let button = self.view.viewWithTag(1603) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion17 = dicarr["samehouseasyourinlaws"] as? String ?? ""
                        if dicarr["samehouseasyourinlaws"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1701) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["samehouseasyourinlaws"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1702) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["samehouseasyourinlaws"] as? String == "Open for Discussion"
                        {
                            if let button = self.view.viewWithTag(1703) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion18 = dicarr["spousewouldsay24"] as? String ?? ""
                        if dicarr["spousewouldsay24"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1801) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay24"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1802) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["spousewouldsay24"] as? String == "Open for Discussion"
                        {
                            if let button = self.view.viewWithTag(1803) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        
                        /////////////////
                        self.strCheckbox1 = dicarr["importnatqestion16"] as? String ?? ""
                        if dicarr["importnatqestion16"] as? String == "0"
                        {
                            self.imgCheckbox1.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion16"] as? String == "1"
                        {
                            self.imgCheckbox1.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox2 = dicarr["importnatqestion17"] as? String ?? ""
                        if dicarr["importnatqestion17"] as? String == "0"
                        {
                            self.imgCheckbox2.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion17"] as? String == "1"
                        {
                            self.imgCheckbox2.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox3 = dicarr["importnatqestion18"] as? String ?? ""
                        if dicarr["importnatqestion18"] as? String == "0"
                        {
                            self.imgCheckbox3.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion18"] as? String == "1"
                        {
                            self.imgCheckbox3.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox4 = dicarr["importnatqestion19"] as? String ?? ""
                        if dicarr["importnatqestion19"] as? String == "0"
                        {
                            self.imgCheckbox4.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion19"] as? String == "1"
                        {
                            self.imgCheckbox4.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox5 = dicarr["importnatqestion20"] as? String ?? ""
                        if dicarr["importnatqestion20"] as? String == "0"
                        {
                            self.imgCheckbox5.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion20"] as? String == "1"
                        {
                            self.imgCheckbox5.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox6 = dicarr["importnatqestion21"] as? String ?? ""
                        if dicarr["importnatqestion21"] as? String == "0"
                        {
                            self.imgCheckbox6.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion21"] as? String == "1"
                        {
                            self.imgCheckbox6.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox7 = dicarr["importnatqestion22"] as? String ?? ""
                        if dicarr["importnatqestion22"] as? String == "0"
                        {
                            self.imgCheckbox7.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion22"] as? String == "1"
                        {
                            self.imgCheckbox7.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox8 = dicarr["importnatqestion23"] as? String ?? ""
                        if dicarr["importnatqestion23"] as? String == "0"
                        {
                            self.imgCheckbox8.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion23"] as? String == "1"
                        {
                            self.imgCheckbox8.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox9 = dicarr["importnatqestion24"] as? String ?? ""
                        if dicarr["importnatqestion24"] as? String == "0"
                        {
                            self.imgCheckbox9.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["importnatqestion24"] as? String == "1"
                        {
                            self.imgCheckbox9.image = UIImage.init(named: "tickbox.png")
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
