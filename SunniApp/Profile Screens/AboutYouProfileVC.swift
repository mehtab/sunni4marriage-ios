//
//  AboutYouProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import DLRadioButton
class AboutYouProfileVC: UIViewController {
    @IBOutlet weak var viewChildrenTopConst: NSLayoutConstraint!
    
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var viewDatepicker: UIView!
    @IBOutlet weak var viewChildrenBottomConst: NSLayoutConstraint!
    @IBOutlet weak var viewChildrenHieghtConst: NSLayoutConstraint!
    @IBOutlet weak var viewChildren: UIView!
    @IBOutlet weak var tblCheckbox: UITableView!
    @IBOutlet weak var viewTablePopup: UIView!
    @IBOutlet weak var pickerviewRadio: UIPickerView!
    @IBOutlet weak var viewPickerPopup: UIView!
    @IBOutlet weak var txtPotentialSpouse: UITextView!
    @IBOutlet weak var txtSearchingSpouse: UITextView!
    @IBOutlet weak var txtDescribeYourself: UITextView!
    @IBOutlet weak var txtIncome: UITextField!
    @IBOutlet weak var txtOccupation: UITextField!
    @IBOutlet weak var txtEducation: UITextField!
    @IBOutlet weak var txtLanguage: UITextField!
    @IBOutlet weak var txtBody: UITextField!
    @IBOutlet weak var txtHieght: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    
    var isMaritalStatus = ""
    var isChildren = ""
    
    var hieghtArr = [String]()
    var bodyTypeArr = [String]()
    var languageArr = [String]()
    var educationArr = [String]()
    var incomeArr = [String]()
    
    var pickerArray = [String]()
    var selectedIndex = 0
    var is_hieght_body_lang_edu_inc = ""
    var selectedLanguageArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hieghtArr = getHieghts()
        bodyTypeArr = getBodyType()
        languageArr = getLanguageList()
        educationArr = getEducationList()
        incomeArr = getIncomeList()
        tblCheckbox.reloadData()
        
        
        txtDescribeYourself.layer.cornerRadius = 5
        txtDescribeYourself.layer.masksToBounds = true
        txtDescribeYourself.layer.borderWidth = 1
        txtDescribeYourself.layer.borderColor = UIColor.lightGray.cgColor
        
        txtSearchingSpouse.layer.cornerRadius = 5
        txtSearchingSpouse.layer.masksToBounds = true
        txtSearchingSpouse.layer.borderWidth = 1
        txtSearchingSpouse.layer.borderColor = UIColor.lightGray.cgColor
        
        txtPotentialSpouse.layer.cornerRadius = 5
        txtPotentialSpouse.layer.masksToBounds = true
        txtPotentialSpouse.layer.borderWidth = 1
        txtPotentialSpouse.layer.borderColor = UIColor.lightGray.cgColor
        
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datepicker.maximumDate = date
        
        getUserDataForStep2API()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnDateOfbirth(_ sender: Any)
    {
        viewDatepicker.isHidden = false
    }
    @IBAction func btnCancelDate(_ sender: Any)
    {
        viewDatepicker.isHidden = true
    }
    @IBAction func btnDoneDate(_ sender: Any)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        txtDate.text = dateFormatter.string(from: datepicker.date)
        
        viewDatepicker.isHidden = true
    }
    @IBAction func btnSelectHieght(_ sender: Any)
    {
        pickerArray = hieghtArr
        selectedIndex = 0
        is_hieght_body_lang_edu_inc = "hieght"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
    
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnSelectBody(_ sender: Any)
    {
        pickerArray = bodyTypeArr
        selectedIndex = 0
        is_hieght_body_lang_edu_inc = "body"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnLanguage(_ sender: Any)
    {
        viewTablePopup.isHidden = false
    }
    @IBAction func btnNeverMarried(_ sender: DLRadioButton)
    {
        isMaritalStatus = sender.titleLabel!.text!
        if isMaritalStatus == "Divorced" || isMaritalStatus == "Widowed"
        {
            self.viewChildren.isHidden = false
            self.viewChildrenHieghtConst.constant = 60
            self.viewChildrenTopConst.constant = 15
            self.viewChildrenBottomConst.constant = 15
        }
        else
        {
            self.viewChildren.isHidden = true
            self.viewChildrenHieghtConst.constant = 0
            self.viewChildrenTopConst.constant = 0
            self.viewChildrenBottomConst.constant = 0
        }
    }
    
    @IBAction func btnCildren(_ sender: DLRadioButton)
    {
        isChildren = sender.titleLabel!.text!
    }
    @IBAction func btnEducation(_ sender: Any)
    {
        pickerArray = educationArr
        selectedIndex = 0
        is_hieght_body_lang_edu_inc = "edu"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnIncome(_ sender: Any)
    {
        pickerArray = incomeArr
        selectedIndex = 0
        is_hieght_body_lang_edu_inc = "inc"
        pickerviewRadio.reloadAllComponents()
        pickerviewRadio.selectRow(0, inComponent: 0, animated: false)
        
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnNext(_ sender: Any)
    {
        if txtHieght.text == ""
        {
            showTosterAlert(message: "Please select your hieght")
        }
        else if txtBody.text == ""
        {
            showTosterAlert(message: "Please Select Your Body Type")
        }
        else if txtLanguage.text == ""
        {
            showTosterAlert(message: "Please Select your languages")
        }
        else if isMaritalStatus == ""
        {
            showTosterAlert(message: "Please select Marital Status")
        }
        else if txtEducation.text == ""
        {
            showTosterAlert(message: "Please select education")
        }
        else if txtOccupation.text == ""
        {
            showTosterAlert(message: "Please select occupation")
        }
        else if txtIncome.text == ""
        {
            showTosterAlert(message: "Please select income")
        }
        else if txtDescribeYourself.text == ""
        {
            showTosterAlert(message: "Please describe yourself")
        }
        else if txtSearchingSpouse.text == ""
        {
            showTosterAlert(message: "What are you searching for in a spouse")
        }
//        else if txtPotentialSpouse.text == ""
//        {
//            showTosterAlert(message: "Please write three questions you would like to ask you potential spouse")
//        }
        else
        {
            if isMaritalStatus == "Never Married" || isMaritalStatus == "Anulled"
            {
                profileupdatestep2API()
            }
            else
            {
                if isChildren == ""
                {
                    showTosterAlert(message: "Please select children")
                }
                else
                {
                    profileupdatestep2API()
                }
            }
        }
    }
    
    @IBAction func btnDoneTablePopup(_ sender: Any)
    {
        var tempString = ""
        
        for(index,_) in selectedLanguageArr.enumerated()
        {
            tempString.append(selectedLanguageArr[index] as! String)
            tempString.append(",")
        }
        if tempString != ""
        {
            tempString.removeLast()
        }
        txtLanguage.text = tempString
        
        viewTablePopup.isHidden = true
    }
    @IBAction func btnCancelTablePopup(_ sender: Any)
    {
        viewTablePopup.isHidden = true
    }
    
    @IBAction func btnDonePickerPopup(_ sender: Any)
    {
        if is_hieght_body_lang_edu_inc == "hieght"
        {
            txtHieght.text = pickerArray[selectedIndex]
        }
        
        if is_hieght_body_lang_edu_inc == "body"
        {
            txtBody.text = pickerArray[selectedIndex]
        }
        
        if is_hieght_body_lang_edu_inc == "lang"
        {
            txtLanguage.text = pickerArray[selectedIndex]
        }
        
        if is_hieght_body_lang_edu_inc == "edu"
        {
            txtEducation.text = pickerArray[selectedIndex]
        }
        
        if is_hieght_body_lang_edu_inc == "inc"
        {
            txtIncome.text = pickerArray[selectedIndex]
        }
        
        viewPickerPopup.isHidden = true
    }
    @IBAction func btnCancelPickerPopup(_ sender: Any)
    {
        viewPickerPopup.isHidden = true
    }
    func profileupdatestep2API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "dob":txtDate.text!,
            "tall":txtHieght.text!,
            "bodytype":txtBody.text!,
            "languagesspeak":txtLanguage.text!,
            "maritalstatus":isMaritalStatus,
            "children":isChildren,
            "education":txtEducation.text!,
            "occupation":txtOccupation.text!,
            "annualincome":txtIncome.text!,
            "yourself":txtDescribeYourself.text!,
            "searchingspouse":txtSearchingSpouse.text!,
            "threequestionspotential":txtPotentialSpouse.text!,
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        
        let url = MAIN_API_URL+URLs.profileupdatestep2.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        self.navigationController?.popViewController(animated: true)
//                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "CharacterProfileVC") as! CharacterProfileVC
//                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getUserDataForStep2API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserDataForStep2.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        self.txtDate.text = dicarr["dob"] as? String
                        self.txtHieght.text = dicarr["tall"] as? String
                        self.txtBody.text = dicarr["bodytype"] as? String
                        self.txtLanguage.text = dicarr["languagesspeak"] as? String
                        self.txtEducation.text = dicarr["education"] as? String
                        self.txtOccupation.text = dicarr["occupation"] as? String
                        self.txtIncome.text = dicarr["annualincome"] as? String
                        self.txtDescribeYourself.text = dicarr["yourself"] as? String
                        self.txtSearchingSpouse.text = dicarr["searchingspouse"] as? String
                        //self.txtPotentialSpouse.text = dicarr["threequestionspotential"] as? String
                        
                        self.isMaritalStatus = dicarr["maritalstatus"] as? String ?? ""
                        if dicarr["maritalstatus"] as? String == "Never Married"
                        {
                            if let button = self.view.viewWithTag(101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                            
                            self.viewChildren.isHidden = true
                            self.viewChildrenHieghtConst.constant = 0
                            self.viewChildrenTopConst.constant = 0
                            self.viewChildrenBottomConst.constant = 0
                        }
                        else if dicarr["maritalstatus"] as? String == "Divorced"
                        {
                            if let button = self.view.viewWithTag(102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                            
                            self.viewChildren.isHidden = false
                            self.viewChildrenHieghtConst.constant = 60
                            self.viewChildrenTopConst.constant = 15
                            self.viewChildrenBottomConst.constant = 15
                        }
                        else if dicarr["maritalstatus"] as? String == "Widowed"
                        {
                            if let button = self.view.viewWithTag(103) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                            self.viewChildren.isHidden = false
                            self.viewChildrenHieghtConst.constant = 60
                            self.viewChildrenTopConst.constant = 15
                            self.viewChildrenBottomConst.constant = 15
                        }
                        else if dicarr["maritalstatus"] as? String == "Anulled"
                        {
                            if let button = self.view.viewWithTag(103) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                            self.viewChildren.isHidden = true
                            self.viewChildrenHieghtConst.constant = 0
                            self.viewChildrenTopConst.constant = 0
                            self.viewChildrenBottomConst.constant = 0
                        }
                        
                        self.isChildren = dicarr["children"] as? String ?? ""
                        if dicarr["children"] as? String == "0"
                        {
                            if let button = self.view.viewWithTag(200) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["children"] as? String == "1"
                        {
                            if let button = self.view.viewWithTag(201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["children"] as? String == "2"
                        {
                            if let button = self.view.viewWithTag(202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["children"] as? String == "3"
                        {
                            if let button = self.view.viewWithTag(203) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["children"] as? String == "More then 3"
                        {
                            if let button = self.view.viewWithTag(204) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension AboutYouProfileVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return languageArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkboxCell") as! checkboxCell
        
        cell.lblName.text = languageArr[indexPath.row]
        
        cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
        if selectedLanguageArr.contains(languageArr[indexPath.row])
        {
            cell.imgCheckbox.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            cell.imgCheckbox.image = UIImage.init(named: "untickbox.png")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if selectedLanguageArr.contains(languageArr[indexPath.row])
        {
            selectedLanguageArr.remove(languageArr[indexPath.row])
        }
        else
        {
            selectedLanguageArr.add(languageArr[indexPath.row])
        }
        tblCheckbox.reloadData()
    }
}
extension AboutYouProfileVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedIndex = row
    }
}
