//
//  PreferencesProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 31/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire
import SVProgressHUD

class PreferencesProfileVC: UIViewController {

    var isQuestion1 = ""
    var isQuestion2 = ""
    var isQuestion3 = ""
    var isQuestion4 = ""
    var isQuestion5 = ""
    var isQuestion6 = ""
    var isQuestion7 = ""
    var isQuestion8 = ""
    var isQuestion9 = ""
    var isQuestion10 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getUserDataForStepsixAPI()
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnQuestion1(_ sender: DLRadioButton)
    {
        isQuestion1 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion2(_ sender: DLRadioButton)
    {
        isQuestion2 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion3(_ sender: DLRadioButton)
    {
        isQuestion3 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion4(_ sender: DLRadioButton)
    {
        isQuestion4 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion5(_ sender: DLRadioButton)
    {
        isQuestion5 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion6(_ sender: DLRadioButton)
    {
        isQuestion6 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion7(_ sender: DLRadioButton)
    {
        isQuestion7 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion8(_ sender: DLRadioButton)
    {
        isQuestion8 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion9(_ sender: DLRadioButton)
    {
        isQuestion9 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion10(_ sender: DLRadioButton)
    {
        isQuestion10 = sender.titleLabel!.text!
    }
    @IBAction func btnFinish(_ sender: Any)
    {
        profileupdatestep6API()
    }
    
    func profileupdatestep6API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "cityformarriage":isQuestion1,
            "differentethnicity":isQuestion2,
            "differentcaste":isQuestion3,
            "whohasbeendivorced":isQuestion4,
            "whohaschildren":isQuestion5,
            "marrysomeoneyounger":isQuestion6,
            "marrysomeoneolder":isQuestion7,
            "samelevelofeducation":isQuestion8,
            "reverttoIslam":isQuestion9,
            "differentnationality":isQuestion10,
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.profileupdatestep6.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getUserDataForStepsixAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserDataForStepsix.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        
                        /////////////////
                        self.isQuestion1 = dicarr["cityformarriage"] as? String ?? ""
                        if dicarr["cityformarriage"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["cityformarriage"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["cityformarriage"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(103) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion2 = dicarr["differentethnicity"] as? String ?? ""
                        if dicarr["differentethnicity"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["differentethnicity"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["differentethnicity"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(203) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion3 = dicarr["differentcaste"] as? String ?? ""
                        if dicarr["differentcaste"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(301) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["differentcaste"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(302) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["differentcaste"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(303) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion4 = dicarr["whohasbeendivorced"] as? String ?? ""
                        if dicarr["whohasbeendivorced"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(401) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["whohasbeendivorced"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(402) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["whohasbeendivorced"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(403) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion5 = dicarr["whohaschildren"] as? String ?? ""
                        if dicarr["whohaschildren"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(501) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["whohaschildren"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(502) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["whohaschildren"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(503) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion6 = dicarr["marrysomeoneyounger"] as? String ?? ""
                        if dicarr["marrysomeoneyounger"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(601) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["marrysomeoneyounger"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(602) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["marrysomeoneyounger"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(603) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion7 = dicarr["marrysomeoneolder"] as? String ?? ""
                        if dicarr["marrysomeoneolder"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(701) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["marrysomeoneolder"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(702) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["marrysomeoneolder"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(703) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion8 = dicarr["samelevelofeducation"] as? String ?? ""
                        if dicarr["samelevelofeducation"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(801) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["samelevelofeducation"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(802) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["samelevelofeducation"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(803) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion9 = dicarr["reverttoIslam"] as? String ?? ""
                        if dicarr["reverttoIslam"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(901) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["reverttoIslam"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(902) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["reverttoIslam"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(903) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion10 = dicarr["differentnationality"] as? String ?? ""
                        if dicarr["differentnationality"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1001) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["differentnationality"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1002) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["differentnationality"] as? String == "Maybe"
                        {
                            if let button = self.view.viewWithTag(1003) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
