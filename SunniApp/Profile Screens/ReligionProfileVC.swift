//
//  ReligionProfileVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 31/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import DLRadioButton

class ReligionProfileVC: UIViewController {

    @IBOutlet weak var imgCheckbox6: UIImageView!
    @IBOutlet weak var imgCheckbox5: UIImageView!
    @IBOutlet weak var imgCheckbox4: UIImageView!
    @IBOutlet weak var imgCheckbox3: UIImageView!
    @IBOutlet weak var imgCheckbox2: UIImageView!
    @IBOutlet weak var imgCheckbox1: UIImageView!
    
    var strCheckbox1 = "0"
    var strCheckbox2 = "0"
    var strCheckbox3 = "0"
    var strCheckbox4 = "0"
    var strCheckbox5 = "0"
    var strCheckbox6 = "0"
    
    var isQuestion1 = ""
    var isQuestion2 = ""
    var isQuestion3 = ""
    var isQuestion4 = ""
    var isQuestion5 = ""
    var isQuestion6 = ""
    var isQuestion7 = ""
    var isQuestion8 = ""
    var isQuestion9 = ""
    var isQuestion10 = ""
    var isQuestion11 = ""
    var isQuestion12 = ""
    var isQuestion13 = ""
    var isQuestion14 = ""
    var isQuestion15 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getUserDataForStepfourAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnQuestion1(_ sender: DLRadioButton)
    {
        isQuestion1 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion2(_ sender: DLRadioButton)
    {
        isQuestion2 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion3(_ sender: DLRadioButton)
    {
        isQuestion3 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion4(_ sender: DLRadioButton)
    {
        isQuestion4 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion5(_ sender: DLRadioButton)
    {
        isQuestion5 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion6(_ sender: DLRadioButton)
    {
        isQuestion6 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion7(_ sender: DLRadioButton)
    {
        isQuestion7 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion8(_ sender: DLRadioButton)
    {
        isQuestion8 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion9(_ sender: DLRadioButton)
    {
        isQuestion9 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion10(_ sender: DLRadioButton)
    {
        isQuestion10 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion11(_ sender: DLRadioButton)
    {
        isQuestion11 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion12(_ sender: DLRadioButton)
    {
        isQuestion12 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion13(_ sender: DLRadioButton)
    {
        isQuestion13 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion14(_ sender: DLRadioButton)
    {
        isQuestion14 = sender.titleLabel!.text!
    }
    @IBAction func btnQuestion15(_ sender: DLRadioButton)
    {
        isQuestion15 = sender.titleLabel!.text!
    }
    
    @IBAction func btnCheckbox1(_ sender: Any)
    {
        if strCheckbox1 == "0"
        {
            strCheckbox1 = "1"
            imgCheckbox1.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox1 = "0"
            imgCheckbox1.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox2(_ sender: Any)
    {
        if strCheckbox2 == "0"
        {
            strCheckbox2 = "1"
            imgCheckbox2.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox2 = "0"
            imgCheckbox2.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox3(_ sender: Any)
    {
        if strCheckbox3 == "0"
        {
            strCheckbox3 = "1"
            imgCheckbox3.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox3 = "0"
            imgCheckbox3.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox4(_ sender: Any)
    {
        if strCheckbox4 == "0"
        {
            strCheckbox4 = "1"
            imgCheckbox4.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox4 = "0"
            imgCheckbox4.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox5(_ sender: Any)
    {
        if strCheckbox5 == "0"
        {
            strCheckbox5 = "1"
            imgCheckbox5.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox5 = "0"
            imgCheckbox5.image = UIImage.init(named: "untickbox.png")
        }
    }
    @IBAction func btnCheckbox6(_ sender: Any)
    {
        if strCheckbox6 == "0"
        {
            strCheckbox6 = "1"
            imgCheckbox6.image = UIImage.init(named: "tickbox.png")
        }
        else
        {
            strCheckbox6 = "0"
            imgCheckbox6.image = UIImage.init(named: "untickbox.png")
        }
    }
    
    @IBAction func btnNext(_ sender: Any)
    {
        profileupdatestep4API()
    }
    func profileupdatestep4API()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "menandwomen":isQuestion1,
            "youspousewouldsay8":isQuestion2,
            "islamis":isQuestion3,
            "youspousewouldsay9":isQuestion4,
            "islamicclasses":isQuestion5,
            "youspousewouldsay10":isQuestion6,
            "bestdescribesyou11":isQuestion7,
            "youspousewouldsay11":isQuestion8,
            "islamdoyoufollow12":isQuestion9,
            "youspousewouldsay12":isQuestion10,
            "performingprayer13":isQuestion11,
            "youspousewouldsay13":isQuestion12,
            "schoolofthought":isQuestion13,
            "importnatqestion8":strCheckbox1,
            "importnatqestion9":strCheckbox2,
            "importnatqestion10":strCheckbox3,
            "importnatqestion11":strCheckbox4,
            "importnatqestion12":strCheckbox5,
            "importnatqestion13":strCheckbox6,
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.profileupdatestep4.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "FamilyProfileVC") as! FamilyProfileVC
                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getUserDataForStepfourAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "userId":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserDataForStepfour.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        
                        /////////////////
                        self.isQuestion1 = dicarr["menandwomen"] as? String ?? ""
                        if dicarr["menandwomen"] as? String == "Can have friendship with the opposite gender"
                        {
                            if let button = self.view.viewWithTag(101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["menandwomen"] as? String == "Can have close friendship only with people of the same gender"
                        {
                            if let button = self.view.viewWithTag(102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.isQuestion2 = dicarr["youspousewouldsay8"] as? String ?? ""
                        if dicarr["youspousewouldsay8"] as? String == "Can have friendship with the opposite gender"
                        {
                            if let button = self.view.viewWithTag(201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay8"] as? String == "Can have close friendship only with people of the same gender"
                        {
                            if let button = self.view.viewWithTag(202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion3 = dicarr["islamis"] as? String ?? ""
                        if dicarr["islamis"] as? String == "Dear to my heart"
                        {
                            if let button = self.view.viewWithTag(301) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["islamis"] as? String == "My way of life"
                        {
                            if let button = self.view.viewWithTag(302) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion4 = dicarr["youspousewouldsay9"] as? String ?? ""
                        if dicarr["youspousewouldsay9"] as? String == "Dear to my heart"
                        {
                            if let button = self.view.viewWithTag(401) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay9"] as? String == "My way of life"
                        {
                            if let button = self.view.viewWithTag(402) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion5 = dicarr["islamicclasses"] as? String ?? ""
                        if dicarr["islamicclasses"] as? String == "Oftent"
                        {
                            if let button = self.view.viewWithTag(501) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["islamicclasses"] as? String == "Rarely"
                        {
                            if let button = self.view.viewWithTag(502) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["islamicclasses"] as? String == "Sometime"
                        {
                            if let button = self.view.viewWithTag(503) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion6 = dicarr["youspousewouldsay10"] as? String ?? ""
                        if dicarr["youspousewouldsay10"] as? String == "Oftent"
                        {
                            if let button = self.view.viewWithTag(601) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay10"] as? String == "Rarely"
                        {
                            if let button = self.view.viewWithTag(602) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay10"] as? String == "Sometime"
                        {
                            if let button = self.view.viewWithTag(603) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion7 = dicarr["bestdescribesyou11"] as? String ?? ""
                        if dicarr["bestdescribesyou11"] as? String == "Conformist"
                        {
                            if let button = self.view.viewWithTag(701) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["bestdescribesyou11"] as? String == "More Liberal"
                        {
                            if let button = self.view.viewWithTag(702) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion8 = dicarr["youspousewouldsay11"] as? String ?? ""
                        if dicarr["youspousewouldsay11"] as? String == "Conformist"
                        {
                            if let button = self.view.viewWithTag(801) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay11"] as? String == "More Liberal"
                        {
                            if let button = self.view.viewWithTag(802) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion9 = dicarr["islamdoyoufollow12"] as? String ?? ""
                        if dicarr["islamdoyoufollow12"] as? String == "Sunni"
                        {
                            if let button = self.view.viewWithTag(901) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["islamdoyoufollow12"] as? String == "Shia"
                        {
                            if let button = self.view.viewWithTag(902) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion10 = dicarr["youspousewouldsay12"] as? String ?? ""
                        if dicarr["youspousewouldsay12"] as? String == "Sunni"
                        {
                            if let button = self.view.viewWithTag(1001) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay12"] as? String == "Shia"
                        {
                            if let button = self.view.viewWithTag(1002) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion11 = dicarr["performingprayer13"] as? String ?? ""
                        if dicarr["performingprayer13"] as? String == "Sometimes hard for me to perform"
                        {
                            if let button = self.view.viewWithTag(1101) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["performingprayer13"] as? String == "Someting that is adhere to completely"
                        {
                            if let button = self.view.viewWithTag(1102) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion12 = dicarr["youspousewouldsay13"] as? String ?? ""
                        if dicarr["youspousewouldsay13"] as? String == "Sometimes hard for me to perform"
                        {
                            if let button = self.view.viewWithTag(1201) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay13"] as? String == "Someting that is adhere to completely"
                        {
                            if let button = self.view.viewWithTag(1202) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion13 = dicarr["schoolofthought"] as? String ?? ""
                        if dicarr["schoolofthought"] as? String == "Hanafi"
                        {
                            if let button = self.view.viewWithTag(1301) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["schoolofthought"] as? String == "Shafi"
                        {
                            if let button = self.view.viewWithTag(1302) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["schoolofthought"] as? String == "Maliki"
                        {
                            if let button = self.view.viewWithTag(1303) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["schoolofthought"] as? String == "Hanbali"
                        {
                            if let button = self.view.viewWithTag(1304) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["schoolofthought"] as? String == "Jafari"
                        {
                            if let button = self.view.viewWithTag(1305) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["schoolofthought"] as? String == "Salafi"
                        {
                            if let button = self.view.viewWithTag(1306) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["schoolofthought"] as? String == "Other"
                        {
                            if let button = self.view.viewWithTag(1307) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion14 = dicarr["scholar"] as? String ?? ""
                        if dicarr["scholar"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1401) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["scholar"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1402) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        /////////////////
                        self.isQuestion15 = dicarr["youspousewouldsay14"] as? String ?? ""
                        if dicarr["youspousewouldsay14"] as? String == "Yes"
                        {
                            if let button = self.view.viewWithTag(1501) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        else if dicarr["youspousewouldsay14"] as? String == "No"
                        {
                            if let button = self.view.viewWithTag(1502) as? DLRadioButton
                            {
                                button.isSelected = true
                            }
                        }
                        
                        
                        /////////////////
                        self.strCheckbox1 = dicarr["questionisimportant1"] as? String ?? ""
                        if dicarr["questionisimportant1"] as? String == "0"
                        {
                            self.imgCheckbox1.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant1"] as? String == "1"
                        {
                            self.imgCheckbox1.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox2 = dicarr["questionisimportant2"] as? String ?? ""
                        if dicarr["questionisimportant2"] as? String == "0"
                        {
                            self.imgCheckbox2.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant2"] as? String == "1"
                        {
                            self.imgCheckbox2.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox3 = dicarr["questionisimportant3"] as? String ?? ""
                        if dicarr["questionisimportant3"] as? String == "0"
                        {
                            self.imgCheckbox3.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant3"] as? String == "1"
                        {
                            self.imgCheckbox3.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox4 = dicarr["questionisimportant4"] as? String ?? ""
                        if dicarr["questionisimportant4"] as? String == "0"
                        {
                            self.imgCheckbox4.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant4"] as? String == "1"
                        {
                            self.imgCheckbox4.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox5 = dicarr["questionisimportant5"] as? String ?? ""
                        if dicarr["questionisimportant5"] as? String == "0"
                        {
                            self.imgCheckbox5.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant5"] as? String == "1"
                        {
                            self.imgCheckbox5.image = UIImage.init(named: "tickbox.png")
                        }
                        
                        /////////////////
                        self.strCheckbox6 = dicarr["questionisimportant6"] as? String ?? ""
                        if dicarr["questionisimportant6"] as? String == "0"
                        {
                            self.imgCheckbox6.image = UIImage.init(named: "untickbox.png")
                        }
                        else if dicarr["questionisimportant6"] as? String == "1"
                        {
                            self.imgCheckbox6.image = UIImage.init(named: "tickbox.png")
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
