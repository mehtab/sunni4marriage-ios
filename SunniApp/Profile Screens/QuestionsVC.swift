//
//  QuestionsVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 31/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class QuestionsVC: UIViewController {

    @IBOutlet weak var viewDeleteQus: LetsView!
    @IBOutlet weak var txtQus3: UITextField!
    @IBOutlet weak var txtQus2: UITextField!
    @IBOutlet weak var txtQus1: UITextField!
    
    var qid1 = ""
    var qid2 = ""
    var qid3 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getQueAnsByUserIdAPI()
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmit(_ sender: Any)
    {
        if txtQus1.text == ""
        {
            showTosterAlert(message: "Please write all the questions")
        }
        else if txtQus2.text == ""
        {
            showTosterAlert(message: "Please write all the questions")
        }
        else if txtQus3.text == ""
        {
            showTosterAlert(message: "Please write all the questions")
        }
        else
        {
            txtQus1.resignFirstResponder()
            txtQus2.resignFirstResponder()
            txtQus3.resignFirstResponder()

            
            addQuestionAPI()
        }
    }
    @IBAction func btnDeleteQus(_ sender: Any)
    {
        deleteQuestionAPI()
    }
    
    func getQueAnsByUserIdAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getQuestins.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        if arr.count != 0
                        {
                            if arr.count == 1
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.txtQus1.text = questions
                                }
                                if let q_id = dicarr["q_id"] as? String
                                {
                                    self.qid1 = q_id
                                }
                            }
                            else if arr.count == 2
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.txtQus1.text = questions
                                }
                                if let q_id = dicarr["q_id"] as? String
                                {
                                    self.qid1 = q_id
                                }
                                
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.txtQus2.text = questions1
                                }
                                if let q_id = dicarr1["q_id"] as? String
                                {
                                    self.qid2 = q_id
                                }
                            }
                            else if arr.count == 3
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.txtQus1.text = questions
                                }
                                if let q_id = dicarr["q_id"] as? String
                                {
                                    self.qid1 = q_id
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.txtQus2.text = questions1
                                }
                                if let q_id = dicarr1["q_id"] as? String
                                {
                                    self.qid2 = q_id
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.txtQus3.text = questions2
                                }
                                if let q_id = dicarr2["q_id"] as? String
                                {
                                    self.qid3 = q_id
                                }
                            }
                            else
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.txtQus1.text = questions
                                }
                                if let q_id = dicarr["q_id"] as? String
                                {
                                    self.qid1 = q_id
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.txtQus2.text = questions1
                                }
                                if let q_id = dicarr1["q_id"] as? String
                                {
                                    self.qid2 = q_id
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.txtQus3.text = questions2
                                }
                                if let q_id = dicarr2["q_id"] as? String
                                {
                                    self.qid3 = q_id
                                }
                            }
                            
                            if self.txtQus1.text == "" && self.txtQus2.text == "" && self.txtQus3.text == ""
                            {
                                self.viewDeleteQus.isHidden = true
                            }
                            else
                            {
                                self.viewDeleteQus.isHidden = false
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.viewDeleteQus.isHidden = true
                        //self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func addQuestionAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        var qids = ""
        if qid1 != ""
        {
            qids.append(qid1)
            qids.append(",")
        }
        if qid2 != ""
        {
            qids.append(qid2)
            qids.append(",")
        }
        if qid3 != ""
        {
            qids.append(qid3)
            qids.append(",")
        }
        if qids.count != 0
        {
            qids.removeLast()
        }
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "questions":txtQus1.text!,
            "questions1":txtQus2.text!,
            "questions2":txtQus3.text!,
            "q_id":qids
        ]
        
        let url = MAIN_API_URL+URLs.addQuestion.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.getQueAnsByUserIdAPI()
//                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
//                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func deleteQuestionAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.deleteQuestion.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        self.txtQus1.text = ""
                        self.txtQus2.text = ""
                        self.txtQus3.text = ""
                        
                        self.getQueAnsByUserIdAPI()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
