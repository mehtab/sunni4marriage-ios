//
//  SentMessagePopupVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 01/08/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class SentMessagePopupVC: UIViewController {

    @IBOutlet weak var lblQ3: UILabel!
    @IBOutlet weak var lblQ2: UILabel!
    @IBOutlet weak var lblQ1: UILabel!
    @IBOutlet weak var txtMessageHieghtConst: NSLayoutConstraint!
    @IBOutlet weak var lblMessageTextCount: UILabel!
    @IBOutlet weak var viewSendMessagePopup: UIView!
    @IBOutlet weak var txtMessage: UITextView!
    
    var connect_request_user_id = ""
    
    var delegate:UserListDelegate? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        
        self.txtMessage.delegate = self
        lblMessageTextCount.text = "\(txtMessage.text.count)/50"
        
        self.txtMessage.isScrollEnabled = false
        self.txtMessage.layer.cornerRadius = 5
        self.txtMessage.layer.masksToBounds = true
        self.txtMessage.layer.borderColor = UIColor.lightGray.cgColor
        self.txtMessage.layer.borderWidth = 1
        //setTextviewHeight(txtMessage)
        getQueAnsByUserIdAPI()
    }
    
    @IBAction func btnCloseMessagePopup(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSubmitMessage(_ sender: Any)
    {
        if txtMessage.text == ""
        {
            showTosterAlert(message: "Enter Your Message")
        }
        else
        {
            txtMessage.resignFirstResponder()
            updateFriendDataAPI()
        }
    }
}
extension SentMessagePopupVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        lblMessageTextCount.text = "\(txtMessage.text.count)/50"
        
        setTextviewHeight(textView)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 51    // 50 Limit Value
    }
    func setTextviewHeight(_ textView: UITextView)
    {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        txtMessageHieghtConst.constant = newFrame.size.height
    }
}
extension SentMessagePopupVC
{
    func updateFriendDataAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "connect_request_user_id":connect_request_user_id,
            "status":"0",
            "flag":"search_friend",
            "message":txtMessage.text!
        ]
        
        
        let url = MAIN_API_URL+URLs.UpdateFriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2, execute: {
                            self.delegate?.refreshAPIResponce()
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getQueAnsByUserIdAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":connect_request_user_id
        ]
        
        let url = MAIN_API_URL+URLs.getQuestins.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        if arr.count != 0
                        {
                            self.lblQ1.text = "Question 1"
                            self.lblQ2.text = "Question 2"
                            self.lblQ3.text = "Question 3"
                            
                            if arr.count == 1
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQ1.text = "Question 1 : " + questions
                                }
                            }
                            else if arr.count == 2
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQ1.text = "Question 1 : " + questions
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQ2.text = "Question 2 : " + questions1
                                }
                            }
                            else if arr.count == 3
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQ1.text = "Question 1 : " + questions
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQ2.text = "Question 2 : " + questions1
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.lblQ3.text = "Question 3 : " + questions2
                                }
                            }
                            else
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQ1.text = "Question 1 : " + questions
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQ2.text = "Question 2 : " + questions1
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.lblQ3.text = "Question 3 : " + questions2
                                }
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        //self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
