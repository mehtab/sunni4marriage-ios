//
//  ProfilePictureVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 02/08/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Alamofire

class ProfilePictureVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    
    var imageNumber = ""
    var selectedImage = UIImage()
    
    private let picker = UIImagePickerController()
    private let cropper = UIImageCropper(cropRatio: 2/3)

    var strimg1 = ""
    var strimg2 = ""
    var strimg3 = ""
    var strimg4 = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //setup the cropper
        cropper.delegate = self
        //cropper.cropRatio = 2/3 //(can be set during runtime or in init)
        cropper.cropButtonText = "Crop" // this can be localized if needed (as well as the cancelButtonText)
        
        image1.contentMode = .scaleAspectFill
        image2.contentMode = .scaleAspectFill
        image3.contentMode = .scaleAspectFill
        image4.contentMode = .scaleAspectFill
        
        getProfileAPI()
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnShowimage1(_ sender: Any)
    {
        if strimg1 != ""
        {
            let tempImageArr = [strimg1]
            
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            nextview.imagesArr = tempImageArr
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnShowimage2(_ sender: Any)
    {
        if strimg2 != ""
        {
            let tempImageArr = [strimg2]
            
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            nextview.imagesArr = tempImageArr
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnShowimage3(_ sender: Any)
    {
        if strimg3 != ""
        {
            let tempImageArr = [strimg3]
            
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            nextview.imagesArr = tempImageArr
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnShowimage4(_ sender: Any)
    {
        if strimg4 != ""
        {
            let tempImageArr = [strimg4]
            
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            nextview.imagesArr = tempImageArr
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnImage1(_ sender: Any)
    {
        imageNumber = "1"
        
        cropper.picker = picker
        cropper.cancelButtonText = "Retake"
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
        
//        let ImagePicker = UIImagePickerController()
//        ImagePicker.delegate = self
//        ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        ImagePicker.allowsEditing = false
//        self.present(ImagePicker, animated: true, completion: nil)
    }
    @IBAction func btnImage2(_ sender: Any)
    {
        imageNumber = "2"
        
        cropper.picker = picker
        cropper.cancelButtonText = "Retake"
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
        
//        let ImagePicker = UIImagePickerController()
//        ImagePicker.delegate = self
//        ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        ImagePicker.allowsEditing = false
//        self.present(ImagePicker, animated: true, completion: nil)
    }
    @IBAction func btnImage3(_ sender: Any)
    {
        imageNumber = "3"
        
        cropper.picker = picker
        cropper.cancelButtonText = "Retake"
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
        
//        let ImagePicker = UIImagePickerController()
//        ImagePicker.delegate = self
//        ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        ImagePicker.allowsEditing = false
//        self.present(ImagePicker, animated: true, completion: nil)
    }
    @IBAction func btnImage4(_ sender: Any)
    {
        imageNumber = "4"
        
        cropper.picker = picker
        cropper.cancelButtonText = "Retake"
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
        
//        let ImagePicker = UIImagePickerController()
//        ImagePicker.delegate = self
//        ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        ImagePicker.allowsEditing = false
//        self.present(ImagePicker, animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
//    {
//        var newImage: UIImage
//
//        if let possibleImage = info[.editedImage] as? UIImage
//        {
//            newImage = possibleImage
//        }
//        else if let possibleImage = info[.originalImage] as? UIImage
//        {
//            newImage = possibleImage
//        }
//        else
//        {
//            return
//        }
//
//        if imageNumber == "1"
//        {
//            image1.image = newImage
//        }
//        if imageNumber == "2"
//        {
//            image2.image = newImage
//        }
//        if imageNumber == "3"
//        {
//            image3.image = newImage
//        }
//        if imageNumber == "4"
//        {
//            image4.image = newImage
//        }
//        selectedImage = newImage
//
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
//            self.UploadProfileImageAPI()
//        }
//
//
//        self.dismiss(animated: true, completion: nil)
//    }
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
    @IBAction func btnDeleteImage1(_ sender: Any) {
        removeImageAPI(image: "image11")
    }
    @IBAction func btnDeleteImage2(_ sender: Any) {
        removeImageAPI(image: "image12")
    }
    @IBAction func btnDeleteImage3(_ sender: Any) {
        removeImageAPI(image: "image13")
    }
    @IBAction func btnDeleteImage4(_ sender: Any) {
        removeImageAPI(image: "image14")
    }
}
extension ProfilePictureVC: UIImageCropperProtocol {
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?)
    {
        if imageNumber == "1"
        {
            image1.image = croppedImage
        }
        if imageNumber == "2"
        {
            image2.image = croppedImage
        }
        if imageNumber == "3"
        {
            image3.image = croppedImage
        }
        if imageNumber == "4"
        {
            image4.image = croppedImage
        }
        selectedImage = croppedImage!
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
            self.UploadProfileImageAPI()
        }
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
extension ProfilePictureVC
{
    func getProfileAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":UserDefaults.standard.value(forKey: "email") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getUserData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        let avtarImage = UIImage.init(named: "placeholderProPic.jpg")
//                        if let gender = dicarr["gender"] as? String
//                        {
//                            if gender == "FEMALE"
//                            {
//                                avtarImage = UIImage.init(named: placeHolderImages_female)
//                            }
//                            else
//                            {
//                                avtarImage = UIImage.init(named: placeHolderImages_male)
//                            }
//                        }
                        
                        if let image1 = dicarr["image11"] as? String
                        {
                            self.strimg1 = image1
                            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(image1)"
                            self.image1.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
                            UserDefaults.standard.set(image1, forKey: "profilepic")
                        }
                        if let image2 = dicarr["image12"] as? String
                        {
                            self.strimg2 = image2
                            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(image2)"
                            self.image2.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
                        }
                        if let image3 = dicarr["image13"] as? String
                        {
                            self.strimg3 = image3
                            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(image3)"
                            self.image3.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
                        }
                        if let image4 = dicarr["image14"] as? String
                        {
                            self.strimg4 = image4
                            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(image4)"
                            self.image4.sd_setImage(with: URL(string: imagefullurl), placeholderImage: avtarImage)
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func UploadProfileImageAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let url = MAIN_API_URL+URLs.UploadProfileImage.rawValue
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "image_type":"image\(imageNumber)"
        ]
        
        
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            //upload parameter dic
            for (key, value) in parameter
            {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            var datestring = dateFormatterGet.string(from: Date())
            datestring = datestring.replacingOccurrences(of: " ", with: "")
            datestring = datestring.replacingOccurrences(of: "-", with: "")
            datestring = datestring.replacingOccurrences(of: ":", with: "")
            
            //upload images
            if self.imageNumber == "1"
            {
                let imgData = self.selectedImage.jpegData(compressionQuality: 0.5)!
                multipartFormData.append(imgData, withName: "file",fileName: "files\(datestring).jpg", mimeType: "image/jpg")
            }
            else if self.imageNumber == "2"
            {
                let imgData = self.selectedImage.jpegData(compressionQuality: 0.5)!
                multipartFormData.append(imgData, withName: "file",fileName: "files\(datestring).jpg", mimeType: "image/jpg")
            }
            else if self.imageNumber == "3"
            {
                let imgData = self.selectedImage.jpegData(compressionQuality: 0.5)!
                multipartFormData.append(imgData, withName: "file",fileName: "files\(datestring).jpg", mimeType: "image/jpg")
            }
            else if self.imageNumber == "4"
            {
                let imgData = self.selectedImage.jpegData(compressionQuality: 0.5)!
                multipartFormData.append(imgData, withName: "file",fileName: "files\(datestring).jpg", mimeType: "image/jpg")
            }
            
            
        }, to:url, headers:nil)
        { (result) in
            
            switch result
            {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "UPLOADING")
                })
                upload.responseString(completionHandler: { (response) in
                    print("responseString : \(response)")
                })
                upload.responseJSON { response in
                    print(response)
                    SVProgressHUD.dismiss()
                    if response.result.value != nil
                    {
                        let mainResponce = response.result.value as! NSDictionary
                        
                        if mainResponce["statusCode"] as! Int == 200
                        {
                            self.showTosterAlert(message: mainResponce["message"] as! String)
                            self.getProfileAPI()
                        }
                        else
                        {
                            print("data not found")
                            self.showTosterAlert(message: mainResponce["message"] as! String)
                        }
                    }
                    else
                    {
                        print("responce nil")
                        self.lostInternetConnectionAlert()
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    func removeImageAPI(image:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "\(image)":"\(image)"
        ]
        
        let url = MAIN_API_URL+URLs.RemoveImage.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let avtarImage = UIImage.init(named: "placeholderProPic.jpg")
                        
                        if image == "image11"
                        {
                            self.image1.image = avtarImage
                            UserDefaults.standard.set("", forKey: "profilepic")
                        }
                        if image == "image12"
                        {
                            self.image2.image = avtarImage
                        }
                        if image == "image13"
                        {
                            self.image3.image = avtarImage
                        }
                        if image == "image14"
                        {
                            self.image4.image = avtarImage
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}

