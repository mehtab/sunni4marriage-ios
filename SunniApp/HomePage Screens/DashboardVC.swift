//
//  DashboardVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SVProgressHUD
import CoreLocation

class DashboardVC: UIViewController {

    @IBOutlet weak var viewPremium: LetsView!
    @IBOutlet weak var viewUpgrade: LetsView!
    @IBOutlet weak var progressview: UIProgressView!
    @IBOutlet weak var lblAccountType: UILabel!
    @IBOutlet weak var imgLogo: LetsImageView!
    @IBOutlet weak var lblAccountNo: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    var isUpload = false
    let locationManager = CLLocationManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        getProfileAPI(username: UserDefaults.standard.value(forKey: "email") as! String)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        Annual membership fee of £69.00 is due
//
//        Only verified accounts can access the compatibility search
        
        if UserDefaults.standard.value(forKey: "first_name") != nil
        {
            lblUsername.text = UserDefaults.standard.value(forKey: "first_name") as? String
        }
        if UserDefaults.standard.value(forKey: "unique_user_id") != nil
        {
            lblAccountNo.text = UserDefaults.standard.value(forKey: "unique_user_id") as? String
        }
        if UserDefaults.standard.value(forKey: "payment_status") != nil
        {
            if UserDefaults.standard.value(forKey: "payment_status") as! String == "1"
            {
                lblAccountType.text = "Account-Paid"
                viewUpgrade.isHidden = true
                viewPremium.isHidden = false
            }
            else
            {
                lblAccountType.text = "Account-Free"
                viewUpgrade.isHidden = false
                viewPremium.isHidden = true
                
                let alertController = UIAlertController(title: "Annual membership fee of £\(IN_APP_PURCHASE.mc_gross) is due", message: "Only verified accounts can access the compatibility search", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Close", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        if UserDefaults.standard.value(forKey: "profilepic") != nil
        {
            let propic = UserDefaults.standard.value(forKey: "profilepic") as! String
            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(propic)"
            imgLogo.sd_setImage(with: URL(string: imagefullurl), placeholderImage: UIImage(named: "splash_image.png"))
        }
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnUpgradeNow(_ sender: Any)
    {
        //self.getPlanAPI()
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "AllPlanVC") as! AllPlanVC
        nextview.isOpenAllPlan = true
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnSelectProfilePic(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePictureVC") as! ProfilePictureVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnYourProfile(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnVisitorStatistics(_ sender: Any)
    {
        if lblAccountType.text == "Account-Free"
        {
            showTosterAlert(message: "Please Pay First Payment")
        }
        else
        {
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "VisitorsStatisticVC") as! VisitorsStatisticVC
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnShortlist(_ sender: Any)
    {
        if lblAccountType.text == "Account-Free"
        {
            showTosterAlert(message: "Please Pay First Payment")
        }
        else
        {
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ShortListVC") as! ShortListVC
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnRequest(_ sender: Any)
    {
        if lblAccountType.text == "Account-Free"
        {
            showTosterAlert(message: "Please Pay First Payment")
        }else{
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "RequestVC") as! RequestVC
            self.navigationController?.pushViewController(nextview, animated: true)}
    }
    @IBAction func btnChat(_ sender: Any)
    {
        if lblAccountType.text == "Account-Free"
        {
            showTosterAlert(message: "Please Pay First Payment")
        }else{
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            self.navigationController?.pushViewController(nextview, animated: true)}
    }
    @IBAction func btnCompatibilitySearch(_ sender: Any)
    {
        if lblAccountType.text == "Account-Free"
        {
            showTosterAlert(message: "Please Pay First Payment")
        }
        else
        {
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "CompatibilitySearchVC") as! CompatibilitySearchVC
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnInvitedFriend(_ sender: Any)
    {
        let text = "download Sunnis4Marriage app using this link https://apps.apple.com/us/app/sunnis-4-marriage/id1510760954"
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func btnSuggestion(_ sender: Any)
    {
        if lblAccountType.text == "Account-Free"
        {
            showTosterAlert(message: "Please Pay First Payment")
        }
        else
        {
            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SuggestionMainVC") as! SuggestionMainVC
            self.navigationController?.pushViewController(nextview, animated: true)
        }
    }
    @IBAction func btnPlan(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PlanVC") as! PlanVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnPaymentHistory(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PaymentHistoryVC") as! PaymentHistoryVC
        var name1 = ""
//        var name2 = ""
        if let name = UserDefaults.standard.value(forKey: "name") as? String
        {
            name1 = name
        }
//        if let last_name = UserDefaults.standard.value(forKey: "last_name") as? String
//        {
//            name2 = last_name
//        }
        nextview.nickname = "\(name1)"
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnBlockList(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "BlockListVC") as! BlockListVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnSettings(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnLogout(_ sender: Any)
    {
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "name")
        UserDefaults.standard.removeObject(forKey: "unique_user_id")
        UserDefaults.standard.removeObject(forKey: "payment_status")
        UserDefaults.standard.removeObject(forKey: "profilepic")
        UserDefaults.standard.removeObject(forKey: "email")
        
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    
}

extension DashboardVC : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        latitude = "\(locValue.latitude)"
        longitude = "\(locValue.longitude)"
        
        locationManager.stopUpdatingLocation()
    }
}

extension DashboardVC {
    func getProfileAPI(username:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":username
        ]
        
        let url = MAIN_API_URL+URLs.getUserData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        if let userId = dicarr["userId"] as? String
                        {
                            UserDefaults.standard.set(userId, forKey: "user_id")
                        }
                        if let name = dicarr["name"] as? String
                        {
                            UserDefaults.standard.set(name, forKey: "name")
                        }
                        if let first_name = dicarr["first_name"] as? String
                        {
                            UserDefaults.standard.set(first_name, forKey: "first_name")
                        }
                        if let last_name = dicarr["last_name"] as? String
                        {
                            UserDefaults.standard.set(last_name, forKey: "last_name")
                        }
                        if let unique_user_id = dicarr["unique_user_id"] as? String
                        {
                            UserDefaults.standard.set(unique_user_id, forKey: "unique_user_id")
                        }
                        if let customerID = dicarr["customer_id"] as? String
                        {
                            UserDefaults.standard.set(customerID, forKey: "customer_id")
                        }
                        
                        let payment_status = self.getNumberString(object: dicarr["payment_status"] as? NSObject)
                        UserDefaults.standard.set(payment_status, forKey: "payment_status")
                        
                        if let profilepic = dicarr["profilepic"] as? String
                        {
                            UserDefaults.standard.set(profilepic, forKey: "profilepic")
                        }
                        
                        self.viewWillAppear(true)
                    }
                    else if mainResponce["statusCode"] as! Int == 401
                    {
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                    else if mainResponce["statusCode"] as! Int == 404
                    {
                        UserDefaults.standard.removeObject(forKey: "email")
                        
                        let view = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    func getPlanAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getPlan.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let dataArr = mainResponce["data"] as! NSArray
                        if dataArr.count != 0 {
                            let mainDic = dataArr[0] as! NSDictionary
                            
                            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            nextview.amount = "\(mainDic["plan_sell_amount"] as! String)"
                            self.navigationController?.pushViewController(nextview, animated: true)
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
