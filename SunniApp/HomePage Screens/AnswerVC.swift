//
//  AnswerVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 08/09/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class AnswerVC: UIViewController {

    @IBOutlet weak var txtAns3: UITextField!
    @IBOutlet weak var txtAns2: UITextField!
    @IBOutlet weak var txtAns1: UITextField!
    @IBOutlet weak var lblQus3: UILabel!
    @IBOutlet weak var lblQus2: UILabel!
    @IBOutlet weak var lblQus1: UILabel!
    
    var user_id = ""
    
    var id1 = ""
    var id2 = ""
    var id3 = ""
    
    var delegate:UserListDelegate? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()

        getQueAnsByUserIdAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSubmit(_ sender: Any)
    {
        if id1 != "" && txtAns1.text == ""
        {
            showTosterAlert(message: "Please enter answer1")
        }
        else if id2 != "" && txtAns2.text == ""
        {
            showTosterAlert(message: "Please enter answer2")
        }
        else if id3 != "" && txtAns3.text == ""
        {
            showTosterAlert(message: "Please enter answer3")
        }
        else
        {
            submitAnswerAPI()
        }
    }
    
    func getQueAnsByUserIdAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":user_id
        ]
        
        let url = MAIN_API_URL+URLs.getQuestins.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        if arr.count != 0
                        {
                            if arr.count == 1
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQus1.text = questions
                                    self.id1 = dicarr["q_id"] as! String
                                }
                            }
                            else if arr.count == 2
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQus1.text = questions
                                    self.id1 = dicarr["q_id"] as! String
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQus2.text = questions1
                                    self.id2 = dicarr1["q_id"] as! String
                                }
                            }
                            else if arr.count == 3
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQus1.text = questions
                                    self.id1 = dicarr["q_id"] as! String
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQus2.text = questions1
                                    self.id2 = dicarr1["q_id"] as! String
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.lblQus3.text = questions2
                                    self.id3 = dicarr2["q_id"] as! String
                                }
                            }
                            else
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQus1.text = questions
                                    self.id1 = dicarr["q_id"] as! String
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQus2.text = questions1
                                    self.id2 = dicarr1["q_id"] as! String
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.lblQus3.text = questions2
                                    self.id3 = dicarr2["q_id"] as! String
                                }
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        //self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func submitAnswerAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let idArr = [id1,id2,id3]
        let ansArr = [txtAns1.text!,txtAns2.text!,txtAns3.text!]
        
        let parameter = [
            "user_id":user_id,
            "answer":ansArr,
            "id":idArr
            ] as [String : Any]
        
        let url = MAIN_API_URL+URLs.submitAnswer.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.updateFriendDataAPI()
                    }
                    else
                    {
                        print("data not found")
                        //self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func updateFriendDataAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "connect_request_user_id":user_id,
            "status":"0",
            "flag":"search_friend",
            "message":""
        ]
        
        
        let url = MAIN_API_URL+URLs.UpdateFriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2, execute: {
                            self.delegate?.refreshAPIResponce()
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
