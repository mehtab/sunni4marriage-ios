//
//  RequestVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Parchment

class RequestVC: UIViewController {

    @IBOutlet weak var viewcontainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "PendingRequestVC")
        firstViewController.title = "RECEIVED REQUEST"
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "AcceptedRequestVC")
        secondViewController.title = "ACCEPTED REQUEST"
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "SentRequestVC")
        thirdViewController.title = "SENT REQUEST"
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController,
            thirdViewController
            ])
        
        pagingViewController.menuBackgroundColor = UIColor(rgb: 0x008515)
        pagingViewController.textColor = UIColor.white
        pagingViewController.borderColor = UIColor.white
        pagingViewController.indicatorColor = UIColor.white
        pagingViewController.selectedTextColor = UIColor.white
        pagingViewController.selectedBackgroundColor = UIColor(rgb: 0x008515)
        pagingViewController.menuItemSize = .sizeToFit(minWidth: 170, height: 50)
        
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(pagingViewController)
        self.addSubview(subView: pagingViewController.view, toView: viewcontainer)
        pagingViewController.didMove(toParent: self)
        
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
