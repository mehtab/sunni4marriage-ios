//
//  NotificationVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 10/10/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SDWebImage

class notificationCell: UITableViewCell
{
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: LetsImageView!
}
class NotificationVC: UIViewController {

    @IBOutlet weak var tblNotification: UITableView!
    
    var notificationArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getNotificationDataAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func getNotificationDataAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getNotificationData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.notificationArr = data.mutableCopy() as! NSMutableArray
                        self.tblNotification.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension NotificationVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell") as! notificationCell
        
        let mainDic = notificationArr[indexPath.row] as! NSDictionary
        
        if let name = mainDic["nf_message"] as? String
        {
            let trimmedString = name.trimmingCharacters(in: .whitespaces)
            cell.lblTitle.text = trimmedString
        }
        else
        {
            cell.lblTitle.text = ""
        }

        if let nf_created = mainDic["nf_created"] as? String
        {
            cell.lblDate.text = nf_created
        }
        else
        {
            cell.lblDate.text = ""
        }
        
        if let profilepic = mainDic["profile_image"] as? String
        {
            let imagefullurl = "\(MAIN_API_IMAGE_URL)\(profilepic)"
            cell.imgProfile.sd_setImage(with: URL(string: imagefullurl), placeholderImage: UIImage.init(named: placeHolderImages))
            cell.imgProfile.isHidden = false
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
