//
//  UserListVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SDWebImage


class UserListVC: UIViewController {

    @IBOutlet weak var tblUserList: UITableView!
    
    var searchDataArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isFrom_UserData == "BlockListVC"
        {
            getBlockListAPI()
        }
        
    }
    @IBAction func btnLikeUnlike(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let shortlist_status = mainDic["shortlist_status"] as? Int
            {
                if shortlist_status == 0
                {
                    //addShortListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    //RemoveShortlistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnInvitationSent(_ sender: UIButton)
    {
//        let mainDic = searchDataArray[sender.tag] as! NSDictionary
//        if let userId = mainDic["userId"] as? String
//        {
//            if let request_status = mainDic["request_status"] as? Int
//            {
//                if request_status == 0
//                {
//
//                }
//                else
//                {
//
//                }
//            }
//        }
    }
    @IBAction func btnBlockUnblobk(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let block_status = mainDic["block_status"] as? Int
            {
                if block_status == 0
                {
                    //addBlockListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    //RemoveBlocklistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
}
extension UserListVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return searchDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        cell.setUserListData(mainDic: mainDic)
        
        if let shortlist_status = mainDic["shortlist_status"] as? Int
        {
            if shortlist_status == 0
            {
                cell.imgLikeUnlike.image = UIImage.init(named: "unlike.png")
            }
            else
            {
                cell.imgLikeUnlike.image = UIImage.init(named: "like.png")
            }
        }
        else
        {
            cell.imgLikeUnlike.image = UIImage.init(named: "unlike.png")
        }
        
        if let block_status = mainDic["block_status"] as? Int
        {
            if block_status == 0
            {
                cell.imgBlckUnblock.image = UIImage.init(named: "unblock.png")
            }
            else
            {
                cell.imgBlckUnblock.image = UIImage.init(named: "block.png")
            }
        }
        else
        {
            cell.imgBlckUnblock.image = UIImage.init(named: "unblock.png")
        }
        
        if let request_status = mainDic["request_status"] as? String
        {
            if request_status == "0"
            {
                cell.lblInvitationSent.text = "SEND REQUEST"
            }
            else
            {
                cell.lblInvitationSent.text = "CANCEL REQUEST"
            }
        }
        else
        {
            cell.lblInvitationSent.text = "SEND REQUEST"
        }
        
        
        cell.btnLikeUnlike.tag = indexPath.row
        //userId = 82;
        //"shortlist_status" = 0;
        //"request_status" = "";
        //profilepic = "<null>";
        //"block_status" = 0;
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
extension UserListVC
{
    
    func getShortListAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "sl_user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.addShortList.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.searchDataArray = data.mutableCopy() as! NSMutableArray
                        self.tblUserList.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    
    func getBlockListAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "flag":"blocktlist_data"
        ]
        
        let url = MAIN_API_URL+URLs.FriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.searchDataArray = data.mutableCopy() as! NSMutableArray
                        self.tblUserList.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    
    
}
