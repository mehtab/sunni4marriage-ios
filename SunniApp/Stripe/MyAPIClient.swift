//
//  MyAPIClient.swift
//  Doro
//
//  Created by Mapple Technologies on 09/11/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import Foundation
import Stripe
import PassKit
import Alamofire
import SVProgressHUD
import Toaster

class MyAPIClient: NSObject, STPCustomerEphemeralKeyProvider {
    
    
    enum APIError: Error {
        case unknown
        
        var localizedDescription: String {
            switch self {
            case .unknown:
                return "Unknown error"
            }
        }
    }
    
    var transactionId = ""
    
    var amount = ""
    var viewController: UIViewController!
    
    static let sharedClient = MyAPIClient()
    var baseURLString: String? = APIRoutes.baseUrl
    
    var baseURL: URL {
        return URL(string:  APIRoutes.baseUrl)!
    }
    
    func completeCharge(_ result: STPPaymentResult, amount: String, viewController: UIViewController,_ shippingAddress: STPAddress?, shippingMethod: PKShippingMethod?, completion: @escaping STPPaymentStatusBlock) {
        
        //   Utility.showLoading()
        self.viewController = viewController
        self.amount = amount.contains(".") ? amount.components(separatedBy: ".").first! : amount
        
        // Stripe charge in units i.e pens for GBP, cents for USD etc so to we have to convert in GBP or USD
        
        let finalAmount = Int(self.amount)! * 100
        
        APIClient.shared.updateProfileData(url: URL(string: APIRoutes.baseUrl+APIRoutes.paymentIntent)!, parameter: ["customer_id":UserDefaults.standard.string(forKey: "customer_id") ?? "","amount":"\(finalAmount)", "currency":"GBP", "payment_method_types":"card"]) { data in
            
            //  if error == nil {
            guard let paymentIntentClientSecret = data as? [String: AnyObject] else {
                return;
            }
            // Collect card details
            let paymentIntentParams = STPPaymentIntentParams(clientSecret: (((data["data"] as? [String: AnyObject] ?? [:])["client_secret"] as? String)!))
            
            self.transactionId = (((data["data"] as? [String: AnyObject] ?? [:])["id"] as? String)!)
            
            paymentIntentParams.paymentMethodId = result.paymentMethod?.stripeId
            
            // Submit the payment
            let paymentHandler = STPPaymentHandler.shared()
            paymentHandler.confirmPayment(paymentIntentParams, with: viewController as! STPAuthenticationContext) { (status, paymentIntent, error) in
                //  Utility.hideLoading()
                
                switch (status) {
                case .failed:
                    print("not working")
                    //   viewController.showAlert(title: "", message: error?.localizedDescription ?? "")
                    self.showAlertView(message: error?.localizedDescription ?? "")
                    break
                case .canceled:
                    // viewController.showAlert(title: "", message: error?.localizedDescription ?? "")
                    self.showAlertView(message: error?.localizedDescription ?? "")
                    break
                case .succeeded:
                    print("working")
                    self.setPaymentInfoAPI()
                    break
                @unknown default:
                    fatalError()
                    break
                }
            }
        }
    }
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = self.baseURL.appendingPathComponent(APIRoutes.emphemeralKeys)
        let parameters = ["customer_id":  UserDefaults.standard.string(forKey: "customer_id") ?? "", "stripe_version":"2020-08-27"]
        
        APIClient.shared.updateProfileData(url: url, parameter: parameters) { result in
            
            completion(result["data"] as! [AnyHashable : Any], nil)
            
        }
    }
    
    func setPaymentInfoAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "custom":UserDefaults.standard.value(forKey: "user_id") as! String,
            "transaction_id":self.transactionId,
            "currency":"GBP",
            "amount":amount,
            "merchant_account_id":"133",
            "submerchant_account_id":"133",
            "mastermerchant_account_id":"133",
            "processor_response_code":"1",
            "processor_response_type":"1",
            "payment_date":dateFormatter.string(from: Date()),
            "payment_type":"upgrade",
            "plan_type":"membership"
        ]
        
        let url = MAIN_API_URL+URLs.setPaymentInfo.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
            )
        { progress in
            print("Progress: \(progress.fractionCompleted)")
        }
        .validate { request, response, data in
            return .success
        }
        .responseJSON
        { response in
            print(response)
            SVProgressHUD.dismiss()
            
            if response.result.value != nil
            {
                let mainResponce = response.result.value as! NSDictionary
                
                if mainResponce["statusCode"] as! Int == 200
                {
                    self.showTosterAlert(message: mainResponce["message"] as! String)
                    
                    self.getProfileAPI(username: UserDefaults.standard.value(forKey: "email") as! String)
                }
                else
                {
                    print("data not found")
                    self.showAlertView(message: mainResponce["message"] as! String)
                }
            }
            else
            {
                print("responce nil")
                self.lostInternetConnectionAlert()
            }
        }
    }
    
    func showAlertView(message:String)
    {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    func lostInternetConnectionAlert()
    {
        let alertController = UIAlertController(title: "Alert", message: "Server is slow down Please try Again", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func showTosterAlert(message:String)
    {
        let toast = Toast(text: message, duration: Delay.long)
        toast.show()
    }
    
    
    func getProfileAPI(username:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":username
        ]
        
        let url = MAIN_API_URL+URLs.getUserData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
            )
        { progress in
            print("Progress: \(progress.fractionCompleted)")
        }
        .validate { request, response, data in
            return .success
        }
        .responseJSON
        { response in
            print(response)
            SVProgressHUD.dismiss()
            
            if response.result.value != nil
            {
                let mainResponce = response.result.value as! NSDictionary
                
                if mainResponce["statusCode"] as! Int == 200
                {
                    let arr = mainResponce["data"] as! NSArray
                    let dicarr = arr[0] as! NSDictionary
                    
                    if let userId = dicarr["userId"] as? String
                    {
                        UserDefaults.standard.set(userId, forKey: "user_id")
                    }
                    if let name = dicarr["name"] as? String
                    {
                        UserDefaults.standard.set(name, forKey: "name")
                    }
                    if let unique_user_id = dicarr["unique_user_id"] as? String
                    {
                        UserDefaults.standard.set(unique_user_id, forKey: "unique_user_id")
                    }
                    
                    let payment_status = self.getNumberString(object: dicarr["payment_status"] as? NSObject)
                    UserDefaults.standard.set(payment_status, forKey: "payment_status")
                    
                    if let profilepic = dicarr["profilepic"] as? String
                    {
                        UserDefaults.standard.set(profilepic, forKey: "profilepic")
                    }
                    
                    self.showTosterAlert(message: mainResponce["message"] as! String)
                    
                    let nextView = self.viewController.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                    self.viewController.navigationController?.pushViewController(nextView, animated: true)
                }
                else if mainResponce["statusCode"] as! Int == 401
                {
                    self.showAlertView(message: mainResponce["message"] as! String)
                }
                else if mainResponce["statusCode"] as! Int == 404
                {
                    UserDefaults.standard.removeObject(forKey: "email")
                    
                    let view = self.viewController.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.viewController.navigationController?.pushViewController(view, animated: true)
                }
                else
                {
                    print("data not found")
                    self.showAlertView(message: mainResponce["message"] as! String)
                }
            }
            else
            {
                print("responce nil")
                self.lostInternetConnectionAlert()
            }
        }
    }
    
    func getNumberString(object:NSObject?) -> String
    {
        var message = ""
        if let obj = object as? String {
            message = obj
        } else if let obj = object as? NSNumber {
            message = obj.stringValue
        }
        return message
    }
    func getBoolValue(object:NSObject?) -> Bool {
        var message = false
        if let obj = object as? String {
            let obj111 = obj.lowercased()
            if obj111 == "false" || obj111 == "0"{
                message = false
            } else {
                message = true
            }
        } else if let obj = object as? NSNumber {
            if obj.intValue == 0 {
                message = false
            } else {
                message = true
            }
        } else if let obj = object as? Bool {
            if obj == false {
                message = false
            } else {
                message = true
            }
        } else {
            message = false
        }
        return message
    }
}


