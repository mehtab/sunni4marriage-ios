//
//  LoginVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class LoginVC: UIViewController {

    @IBOutlet weak var viewOtpTextfield2: UIView!
    @IBOutlet weak var txtForgot2: LetsTextField!
    @IBOutlet weak var txtForgot1: LetsTextField!
    @IBOutlet weak var viewForgotPopuyp: UIView!
    @IBOutlet weak var txtPassword: LetsTextField!
    @IBOutlet weak var txtEmail: LetsTextField!
    
    var forgotStatus = "getOTP"//getOTP,CheckOTPMail,updatePwd
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func btnSignIn(_ sender: Any)
    {
        if txtEmail.text == ""
        {
            showTosterAlert(message: "Please enter username")
        }
        else if txtPassword.text == ""
        {
            showTosterAlert(message: "Please enter password")
        }
        else
        {
            loginAPI()
        }
    }
    @IBAction func btnForgot(_ sender: Any)
    {
        viewForgotPopuyp.isHidden = false
        
        txtForgot1.placeholder = "Email Id"
        viewOtpTextfield2.isHidden = true
        forgotStatus = "getOTP"
        
        txtForgot1.isSecureTextEntry = false
        txtForgot2.isSecureTextEntry = false
    }
    @IBAction func btnHideForgotPopup(_ sender: Any)
    {
        viewForgotPopuyp.isHidden = true
        
        txtForgot1.placeholder = "Email Id"
        viewOtpTextfield2.isHidden = true
        forgotStatus = "getOTP"
        
        txtForgot1.isSecureTextEntry = false
        txtForgot2.isSecureTextEntry = false
        
        txtForgot1.resignFirstResponder()
        txtForgot1.resignFirstResponder()
    }
    @IBAction func btnSubmitForgot(_ sender: Any)
    {
        if forgotStatus == "getOTP"
        {
            if txtForgot1.text == ""
            {
                showTosterAlert(message: "Please enter email")
            }
            else
            {
                getOTPAPI(mail: txtForgot1.text!)
            }
        }
        else if forgotStatus == "CheckOTPMail"
        {
            if txtForgot1.text == ""
            {
                showTosterAlert(message: "Please enter email")
            }
            if txtForgot2.text == ""
            {
                showTosterAlert(message: "Please enter otp")
            }
            else
            {
                CheckOTPMailAPI(mail: txtForgot1.text!, otp: txtForgot2.text!)
            }
            
        }
        else if forgotStatus == "updatePwd"
        {
            if txtForgot1.text == ""
            {
                showTosterAlert(message: "Please enter new password")
            }
            if txtForgot2.text == ""
            {
                showTosterAlert(message: "Please enter confirm new password")
            }
            else
            {
                updatePwdAPI(password: txtForgot1.text!)
            }
        }
    }
    @IBAction func btnSignUp(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    func loginAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":txtEmail.text!,
            "password":txtPassword.text!,
            "fcm_key":"this is test key"
        ]
        
        let url = MAIN_API_URL+URLs.login.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        UserDefaults.standard.set(self.txtEmail.text!, forKey: "email")
                        
                        self.getProfileAPI(username: self.txtEmail.text!)
                    }
                    else if mainResponce["statusCode"] as! Int == 402
                    {
                        //alredy exist
                        
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        UserDefaults.standard.set(self.txtEmail.text!, forKey: "email")
                        
                        self.getProfileAPI(username: self.txtEmail.text!)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getProfileAPI(username:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":username
        ]
        
        let url = MAIN_API_URL+URLs.getUserData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        print("Authorization : \(UserDefaults.standard.value(forKey: "email") as! String)")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        
                        if let userId = dicarr["customer_id"] as? String
                        {
                            UserDefaults.standard.set(userId, forKey: "customer_id")
                        }
                        
                        if let userId = dicarr["userId"] as? String
                        {
                            UserDefaults.standard.set(userId, forKey: "user_id")
                        }
                        if let name = dicarr["name"] as? String
                        {
                            UserDefaults.standard.set(name, forKey: "name")
                        }
                        if let first_name = dicarr["first_name"] as? String
                        {
                            UserDefaults.standard.set(first_name, forKey: "first_name")
                        }
                        if let last_name = dicarr["last_name"] as? String
                        {
                            UserDefaults.standard.set(last_name, forKey: "last_name")
                        }
                        if let unique_user_id = dicarr["unique_user_id"] as? String
                        {
                            UserDefaults.standard.set(unique_user_id, forKey: "unique_user_id")
                        }
                        
                        let payment_status = self.getNumberString(object: dicarr["payment_status"] as? NSObject)
                        UserDefaults.standard.set(payment_status, forKey: "payment_status")
                        
                        if let profilepic = dicarr["image11"] as? String
                        {
                            UserDefaults.standard.set(profilepic, forKey: "profilepic")
                        }
                        
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else if mainResponce["statusCode"] as! Int == 401
                    {
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                    else if mainResponce["statusCode"] as! Int == 404
                    {
                        UserDefaults.standard.removeObject(forKey: "email")
                        
                        let view = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getOTPAPI(mail:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "mail":mail
        ]
        
        let url = MAIN_API_URL+URLs.getOTP.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 201
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.txtForgot1.resignFirstResponder()
                        self.txtForgot1.isUserInteractionEnabled = false
                        
                        self.forgotStatus = "CheckOTPMail"
                        
                        self.txtForgot1.placeholder = "Email Id"
                        self.txtForgot2.placeholder = "OTP"
                        self.viewOtpTextfield2.isHidden = false
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func CheckOTPMailAPI(mail:String,otp:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":mail,
            "otp":otp
        ]
        
        let url = MAIN_API_URL+URLs.CheckOTPMail.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 201
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.forgotStatus = "updatePwd"
                        
                        self.email = self.txtForgot1.text!
                        
                        self.txtForgot1.resignFirstResponder()
                        self.txtForgot2.resignFirstResponder()
                        self.txtForgot1.isUserInteractionEnabled = true
                        self.txtForgot1.text = ""
                        self.txtForgot2.text = ""
                        
                        self.txtForgot1.placeholder = "New Password"
                        self.txtForgot2.placeholder = "Confirm New Password"
                        self.viewOtpTextfield2.isHidden = false
                        
                        self.txtForgot1.isSecureTextEntry = true
                        self.txtForgot2.isSecureTextEntry = true
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func updatePwdAPI(password:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":email,
            "password":password
        ]
        
        let url = MAIN_API_URL+URLs.updatePwd.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.forgotStatus = "getOTP"
                        self.viewForgotPopuyp.isHidden = true
                        
                        self.txtForgot1.resignFirstResponder()
                        self.txtForgot2.resignFirstResponder()
                        
                        self.txtForgot1.text = ""
                        self.txtForgot2.text = ""
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
