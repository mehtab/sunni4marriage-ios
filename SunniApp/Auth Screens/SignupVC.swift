//
//  SignupVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class SignupVC: UIViewController {

    @IBOutlet weak var imgAgree: UIImageView!
    @IBOutlet weak var txtFirstNameNew: UITextField!
    @IBOutlet weak var pickerviewRadio: UIPickerView!
    @IBOutlet weak var viewPickerPopup: UIView!
    
    @IBOutlet weak var datepickerviewRadio: UIDatePicker!
    @IBOutlet weak var viewDatePickerPopup: UIView!
    
    @IBOutlet weak var txtLivingIn: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtContactnoLegal: UITextField!
    @IBOutlet weak var txtEmailLegal: UITextField!
    @IBOutlet weak var txtReletionshipLegal: UITextField!
    @IBOutlet weak var txtNameLegal: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    
    var genderArr = ["Male","Female"]
    var selectedIndex = 0
    
    var isAgree = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datepickerviewRadio.maximumDate = date
        if #available(iOS 13.4, *) {
            datepickerviewRadio.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
    }
    @IBAction func btnLogin(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnSelectGender(_ sender: Any)
    {
        viewPickerPopup.isHidden = false
    }
    @IBAction func btnSelectDob(_ sender: Any)
    {
        viewDatePickerPopup.isHidden = false
    }
    @IBAction func btnSubmit(_ sender: Any)
    {
        if txtFirstName.text == ""
        {
            showTosterAlert(message: "Please enter nickname")
        }
        if txtFirstNameNew.text == ""
        {
            showTosterAlert(message: "Please enter first name")
        }
        else if txtLastName.text == ""
        {
            showTosterAlert(message: "Please enter last name")
        }
        else if txtEmail.text == ""
        {
            showTosterAlert(message: "Please enter email")
        }
        /*else if txtNameLegal.text == ""
        {
            showTosterAlert(message: "Please enter The Name of Your Legal Representative")
        }
        else if txtReletionshipLegal.text == ""
        {
            showTosterAlert(message: "Please enter Reletionship To  Legal Representative")
        }
        else if txtEmailLegal.text == ""
        {
            showTosterAlert(message: "Please enter Your Legal Representative's Email")
        }
        else if txtContactnoLegal.text == ""
        {
            showTosterAlert(message: "Please enter Your Legal Representative's Contact No")
        }*/
        else if txtPassword.text == ""
        {
            showTosterAlert(message: "Please enter password")
        }
        else if txtGender.text == ""
        {
            showTosterAlert(message: "Please enter gender")
        }
        else if txtDob.text == ""
        {
            showTosterAlert(message: "Please enter date of birth")
        }
        else if txtLivingIn.text == ""
        {
            showTosterAlert(message: "Please enter location")
        }
        else if isAgree == false
        {
            showTosterAlert(message: "Please accept terms and conditions")
        }
        else
        {
            registerAPI()
        }
    }
    
    @IBAction func btnDonePickerPopup(_ sender: Any)
    {
        txtGender.text = genderArr[selectedIndex]
        
        viewPickerPopup.isHidden = true
    }
    @IBAction func btnCancelPickerPopup(_ sender: Any)
    {
        viewPickerPopup.isHidden = true
    }
    
    @IBAction func btnDoneDatePickerPopup(_ sender: Any)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        txtDob.text = dateFormatter.string(from: datepickerviewRadio.date)
        
        viewDatePickerPopup.isHidden = true
    }
    @IBAction func btnCancelDatePickerPopup(_ sender: Any)
    {
        viewDatePickerPopup.isHidden = true
    }
    @IBAction func btnTermsCondition(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnAgree(_ sender: Any)
    {
        if isAgree == true
        {
            isAgree = false
            imgAgree.image = UIImage.init(named: "untickbox.png")
        }
        else
        {
            isAgree = true
            imgAgree.image = UIImage.init(named: "tickbox.png")
        }
    }
    func registerAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "first_name":txtFirstNameNew.text!,
            "email":txtEmail.text!,
            "password":txtPassword.text!,
            "fname":txtFirstName.text!,
            "last_name":txtLastName.text!,
            "dob":txtDob.text!,
            "mothertongue":"",
            "country":"",
            "profilefor":txtLivingIn.text!,
            "gender":txtGender.text!,
            "relationship":txtReletionshipLegal.text!,
            "email_rep":txtEmailLegal.text!,
            "contactnumber":txtContactnoLegal.text!,
            "leagalname":txtNameLegal.text!,
            "device_id":""
        ]
        
        
        
        let url = MAIN_API_URL+URLs.register.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 201
                    {
                        //self.showTosterAlert(message: mainResponce["message"] as! String)
                        let alertController = UIAlertController(title: "A Verification link has been sent to your email account", message: "Your account has been created and email will be sent to verify", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                        {
                            UIAlertAction in
                            
                            let nextview = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            self.navigationController?.pushViewController(nextview, animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension SignupVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArr[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedIndex = row
    }
}
