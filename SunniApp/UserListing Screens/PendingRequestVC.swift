//
//  PendingRequestVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class PendingRequestVC: UIViewController {

    @IBOutlet weak var lblAns3: UILabel!
    @IBOutlet weak var lblAns2: UILabel!
    @IBOutlet weak var lblAns1: UILabel!
    @IBOutlet weak var lblQue3: UILabel!
    @IBOutlet weak var lblQue2: UILabel!
    @IBOutlet weak var lblQue1: UILabel!
    @IBOutlet weak var viewQusPopup: UIView!
    @IBOutlet weak var tblPendingReq: UITableView!
    
    var searchDataArray = NSMutableArray()
    private var userAPIModel: UserAPIModel = UserAPIModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userAPIModel.delegate = self
        getPendingRequestAPI()
    }
    
    @IBAction func btnLikeUnlike(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let shortlist_status = mainDic["shortlist_status"] as? Int
            {
                if shortlist_status == 0
                {
                    if let block_status = mainDic["block_status"] as? Int
                    {
                        if block_status == 1
                        {
                            showTosterAlert(message: "Please remove from block first")
                            return
                        }
                    }
                    userAPIModel.addShortListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveShortlistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnBlockUnblock(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let block_status = mainDic["block_status"] as? Int
            {
                if block_status == 0
                {
                    if let shortlist_status = mainDic["shortlist_status"] as? Int
                    {
                        if shortlist_status == 1
                        {
                            showTosterAlert(message: "Please remove from favorite first")
                            return
                        }
                    }
                    userAPIModel.addBlockListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveBlocklistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnInvitation(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        let connect_request_id = mainDic["connect_request_id"] as! String
        
        updateFriendDataAPI(connect_request_id: connect_request_id, isReject: false)
    }
    @IBAction func btnAcceptReq(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        let connect_request_id = mainDic["connect_request_id"] as! String
        
        updateFriendDataAPI(connect_request_id: connect_request_id, isReject: false)
    }
    @IBAction func btnRejectReq(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        let connect_request_id = mainDic["connect_request_id"] as! String
        
        RejectAPI(connect_request_id: connect_request_id)
    }
    @IBAction func btnViewQus(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileDetailsVC") as! UserProfileDetailsVC
        nextview.mainDic = mainDic
        self.navigationController?.pushViewController(nextview, animated: true)
        
//        let mainDic = searchDataArray[sender.tag] as! NSDictionary
//        getQueAnsByUserIdAPI(user_id: mainDic["userId"] as! String)
    }
    @IBAction func btnHidePopup(_ sender: Any)
    {
        self.viewQusPopup.isHidden = true
    }
}
extension PendingRequestVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let count = searchDataArray.count
        if count == 0 {
            let label: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            if let font = UIFont.init(name: "Montserrat-Regular", size: 25) {
                label.font = font
            }
            label.text = "No records found!"
            label.textColor = UIColor.init(rgb: 0x6F7179)
            label.textAlignment = .center
            tableView.backgroundView  = label
            tableView.separatorStyle  = .none
        } else {
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        cell.setUserListData_pending(mainDic: mainDic)
        
        cell.btnViewQus.tag = indexPath.row
        cell.btnAcceptReq.tag = indexPath.row
        cell.btnRejectReq.tag = indexPath.row
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension PendingRequestVC : UserListDelegate
{
    func refreshAPIResponce() {
        
    }
    
    func showAlertView_delegate(lostconnection_alert: String, msg: String) {
        if lostconnection_alert == "lostconnection"
        {
            self.lostInternetConnectionAlert()
        }
        else
        {
            self.showAlertView(message: msg)
        }
    }
    
    func addShortListResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "shortlist_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblPendingReq.reloadRows(at: [indexpath], with: .none)
        }
    }
    func removeShortlistResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "shortlist_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblPendingReq.reloadRows(at: [indexpath], with: .none)
        }
    }
    func addBlockListResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "block_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblPendingReq.reloadRows(at: [indexpath], with: .none)
        }
    }
    func removeBlocklistAPIResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "block_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblPendingReq.reloadRows(at: [indexpath], with: .none)
        }
    }
}
extension PendingRequestVC
{
    func getPendingRequestAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "flag":"pending_request"
        ]
        
        let url = MAIN_API_URL+URLs.FriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.searchDataArray = data.mutableCopy() as! NSMutableArray
                        self.tblPendingReq.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func updateFriendDataAPI(connect_request_id:String,isReject:Bool)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        var parameter = [String:String]()
            parameter = [
                "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
                "connect_request_id":connect_request_id,
                "status":"1",
                "flag":"pending_request"
            ]
        
        let url = MAIN_API_URL+URLs.UpdateFriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.getPendingRequestAPI()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func getQueAnsByUserIdAPI(user_id:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getQueAnsByUserId.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        if arr.count != 0
                        {
                            self.viewQusPopup.isHidden = false
                            
                            if arr.count == 1
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQue1.text = questions
                                    self.lblAns1.text = dicarr["answers"] as? String
                                }
                            }
                            else if arr.count == 2
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQue1.text = questions
                                    self.lblAns1.text = dicarr["answers"] as? String
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQue2.text = questions1
                                    self.lblAns2.text = dicarr1["answers"] as? String
                                }
                            }
                            else if arr.count == 3
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQue1.text = questions
                                    self.lblAns1.text = dicarr["answers"] as? String
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQue2.text = questions1
                                    self.lblAns2.text = dicarr1["answers"] as? String
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.lblQue3.text = questions2
                                    self.lblAns3.text = dicarr2["answers"] as? String
                                }
                            }
                            else
                            {
                                let dicarr = arr[0] as! NSDictionary
                                if let questions = dicarr["questions"] as? String
                                {
                                    self.lblQue1.text = questions
                                    self.lblAns1.text = dicarr["answers"] as? String
                                }
                                
                                let dicarr1 = arr[1] as! NSDictionary
                                if let questions1 = dicarr1["questions"] as? String
                                {
                                    self.lblQue2.text = questions1
                                    self.lblAns2.text = dicarr1["answers"] as? String
                                }
                                
                                let dicarr2 = arr[2] as! NSDictionary
                                if let questions2 = dicarr2["questions"] as? String
                                {
                                    self.lblQue3.text = questions2
                                    self.lblAns3.text = dicarr2["answers"] as? String
                                }
                            }
                        }
                    }
                    else
                    {
                        print("data not found")
                        //self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func RejectAPI(connect_request_id:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "status":"2",
            "flag":"pending_request",
            "connect_request_id":connect_request_id
        ]
        
        let url = MAIN_API_URL+URLs.UpdateFriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.getPendingRequestAPI()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
