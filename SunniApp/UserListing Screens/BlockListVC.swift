//
//  BlockListVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class BlockListVC: UIViewController {

    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var tblBlocklist: UITableView!
    
    var searchDataArray = NSMutableArray()
    private var userAPIModel: UserAPIModel = UserAPIModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userAPIModel.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getBlocklistAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLikeUnlike(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let shortlist_status = mainDic["shortlist_status"] as? Int
            {
                if shortlist_status == 0
                {
                    if let block_status = mainDic["block_status"] as? Int
                    {
                        if block_status == 1
                        {
                            showTosterAlert(message: "Please remove from block first")
                            return
                        }
                    }
                    userAPIModel.addShortListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveShortlistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnBlockUnblock(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let block_status = mainDic["block_status"] as? Int
            {
                if block_status == 0
                {
                    if let shortlist_status = mainDic["shortlist_status"] as? Int
                    {
                        if shortlist_status == 1
                        {
                            showTosterAlert(message: "Please remove from favorite first")
                            return
                        }
                    }
                    userAPIModel.addBlockListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveBlocklistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnInvitation(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        let user_id = mainDic["userId"] as! String
        let connect_request_id = mainDic["connect_request_id"] as! String
        
        if let question_status = mainDic["question_status"] as? String
        {
            if let request_status = mainDic["request_status"] as? String
            {
                if request_status == "0"
                {
                    //cell.lblInvitationSent.text = "CANCEL REQUEST"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "1"
                {
                    //cell.lblInvitationSent.text = "REMOVE"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "2"
                {
                    //cell.lblInvitationSent.text = "PENDING REQUEST"
                }
                else if request_status == "3"
                {
//                    if question_status == "1"
//                    {
//                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
//                        nextview.delegate = self
//                        nextview.user_id = user_id
//                        nextview.modalPresentationStyle = .overCurrentContext
//                        self.present(nextview, animated: true, completion: nil)
//                    }
//                    else
//                    {
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                        nextview.delegate = self
                        nextview.connect_request_user_id = user_id
                        nextview.modalPresentationStyle = .overCurrentContext
                        self.present(nextview, animated: true, completion: nil)
//                    }
                }
                else
                {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                }
            }
        }
        else
        {
            if let request_status = mainDic["request_status"] as? String
            {
                if request_status == "0"
                {
                    //cell.lblInvitationSent.text = "CANCEL REQUEST"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "1"
                {
                    //cell.lblInvitationSent.text = "REMOVE"
                    userAPIModel.cancelRequestAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
                }
                else if request_status == "2"
                {
                    //cell.lblInvitationSent.text = "PENDING REQUEST"
                }
                else if request_status == "3"
                {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                }
                else
                {
                    let nextview = self.storyboard?.instantiateViewController(withIdentifier: "SentMessagePopupVC") as! SentMessagePopupVC
                    nextview.delegate = self
                    nextview.connect_request_user_id = user_id
                    nextview.modalPresentationStyle = .overCurrentContext
                    self.present(nextview, animated: true, completion: nil)
                }
            }
        }
    }
}
extension BlockListVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let count = searchDataArray.count
        if count == 0 {
            let label: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            if let font = UIFont.init(name: "Montserrat-Regular", size: 25) {
                label.font = font
            }
            label.text = "No records found!"
            label.textColor = UIColor.init(rgb: 0x6F7179)
            label.textAlignment = .center
            tableView.backgroundView  = label
            tableView.separatorStyle  = .none
        } else {
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        cell.setUserListData(mainDic: mainDic)
        
        
        let payment_status = self.getNumberString(object: mainDic["payment_status"] as? NSObject)
        if payment_status == "0" || payment_status == ""
        {
            cell.viewPrimium.isHidden = true
        }
        else
        {
            cell.viewPrimium.isHidden = false
        }
        
        if let shortlist_status = mainDic["shortlist_status"] as? Int
        {
            if shortlist_status == 0
            {
                cell.imgLikeUnlike.image = UIImage.init(named: "unlike.png")
            }
            else
            {
                cell.imgLikeUnlike.image = UIImage.init(named: "like.png")
            }
        }
        else
        {
            cell.imgLikeUnlike.image = UIImage.init(named: "unlike.png")
        }
        
        if let block_status = mainDic["block_status"] as? Int
        {
            if block_status == 0
            {
                cell.imgBlckUnblock.image = UIImage.init(named: "unblock.png")
            }
            else
            {
                cell.imgBlckUnblock.image = UIImage.init(named: "block.png")
            }
        }
        else
        {
            cell.imgBlckUnblock.image = UIImage.init(named: "unblock.png")
        }
        
        if let request_status = mainDic["request_status"] as? String
        {
            if request_status == "0"
            {
                cell.lblInvitationSent.text = "CANCEL REQUEST"
            }
            else if request_status == "1"
            {
                cell.lblInvitationSent.text = "REMOVE"
            }
            else if request_status == "2"
            {
                cell.lblInvitationSent.text = "PENDING REQUEST"
            }
            else if request_status == "3"
            {
                cell.lblInvitationSent.text = "SEND REQUEST"
            }
            else
            {
                cell.lblInvitationSent.text = "SEND REQUEST"
            }
        }
        else
        {
            cell.lblInvitationSent.text = "SEND REQUEST"
        }
        
        
        
        cell.btnLikeUnlike.tag = indexPath.row
        cell.btnBlockUNblock.tag = indexPath.row
        cell.btnInvitationSet.tag = indexPath.row//INVITATION SENT
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileDetailsVC") as! UserProfileDetailsVC
        nextview.mainDic = mainDic
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    
}
extension BlockListVC : UserListDelegate
{
    func refreshAPIResponce() {
        getBlocklistAPI()
    }
    func showAlertView_delegate(lostconnection_alert: String, msg: String) {
        if lostconnection_alert == "lostconnection"
        {
            self.lostInternetConnectionAlert()
        }
        else
        {
            self.showAlertView(message: msg)
        }
    }
    
    func addShortListResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "shortlist_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblBlocklist.reloadRows(at: [indexpath], with: .none)
        }
    }
    func removeShortlistResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "shortlist_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblBlocklist.reloadRows(at: [indexpath], with: .none)
        }
    }
    func addBlockListResponce(index: Int)
    {
        
    }
    func removeBlocklistAPIResponce(index: Int)
    {
        self.searchDataArray.removeObject(at: index)
        UIView.performWithoutAnimation {
            self.tblBlocklist.reloadData()
        }
        
        if self.searchDataArray.count == 0
        {
            self.lblNoData.isHidden = false
            self.tblBlocklist.isHidden = true
        }
        else
        {
            self.lblNoData.isHidden = true
            self.tblBlocklist.isHidden = false
        }
    }
}
extension BlockListVC
{
    func getBlocklistAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "flag":"blocktlist_data"
        ]
        
        let url = MAIN_API_URL+URLs.FriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.searchDataArray = data.mutableCopy() as! NSMutableArray
                        self.tblBlocklist.reloadData()
                        
                        if self.searchDataArray.count == 0
                        {
                            self.lblNoData.isHidden = false
                            self.tblBlocklist.isHidden = true
                        }
                        else
                        {
                            self.lblNoData.isHidden = true
                            self.tblBlocklist.isHidden = false
                        }
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
