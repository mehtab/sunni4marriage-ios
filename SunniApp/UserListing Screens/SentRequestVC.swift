//
//  SentRequestVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class SentRequestVC: UIViewController {

    @IBOutlet weak var tblSentReq: UITableView!
    
    var searchDataArray = NSMutableArray()
    private var userAPIModel: UserAPIModel = UserAPIModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userAPIModel.delegate = self
        getSentRequestAPI()
    }
    

    @IBAction func btnLikeUnlike(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let shortlist_status = mainDic["shortlist_status"] as? Int
            {
                if shortlist_status == 0
                {
                    if let block_status = mainDic["block_status"] as? Int
                    {
                        if block_status == 1
                        {
                            showTosterAlert(message: "Please remove from block first")
                            return
                        }
                    }
                    userAPIModel.addShortListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveShortlistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnBlockUnblock(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        if let userId = mainDic["userId"] as? String
        {
            if let block_status = mainDic["block_status"] as? Int
            {
                if block_status == 0
                {
                    if let shortlist_status = mainDic["shortlist_status"] as? Int
                    {
                        if shortlist_status == 1
                        {
                            showTosterAlert(message: "Please remove from favorite first")
                            return
                        }
                    }
                    userAPIModel.addBlockListAPI(sl_profile_id: userId, index: sender.tag)
                }
                else
                {
                    userAPIModel.RemoveBlocklistAPI(sl_profile_id: userId, index: sender.tag)
                }
            }
        }
    }
    @IBAction func btnInvitation(_ sender: UIButton)
    {
        let mainDic = searchDataArray[sender.tag] as! NSDictionary
        let user_id = mainDic["userId"] as! String
        let connect_request_id = mainDic["connect_request_id"] as! String
        
        updateFriendDataAPI(connect_request_user_id: user_id, connect_request_id: connect_request_id)
    }
}
extension SentRequestVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let count = searchDataArray.count
        if count == 0 {
            let label: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            if let font = UIFont.init(name: "Montserrat-Regular", size: 25) {
                label.font = font
            }
            label.text = "No records found!"
            label.textColor = UIColor.init(rgb: 0x6F7179)
            label.textAlignment = .center
            tableView.backgroundView  = label
            tableView.separatorStyle  = .none
        } else {
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        cell.setUserListData(mainDic: mainDic)
        
        
        let payment_status = self.getNumberString(object: mainDic["payment_status"] as? NSObject)
        if payment_status == "0" || payment_status == ""
        {
            cell.viewPrimium.isHidden = true
        }
        else
        {
            cell.viewPrimium.isHidden = false
        }
        
        if let shortlist_status = mainDic["shortlist_status"] as? Int
        {
            if shortlist_status == 0
            {
                cell.imgLikeUnlike.image = UIImage.init(named: "unlike.png")
            }
            else
            {
                cell.imgLikeUnlike.image = UIImage.init(named: "like.png")
            }
        }
        else
        {
            cell.imgLikeUnlike.image = UIImage.init(named: "unlike.png")
        }
        
        if let block_status = mainDic["block_status"] as? Int
        {
            if block_status == 0
            {
                cell.imgBlckUnblock.image = UIImage.init(named: "unblock.png")
            }
            else
            {
                cell.imgBlckUnblock.image = UIImage.init(named: "block.png")
            }
        }
        else
        {
            cell.imgBlckUnblock.image = UIImage.init(named: "unblock.png")
        }
        cell.lblInvitationSent.text = "CANCEL REQUEST"
        cell.btnLikeUnlike.tag = indexPath.row
        cell.btnBlockUNblock.tag = indexPath.row
        cell.btnInvitationSet.tag = indexPath.row//INVITATION SENT
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainDic = searchDataArray[indexPath.row] as! NSDictionary
        
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileDetailsVC") as! UserProfileDetailsVC
        nextview.mainDic = mainDic
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    
}
extension SentRequestVC : UserListDelegate
{
    func refreshAPIResponce() {
        
    }
    func showAlertView_delegate(lostconnection_alert: String, msg: String) {
        if lostconnection_alert == "lostconnection"
        {
            self.lostInternetConnectionAlert()
        }
        else
        {
            self.showAlertView(message: msg)
        }
    }
    
    func addShortListResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "shortlist_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblSentReq.reloadRows(at: [indexpath], with: .none)
        }
    }
    func removeShortlistResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "shortlist_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblSentReq.reloadRows(at: [indexpath], with: .none)
        }
    }
    func addBlockListResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(1, forKey: "block_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblSentReq.reloadRows(at: [indexpath], with: .none)
        }
    }
    func removeBlocklistAPIResponce(index: Int)
    {
        let mainDic = self.searchDataArray[index] as! NSDictionary
        let tempMainDic = mainDic.mutableCopy() as! NSMutableDictionary
        tempMainDic.setValue(0, forKey: "block_status")
        
        self.searchDataArray.replaceObject(at: index, with: tempMainDic)
        
        let indexpath = IndexPath.init(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.tblSentReq.reloadRows(at: [indexpath], with: .none)
        }
    }
}
extension SentRequestVC
{
    func getSentRequestAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "flag":"sent_request"
        ]
        
        let url = MAIN_API_URL+URLs.FriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.searchDataArray = data.mutableCopy() as! NSMutableArray
                        self.tblSentReq.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    func updateFriendDataAPI(connect_request_user_id:String, connect_request_id:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "connect_request_user_id":connect_request_user_id,
            "status":"1",
            "flag":"friend_list",
            "message":"",
            "connect_request_id":connect_request_id
        ]
        
        
        let url = MAIN_API_URL+URLs.UpdateFriendData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.getSentRequestAPI()
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
