//
//  PaymentHistoryVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 22/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class PaymentHistoryCell: UITableViewCell
{

    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblPaymentPlan: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblName: UILabel!
}
class PaymentHistoryVC: UIViewController {

    @IBOutlet weak var tblPaymentHistory: UITableView!
    
    var paymentArray = NSMutableArray()
    var nickname = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getPaymentHistoryAPI()
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPaymentHistoryAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        let url = MAIN_API_URL+URLs.getPaymentHistory.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let data = mainResponce["data"] as! NSArray
                        self.paymentArray = data.mutableCopy() as! NSMutableArray
                        self.tblPaymentHistory.reloadData()
                    }
                    else
                    {
                        print("data not found")
                        //self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension PaymentHistoryVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return paymentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        
        let mainDic = paymentArray[indexPath.row] as! NSDictionary
        
        cell.lblPlanName.text = "Membership Plan".uppercased()
        
        cell.lblName.text = nickname
        if let transaction_id = mainDic["transaction_id"] as? String {
            cell.lblId.text = transaction_id
        } else {
            cell.lblId.text = ""
        }
        if let payment_date = mainDic["payment_date"] as? String {
            cell.lblStartDate.text = changeDateFormate_date(date: payment_date)
        } else {
            cell.lblStartDate.text = ""
        }
        if let end_date = mainDic["end_date"] as? String {
            cell.lblEndDate.text = changeDateFormate_date(date: end_date)
        } else {
            cell.lblEndDate.text = ""
        }
        if let time = mainDic["payment_date"] as? String {
            cell.lblTime.text = changeDateFormate_time(date: time)
        } else {
            cell.lblTime.text = ""
        }
        if let amount = mainDic["amount"] as? String {
            cell.lblPaymentPlan.text = "£\(amount)"
        } else {
            cell.lblPaymentPlan.text = ""
        }
        
        return cell
    }
    
    
}
