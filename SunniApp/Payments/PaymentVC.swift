//
//  PaymentVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 29/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import Stripe
import WebKit

class PaymentVC: UIViewController
{

    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var lblAmount: UILabel!
    
    var amount = ""
    var customerContext: STPCustomerContext?
    var paymentContext: STPPaymentContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblAmount.text = "Annual membership fee of £\(amount) is due"
        
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.navigationDelegate = self
        webview.isHidden = true
        
        customerContext = STPCustomerContext(keyProvider: MyAPIClient())
        self.paymentContext = STPPaymentContext(customerContext: customerContext!)
        self.paymentContext?.delegate = self
        self.paymentContext?.hostViewController = self
        if let intAmount = Int(amount) {
            self.paymentContext?.paymentAmount = intAmount
        }
        
        
        
    }

    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func changeCardPressed(_ sender: Any) {
        stripeContext()

    }
    
    
    @IBAction func btnPaypal(_ sender: Any) {
        self.paymentContext?.requestPayment()
    }
    
    func stripeContext () {

        
        self.paymentContext?.presentPaymentOptionsViewController()

    }
    
    func setPaymentInfoAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "transaction_id":"1234567890",
            "currency":"GBP",
            "amount":amount,
            "merchant_account_id":"133",
            "submerchant_account_id":"133",
            "mastermerchant_account_id":"133",
            "processor_response_code":"1",
            "processor_response_type":"1",
            "payment_date":dateFormatter.string(from: Date()),
            "payment_type":"1",
            "plan_type":"membership"
        ]
        
        let url = MAIN_API_URL+URLs.setPaymentInfo.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        self.getProfileAPI(username: UserDefaults.standard.value(forKey: "email") as! String)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
    
    func getProfileAPI(username:String)
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "email":username
        ]
        
        let url = MAIN_API_URL+URLs.getUserData.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default,headers: nil)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        let arr = mainResponce["data"] as! NSArray
                        let dicarr = arr[0] as! NSDictionary
                        
                        if let userId = dicarr["userId"] as? String
                        {
                            UserDefaults.standard.set(userId, forKey: "user_id")
                        }
                        if let name = dicarr["name"] as? String
                        {
                            UserDefaults.standard.set(name, forKey: "name")
                        }
                        if let unique_user_id = dicarr["unique_user_id"] as? String
                        {
                            UserDefaults.standard.set(unique_user_id, forKey: "unique_user_id")
                        }
                        
                        let payment_status = self.getNumberString(object: dicarr["payment_status"] as? NSObject)
                        UserDefaults.standard.set(payment_status, forKey: "payment_status")
                        
                        if let profilepic = dicarr["profilepic"] as? String
                        {
                            UserDefaults.standard.set(profilepic, forKey: "profilepic")
                        }
                        
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        let nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                        self.navigationController?.pushViewController(nextView, animated: true)
                    }
                    else if mainResponce["statusCode"] as! Int == 401
                    {
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                    else if mainResponce["statusCode"] as! Int == 404
                    {
                        UserDefaults.standard.removeObject(forKey: "email")
                        
                        let view = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
extension PaymentVC : WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        print("Finished navigating to url \(webView.url!)")
        
        if webview.url!.absoluteString == PAYPAL_CANCEL_URL
        {
            webview.isHidden = true
            print("Cancel")
        }
        
        if webview.url!.absoluteString == PAYPAL_SUCCESS_URL
        {
            webview.isHidden = true
            print("Success")
            getProfileAPI(username: UserDefaults.standard.value(forKey: "email") as! String)
        }
    }
}


extension PaymentVC: STPPaymentContextDelegate, STPAuthenticationContext {
    
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        
    }

    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPPaymentStatusBlock) {
        MyAPIClient.sharedClient.completeCharge(paymentResult, amount: amount, viewController: self, nil, shippingMethod: nil, completion: completion)
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        print(status.description, error?.localizedDescription)
    }
}
