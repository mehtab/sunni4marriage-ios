//
//  ContactUsVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/10/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class ContactUsVC: UIViewController {

    @IBOutlet weak var webview: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL.init(string: "http://www.sunnis4marriage.com/contact/")!
        let request = URLRequest.init(url: url)
        webview.load(request)
        webview.navigationDelegate = self
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension ContactUsVC: WKNavigationDelegate, WKUIDelegate {
    func showActivityIndicator(show: Bool) {
        if show {
            SVProgressHUD.show()
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(show: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
}
