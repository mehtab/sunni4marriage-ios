//
//  ChangePasswordVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 25/07/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var txtConfirmNewPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmit(_ sender: Any)
    {
        if txtOldPassword.text == ""
        {
            showTosterAlert(message: "Please enter old password")
        }
        else if txtNewPassword.text == ""
        {
            showTosterAlert(message: "Please enter new password")
        }
        else if txtConfirmNewPassword.text == ""
        {
            showTosterAlert(message: "Please enter confirm new password")
        }
        else if txtNewPassword.text != txtConfirmNewPassword.text
        {
            showTosterAlert(message: "Password not match")
        }
        else
        {
            changePasswordAPI()
        }
    }
    
}
extension ChangePasswordVC
{
    func changePasswordAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String,
            "oldPassword":txtOldPassword.text!,
            "newPassword":txtNewPassword.text!
        ]
        

        let url = MAIN_API_URL+URLs.changePassword.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
