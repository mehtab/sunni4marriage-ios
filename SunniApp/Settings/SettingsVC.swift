//
//  SettingsVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/06/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class SettingsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnChangePassword(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    @IBAction func btnDeleteAccount(_ sender: Any)
    {
        let alertController = UIAlertController(title: "DELETE", message: "Do you want to delete your profile?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        let saveAction = UIAlertAction(title: "DELETE", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            self.user_delete_profileAPI()
        }
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnContactUs(_ sender: Any)
    {
        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.navigationController?.pushViewController(nextview, animated: true)
    }
    

    func user_delete_profileAPI()
    {
        SVProgressHUD.show(withStatus: "LOADING")
        
        let parameter = [
            "user_id":UserDefaults.standard.value(forKey: "user_id") as! String
        ]
        
        
        let url = MAIN_API_URL+URLs.user_delete_profile.rawValue
        print("\n\n\n\n\n")
        print(url)
        print(parameter)
        print("\n\n")
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)
                )
            { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON
            { response in
                print(response)
                SVProgressHUD.dismiss()
                
                if response.result.value != nil
                {
                    let mainResponce = response.result.value as! NSDictionary
                    
                    if mainResponce["statusCode"] as! Int == 200
                    {
                        self.showTosterAlert(message: mainResponce["message"] as! String)
                        
                        UserDefaults.standard.removeObject(forKey: "user_id")
                        UserDefaults.standard.removeObject(forKey: "name")
                        UserDefaults.standard.removeObject(forKey: "unique_user_id")
                        UserDefaults.standard.removeObject(forKey: "payment_status")
                        UserDefaults.standard.removeObject(forKey: "profilepic")
                        UserDefaults.standard.removeObject(forKey: "email")
                        
                        let nextview = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                        self.navigationController?.pushViewController(nextview, animated: true)
                    }
                    else
                    {
                        print("data not found")
                        self.showAlertView(message: mainResponce["message"] as! String)
                    }
                }
                else
                {
                    print("responce nil")
                    self.lostInternetConnectionAlert()
                }
        }
    }
}
