//
//  AppDelegate.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 28/05/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD
import Firebase
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        SVProgressHUD.setDefaultMaskType(.black)
        
        let pk_live = "pk_live_51KUFpRJgVt65aXUkSpIUfeRR7j7JWIbFEYr6BPIn8wnAVXjKVBwJ9Ag2706KBf05Bbf8Ah9AIkhp4d4UEPhwTCGG00dRCMpSsy"
        let pk_test = "pk_test_51KUFpRJgVt65aXUkQuIml2aF0rLa8lW2jZ0s3ZQwU0yDXZ4zyOSns8W2Bjj5kGHblK5Ind2B8Kg6xvVX9WRQPUQb00nEXvZySW"
        
        StripeAPI.defaultPublishableKey = pk_live
        FirebaseApp.configure()
                
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

