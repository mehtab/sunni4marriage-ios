//
//  GalleryVC.swift
//  SunniApp
//
//  Created by Nitesh Makwana on 23/10/19.
//  Copyright © 2019 Coder. All rights reserved.
//

import UIKit
class GalleryCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgGallery: UIImageView!
}
class GalleryVC: UIViewController {

    @IBOutlet weak var cvGallery: UICollectionView!
    
    var imagesArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension GalleryVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        
        let imagefullurl = "\(MAIN_API_IMAGE_URL)\(imagesArr[indexPath.row])"
        cell.imgGallery.sd_setImage(with: URL(string: imagefullurl), placeholderImage: UIImage.init(named: placeHolderImages))
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
}
