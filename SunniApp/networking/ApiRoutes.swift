//
//  ApiRoutes.swift
//  SunniApp
//
//  Created by Shahrukh on 11/05/2022.
//  Copyright © 2022 Coder. All rights reserved.
//

import Foundation

struct APIRoutes {
    static let baseUrl = "https://sunnis4marriage.com/Appadmin/app_controller/UserData/"
    static var createCharge = "/api/payments/stripe/createPaymentIntent"
    static var emphemeralKeys = "getEphemeralKeys"
    static var paymentIntent = "createPaymentIntent"

}
