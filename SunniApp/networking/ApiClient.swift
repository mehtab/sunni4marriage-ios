
import UIKit
import Alamofire
import ObjectMapper
import Stripe


class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    // MARK: - Onboarding
    func createUser(parameter: [String: String], _ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(URLs.register.rawValue, parameters: parameter as [String: AnyObject], httpMethod: .post, headers: ["Content-Type": "multipart/form-data"], completionBlock: completionBlock)
      }
    
    
    func createCharge(params: [String: Any], _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + ""]
        let params = params
        _ = sendRequest(APIRoutes.paymentIntent, parameters: params as [String: AnyObject], httpMethod: .post, headers: header, completionBlock: completionBlock)
        
    }
    
    
    func updateProfileData(url: URL ,parameter: [String: String], _ completion: @escaping ([String: AnyObject]) -> ()) {
            var parameters = parameter
           
          // Utility.showLoading()
           Alamofire.upload(multipartFormData: { (multipartFormData) in
               
               for (key, value) in parameters {
                 multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
               }
               
           }, usingThreshold: 0, to: url, method: .post, headers: nil, encodingCompletion: { (encodingResult) in
               
           //    Utility.hideLoading()
               switch encodingResult {
               case .success(let upload, _ , _):
                   upload.uploadProgress(closure: { (progress) in
                       print(progress)
                   })
                   upload.responseJSON { response in
                       switch response.result {
                       case .success(let json):
                           self.showRequestDetailForSuccess(responseObject: response)
                           
                           if let jsonObject = json as? [String:AnyObject] {
                               let errorCount = jsonObject["errors"] as? [String:Any]
                               
                               if errorCount == nil {
                                   
                                   completion(jsonObject)
//                                if let data = Mapper<Profile>().map(JSONObject: jsonObject) {
//                                      // completion(data, nil, "")
//
//                                   } else {
//                                     //  completion(nil, NSError(domain: "", code: -1, userInfo: nil), "")
//                                   }
                                   
                               } else {
                                   // completion(nil,NSError(domain: "", code: 422, userInfo: nil), ((errorCount!.values.first as? [String])?.first ?? "" ) )
                               }
                           }
                           break
                           
                       case .failure(let error):
                           self.showRequestDetailForFailure(responseObject: response)
                         //.  completion(nil, error as NSError, "")
                           break
                       }
                   }
               case .failure:
                   break
               }
           })
       }

    
   
    /*
    //MARK: - Get Cars
    func getAllCars(offset: Int, limit: Int, regNo: String,
                    makeYear: Int, carMake: String, carModel: String,
                    engineCapacity: String, color: String,
                    fuelType: String, carBadge: String,
                    transmission: String, minKlms: Int, maxKlms: Int,
                    maxPrice: Int, minPrice: Int, orderColumn: String, orderType: String,
                    _ completionBlock: @escaping APIClientCompletionHandler) {
        
        let params = ["offset" : offset, "limit" : limit, "reg_no" : regNo , "make_year" : makeYear, "car_make" : carMake, "car_model" : carModel, "engine_capacity" : engineCapacity , "color" : color ,"fuel_type" : fuelType, "car_badge" : carBadge , "transmission" : transmission, "car_km_min" : minKlms, "car_km_max" : maxKlms , "car_price_max" : maxPrice , "car_price_min" : minPrice , "orderColumn" : orderColumn , "orderType" : orderType ] as [String : AnyObject]
        //let params = ["offset" : offset, "limit" : limit] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getCars , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Cars
    func getAllCarsBySearch(offset: Int, limit: Int, searchKey: String,
                    _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["offset" : offset, "limit" : limit, "search" : searchKey] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getCars , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get CarDetails
    func getCarDetails(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["id" : id] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getCarDetails , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    func getNewsFeed (limit: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["limit" : limit] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getNotifications , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Car Makes
    func getCarMakes(_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = [:] as [String : AnyObject]
        _ = sendRequest(APIRoutes.carMakes , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Car Models
    func getCarModels(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["id" : id] as [String : AnyObject]
        _ = sendRequest(APIRoutes.carModels , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get About Us
    func getAboutUs(_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = [:] as [String:String]
        _ = sendRequest(APIRoutes.aboutUs , parameters: params as [String : AnyObject],httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Contacted Users
    func getContactedUsers(offset: Int, limit: Int,_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["offset" : offset, "limit" : limit] as [String : AnyObject]
        _ = sendRequest(APIRoutes.contactedUsers , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Create Car Valuation
    func createCarValuation(name: String, email: String, phoneNo: String, regNo: String, carMake: String, carModel: String, engineCapacity: String, color: String, klms: Int, carBadge: String, fuelType: String, bodyType: String, transmission: String, makeYear: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["name": name, "email" : email, "phone" : phoneNo, "reg_no" : regNo, "car_make" : carMake, "car_model" : carModel, "engine_capacity" : engineCapacity, "color" : color, "km_driven" : klms, "car_badge" : carBadge, "fuel_type" : fuelType, "body_type" : bodyType, "transmission" : transmission, "make_year" : makeYear] as [String:AnyObject]
        _ = sendRequest(APIRoutes.carValuation , parameters: params ,httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Chat History
    func getChatHistory(_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["limit" : 1000, "offset" : 0, "email" : UserDefaults.standard.string(forKey: "email")] as [String : AnyObject]
        _ = sendRequest(APIRoutes.chatHistory , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOnlineAdminMethod(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.onlineAdmin, parameters: nil, httpMethod: .get, headers: nil, completionBlock: completionBlock)
    }
    
    */
    
    
    
//    func createCarValuation(name: String, email: String, phoneNo: String, regNo: String, carMake: String, carModel: String, engineCapacity: String, color: String, klms: Int, carBadge: String, fuelType: String, bodyType: String, transmission: String, makeYear: Int ,_ completionBlock: @escaping APIClientCompletionHandler) {
//        let params = ["name": name, "email" : email, "phone" : phoneNo, "reg_no" : regNo, "car_make" : carMake, "car_model" : carModel, "engine_capacity" : engineCapacity, "color" : color, "km_driven" : klms, "car_badge" : carBadge, "fuel_type" : fuelType, "body_type" : bodyType, "transmission" : transmission, "make_year" : makeYear] as [String:AnyObject]
//        sendRequestUsingMultipart(APIRoutes.carValuation , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
//    }
//
//    func signInWebViewMethod(email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) {
//        let params = ["user": email, "pass": password] as [String:String]
//        sendRequestUsingMultipart(APIRoutes.webViewBaseUrl + APIRoutes.signInWebView, parameters: params as [String : AnyObject] , httpMethod: .post, headers: nil, completionBlock: completionBlock)
//    }

    
    //MARK: - Add Food Screen
//    func getSingleFood(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
//        let parameter = ["id": id,"with":"weights,properties"] as [String : AnyObject]
//        let headers = ["App":kAppName, "AppToken":kAppToken, "Authorization": "Bearer "+DataManager.shared.getAuthentication()!.accessToken]
//        _ = rawRequest(url: APIRoutes.baseUrl + APIRoutes.kGetFood+"/\(id)", method: .get, parameters: parameter, headers: headers, completionBlock: completionBlock)
//    }
    
    //
    //    @discardableResult
    //    func getTopChartEpisodes(_ completionBlock:@escaping APIClientCompletionHandler)->Request {
    //        let tokenString = "bearer "+User.shared.profileData!.token
    //        let params = [:] as [String:AnyObject]
    //        return sendRequest(APIRoutes.getTopChartEpisodes, parameters: params, headers:["Content-Type":  "application/json", "Authorization": tokenString], completionBlock: completionBlock)
    //    }
    
    
    
    
    //
    //extension UIImage {
    //    func rotate(radians: Float) -> UIImage? {
    //        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
    //        // Trim off the extremely small float value to prevent core graphics from rounding it up
    //        newSize.width = floor(newSize.width)
    //        newSize.height = floor(newSize.height)
    //
    //        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
    //        let context = UIGraphicsGetCurrentContext()!
    //
    //        // Move origin to middle
    //        context.translateBy(x: newSize.width/2, y: newSize.height/2)
    //        // Rotate around middle
    //        context.rotate(by: CGFloat(radians))
    //        // Draw the image at its center
    //        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
    //
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return newImage
    //    }
}

